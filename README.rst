`elasticslice` (https://gitlab.flux.utah.edu/elasticslice/elasticslice)
is a simple library that helps users write programs to dynamically and
automatically resize a ProtoGENI/Cloudlab (http://www.cloudlab.us)
experiment.  The ProtoGENI (https://www.protogeni.net) versions of the
GENI interface (http://groups.geni.net/geni/wiki/GAPI_AM_API_V3) support
dynamic addition and removal of nodes from a sliver (roughly,
experiment).  This means an experiment can grow and shrink depending on
resource demand of the user running the experiment -- and also on
resource demand at the cluster at which the experiment is running.

(`elasticslice` requires the `geni-lib` library, which you can find at
https://bitbucket.org/barnstorm/geni-lib .  If you are a CloudLab user,
it likely makes most sense to grab the CloudLab version of `geni-lib` at
https://bitbucket.org/emulab/geni-lib (checkout the `0.9-EMULAB` branch).
We push as many of our changes up to the official geni-lib repository,
but sometimes that process takes time and/or requires changes.)

You can install `elasticslice` in the normal way, i.e.

    $ python setup.py install --prefix=/usr/local

The online documentation is available at

    http://elasticslice.pages.flux.utah.edu/elasticslice

Or you can also build the `elasticslice` documentation via

    $ python setup.py build_sphinx

The documentation will be available in `doc/build/html`.
