#!/usr/bin/env python

from distutils.core import setup
import setuptools

setup(name='elasticslice',
      version='0.2',
      author='David M Johnson, Dmitry Duplyakin, Leigh Stoller',
      author_email='johnsond@flux.utah.edu, dmitry.duplyakin@colorado.edu, stoller@cs.utah.edu',
      url='https://gitlab.flux.utah.edu/elasticslice/elasticslice',
      description='A library and tools to manage ProtoGeni slices that dynamically change size',
      packages=setuptools.find_packages(),
      scripts = [ 'bin/esclient.py' ],
      install_requires = [
          'python >= 2.7.0',
      ]
)
