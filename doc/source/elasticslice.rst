elasticslice package
====================

Subpackages
-----------

.. toctree::

    elasticslice.clients
    elasticslice.managers
    elasticslice.rpc
    elasticslice.util

Module contents
---------------

.. automodule:: elasticslice
    :members:
    :undoc-members:
    :show-inheritance:
