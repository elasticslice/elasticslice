Server-side API Endpoint
========================

The dynslice endpoint exposes several new methods to ProtoGENI clients.

.. function:: GetVersion()

      Returns an integer representing the dynslice API version.

.. function:: GetServerDefaults()

      Returns a dictionary whose keys express server defaults.  For
      instance, the current dictionary contains keys like

      {
	'maxwaittime' => $MAX_WAITTIME,
	'policy' => 'LIFO',
      }

      'maxwaittime' is the maximum amount of time the server is willing
      to wait for a client to handle a pending node deletion (i.e., when
      the server notifies a client that it is going to reclaim and
      delete a node from the slice the client is managing, it will wait
      this many seconds at most for the client to report that it has
      finished using the node).

      'policy' is the default policy the server will use to obtain
      client-side resource values.  Currently, the server supports two
      policies: 'CLIENT', where the client *must* send the server a set
      of resource values for every resource currently in its slice
      (otherwise, the server uses a value of 1.0 for each node without a
      client-set value); and 'LIFO', by which the server does not expect
      to hear resource values from the client, and will automatically
      create a set of client-side resource values on behalf of the
      server.  Each value will be determined by the length of time the
      node has been allocated to the slice, with longest-lived nodes
      receiving highest values, and shortest-lived nodes receiving least
      values.

  * SetSliceClientEndpoint(slice_urn, url, credentials[], policy, defwaittime)

      Associates a client-side API endpoint with a slice urn.  This
      tells the server that it can talk to the dynslice client, to
      notify it of new resource values, or pending deletions of nodes in
      the client's slice.  The URL is an endpoint at which the server
      can invoke the client-side API.

      This method should only fail if the client attempts to set a
      defwaittime that is longer that the server's maxwaittime.

  * RemoveSliceClientEndpoint(slice_urn, credentials[])

      Disassociates a slice from a client endpoint.  The server will no
      longer attempt to notify this endpoint of new resource values nor
      pending deletions of nodes in the slice.

  * ListSliceResourceValuePolicies()

      Returns a list of 2-element tuples, where the first element is the
      name of the policy, and the second element is the description of
      the policy.  The client can tell the server how it plans to set
      resource values for each resource in its slice, by selecting one
      of these policies.

  * SetSliceClientOptions(slice_urn, credentials[], policy, defwaittime)

      Set new client options for an existing slice_urn.  Both policy and
      defwaittime are optional, although the caller presumably would
      want to set one or the other.  For instance, this allows the
      client to change the resource value policy it last selected.

  * SetSliceResourceValues(slice_urn, credentials[], values)

      Set new resource values for the resources in slice_urn.  The
      client calls this server method to inform the server of the values
      it has placed on its currently-held resources.  The values
      parameter is a list of dictionaries, one for each resource
      (node).  Each dictionary must have the 'value' key, and the range
      of supported values is between 0.0 and 1.0 .  To identify which
      node the value corresponds to, the user may set one of several
      keys:

        'node_id'       (physical node id, i.e. "pc312")
 	'vname'         (logical node name within the slice, i.e. "mynode-1")
	'component_urn' (the URN of the node)
	'client_id'     (logical node name within the slice, i.e. "mynode-1")

      The client can set all of these keys for each value dictionary,
      but they are checked in the order above.

      This method returns success unless you attempt to set a value for
      a node identifier that is not a member of your slice.
