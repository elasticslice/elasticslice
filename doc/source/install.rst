Build and Install
=================

You can install `elasticslice` in the normal way:

    ``$ python setup.py install --user``

or

    ``$ python setup.py install --prefix=/usr/local``

or similar.  If you use the ``--user`` option, elasticslice will be
installed in ``$HOME/.local/{bin,lib}`` or thereabouts.

You can build the `elasticslice` documentation via

    ``$ python setup.py build_sphinx``

The documentation will be available in `doc/build/html`.
