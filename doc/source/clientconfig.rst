Configuring the elasticslice client
===================================

First, you need to configure your environment to run the basic
elasticslice client tool.  Set your PYTHONPATH environment variable
correctly to include the place where you installed elasticslice and
geni-lib (as specified in the installation instructions).

Second, you'll need to place your GENI certificate, passphrase, and CM
certificates, into your home directory in the `.ssl` directory.  All
GENI XMLRPC methods must be called with appropriate credentials, and for
you that means your certificate and its passphrase.  By default,
elasticslice looks for your certificate in `~/.ssl/encrypted.pem`, and it
looks for the passphrase for that certificate in `~/.ssl/password`.  If
you don't specify your passphrase, you'll be prompted for it on
``stdin`` for each RPC!

Finally, copy `etc/main.conf` from the elasticslice source repository
to `~/.protogeni/elasticslice.conf`, and modify it as necessary (i.e.,
set a slicename, a default CM, and a nodetype).  Note that any options
supplied on the command line override those in the configuration file.
Here's a simple version that targets a particular slice on the `Apt`
cluster, and will populate it with nodes of type `c6220`::


    [MAIN]
    #
    # Use this URN as the default slice URN so you don't have to specify
    # one every time.
    #
    slicename=urn:publicid:IDN+emulab.net:tbres+slice+djdynslice1
    #
    # Certificate and passphrase files.
    #
    cert=~/.ssl/encrypted.pem
    passphrasefile=~/.ssl/password
    #
    # The CM targeted by default -- i.e., the cluster at which your
    # nodes will be allocated by default.  Make sure your certificate
    # will be accepted here.
    #
    default-cm=apt
    #
    # A certificate for the CM, so it can be verified.
    #
    cacert=~/.ssl/apt-cacert.pem
    #
    # A node type to use by default when adding nodes, or computing
    # type-specific availability thresholds in the manager.
    #
    nodetype=c6220
    #
    # Use a simple cache to alleviate load on XMLRPC server.  Certain
    # methods are very expensive (i.e., GetResources on the Emulab CM
    # takes many seconds); and some return values are useful until they
    # expire (i.e., credentials).
    #
    fscache=True
    #
    # An image to use by default when adding nodes.
    #
    image_urn=urn:publicid:IDN+emulab.net+image+emulab-ops:UBUNTU14-64-STD
    
    #
    # DAEMON (AUTO-MANAGEMENT) MODE SETTINGS
    #
    # If `automanage` is True, the daemon's automatic manager thread
    # will run immediately at program start and create, renew, and
    # add/delete nodes to/from the slice as necessary, according to policy.
    #
    # If `autostart` is True, the client's internal XMLRPC server will
    # be automatically started at program start as well.  This server
    # accepts notifications from a daemon running at the CM.  For
    # instance, the CM's daemon will notify this client of new
    # server-side resource values, and also if/when it needs to reclaim
    # elastically-added nodes from this slice.
    #
    # If you don't plan to run in daemon mode, set these to False.
    #
    automanage=False
    autostart=False
    #
    # These options specify Python classes for the manager, helper, and
    # clientserver endpoint.  The defaults are the SimpleElasticSlice*
    # classes below; but of course you can extend or replace them.  Note
    # that the SimpleElasticSliceManager also currently provides the
    # clientserver endpoint interface; thus that option is unspecified.
    #
    manager_class=elasticslice.managers.core.SimpleElasticSliceManager
    helper_class=elasticslice.managers.core.SimpleElasticSliceHelper
    clientserver_endpoint_class=
    #
    # These options provide several basic policy knobs to the
    # SimpleElasticSlice* family of classes.  In management mode, the
    # manager will always maintain at least `minthreshold` nodes in your
    # slice, and will never add more than `maxthreshold` nodes.
    # Furthermore, it will maintain the slice's allocation to respect
    # the `percent_available_minimum` threshold, which is a minimum
    # amount of free nodes to ensure are available at the CM.  In other
    # words, the manager will continuously poll the amount of available
    # nodes at the CM, and adjust its own allocation to ensure that
    # there are at least `percent_available_minimum` percentage of free
    # nodes at the CM.  That said, it will never reduce below the
    # `minthreshold`, and never add nodes above the `maxthreshold`.
    #
    minthreshold=3
    maxthreshold=10
    percent_available_minimum=0.33
    #
    # If True, the manager is allowed to automatically delete nodes.
    #
    enable_delete=True
    #
    # If automatic manager deletion is enabled, wait at least this many
    # seconds in between successive node deletions (removals of a node
    # from your slice).
    #
    node_delete_wait_time=300


