elasticslice.managers package
=============================

Submodules
----------

elasticslice.managers.core module
---------------------------------

.. automodule:: elasticslice.managers.core
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: elasticslice.managers
    :members:
    :undoc-members:
    :show-inheritance:
