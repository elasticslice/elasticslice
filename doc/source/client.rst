Client-side API Endpoint
========================

Unlike the other GENI API interfaces, which are all provided by an
XMLRPC server at a GENI component (i.e., a slice authority,
clearinghouse, component manager, aggregate manager), this is a server
interface that is provided by a client!  It allows a server daemon to
notify a client of things that are happening to its slice.  This is
necessary to accomodate the full dynslice model of use.

Here, our documentation is provided in the form of a python interface.
You can find this interface in a client-side dynslice library, at
https://gitlab.flux.utah.edu/elasticslice/elasticslice/ (specifically,
the file containing this class is
https://gitlab.flux.utah.edu/elasticslice/elasticslice/blob/master/elasticslice/managers/core.py).
Here is the interface::


  class ElasticSliceClientEndpoint(ProtoGeniClientServerEndpoint):
    """
    Creates a stub elasticslice client-server API endpoint that simply
    returns SUCCESS (and prints out the RPC arguments on invocation if
    in debug mode).
    """
    def __init__(self):
        super(ElasticSliceClientEndpoint,self).__init__(self)
        pass
    
    def GetVersion(self,params=None):
        return ProtoGeniResponse(ProtoGeniClientDefs.RESPONSE_SUCCESS,
                                 output="1.0")
    
    def SetResourceValues(self,params):
        """
        @params["nodelist"] is a list of physical node resources.  Each list
        item must have at least the following key/value pairs:
        
          "node_id" : "<NODE_ID>"
          "value" : float(0,1.0)
        
        Each list item may also have the following values, if the server sends
        them:
        
          "client_id" : "<CLIENT_ID>" (the client_id from the rspec, if this
                                       client has this component_id allocated)
          "component_urn" : "<COMPONENT_URN>"
        """
        LOG.debug("SetResourceValues(%s)" % (str(params),))
        
        return ProtoGeniResponse(ProtoGeniClientDefs.RESPONSE_SUCCESS)
    
    def NotifyDeletePending(self,params):
        """
        The server calls this method to tell us it is going to revoke some of
        our resources.  @params["nodelist"] is a list of physical
        resources it is going to revoke.  Each list item must have at
        least the following key/value pairs:

          "node_id" : "<NODE_ID>" (the physical node_id)        
          "client_id" : "<CLIENT_ID>" (the client_id from the rspec, if this
                                       client has this component_id allocated)
          "max_wait_time" : <seconds>
          
        The server may call this method repeatedly as a sort of "countdown"
        timer (by default it gets called every minute), and it will count down
        the max_wait_time dict fields on each call.  Thus, you have multiple
        opportunities to respond when your delete jobs have finished.
        
        The server may specify max_wait_time of 0 if it cannot wait gracefully
        for the client to clean off the node (in which case the node may
        have been revoked by the time the client receives this message);
        otherwise it should specify how long it is willing to wait for the
        client to release the node before it forcibly takes the node back.
        
        The client may respond with simply success, or it can reply with
        a list of dicts that specify nodes that may be immediately
        stolen.  This list should be idential to the input list, but you
        tell the server that it can stop waiting for a particular node
        by setting max_wait_time to 0.
        """
        LOG.debug("NotifyResourceRevocation(%s)" % (str(params),))

        return ProtoGeniResponse(ProtoGeniClientDefs.RESPONSE_SUCCESS)
    
    def NotifyDeleteComplete(self,params):
        """
        The server calls this method to tell us that it has finished invoking
        the CM v2.0 DeleteNodes() method on your behalf -- so from
        Cloudlab's perspective, those nodes have been removed from your
        experiment.  @params["nodelist"] is a list of physical resources
        it is going to revoke.  Each list item must have at least the
        following key/value pairs:

          "node_id" : "<NODE_ID>" (the physical node_id)        
          "client_id" : "<CLIENT_ID>" (the client_id from the rspec, if this
                                       client has this component_id allocated)
          "max_wait_time" : <seconds>
        
        The only valid response from this method is SUCCESS; it is ignored
        anyway.
        """
        LOG.debug("NotifyResourceRevocation(%s)" % (str(params),))

        return ProtoGeniResponse(ProtoGeniClientDefs.RESPONSE_SUCCESS)
    
    pass
