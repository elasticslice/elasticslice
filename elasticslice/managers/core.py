
import sys
import os
import traceback
import time
import threading
from elasticslice.rpc.protogeni import ProtoGeniClientDefs, ProtoGeniServer, \
    ProtoGeniResponse, ProtoGeniManifestWrapper, \
    ProtoGeniClientServerEndpoint, ProtoGeniClientServer, SENDMAIL
from elasticslice.util.applicable import ApplicableClass,ApplicableMethod
import elasticslice.util.exceptions as exc
import geni.rspec.pgad as RSpecAd
import geni.rspec.pg as RSpec
import geni.rspec.igext as IG
import logging
import random

LOG = logging.getLogger(__name__)

SECOND = 1
MINUTE = SECOND * 60
HOUR   = MINUTE * 60
DAY    = HOUR * 24
MONTH  = DAY * 31
YEAR   = DAY * 365


class ElasticSliceHelper(object):
    """
    An abstract class that can be subclassed to specialize the
    functionality of the ElasticSliceManager (below) without actually
    changing that class.  The idea is that this class is supposed to
    handle the semantics of dealing with a dynamic experiment (i.e.,
    rspec, startup commands, adding new nodes, handling deletions) --
    but doesn't have to manage any of the dynamic experiment's state or
    lifecycle operations.
    """
    def __init__(self,server=None,config=None):
        self.server = server
        self.config = config
        pass
    
    def create_rspec(self):
        """
        Returns an rspec XML string that will be used to create the sliver
        for a dynamic experiment if it doesn't already exist.  This
        method is only called if the sliver doesn't exist already.  If
        the sliver does exist, the manager will only call
        generateAddNodesArgs below when it decides to add nodes
        dynamically.  At that time, the helper should grab the current
        sliver manifest and initialize any necessary state to be able to
        generate new nodes (that don't overlap with existing ones, for
        instance).
        """
        raise exc.NotImplementedError("create_rspec")

    def get_add_args(self,count=1):
        """
        Returns a dict of nodes that conforms to the CMv2 AddNodes() API call.
        """
        raise exc.NotImplementedError("get_add_args")
    
    def get_delete_commands(self,nodelist):
        """
        Returns a list of `sh` commands that are executed serially in a
        forked child.
        """
        return None
    
    def handle_added_node(self,node,status):
        """
        This method should be called by a manager when it sees that an
        added node has become "ready" or "failed".  @node is a dict like
        
          { 'client_id':'node-1','node_id':'pc100',...}
        
        and @status is either 'ready', 'failed'.  The return value is
        not checked by the caller.
        """
        pass
    
    def handle_deleted_node(self,node):
        """
        This method should be called by a manager when it sees that an
        added node has become "ready" or "failed".  @node is a dict like
        
          { 'client_id':'node-1','node_id':'pc100',...}
        
        The return value is not checked by the caller.
        """
        pass
    
    pass

class PluginElasticSliceHelper(ElasticSliceHelper):
    """
    Managers that don't implement any semantics themselves (i.e., how to Add,
    Delete a node, or create an Rspec), should use this class as a mixin.
    """
    def __init__(self,server=None,config=None,helper=None):
        ElasticSliceHelper.__init__(self,server=server,config=config)
        self.helper = helper
        pass
    
    def set_helper(self,helper):
        """
        This class is designed to be used only as a mixin to a Manager.
        The core driver program looks for this function as a method of
        the Manager, and if it exists, calls it.  This is a mixin
        function to a Manager; thus we must provide this so that this
        plugin helper can call through to an external helper.
        
        """
        self.helper = helper
        pass

    def create_rspec(self):
        if self.helper:
            return self.helper.create_rspec()
        else:
            raise exc.NotImplementedError("create_rspec")
        pass

    def get_add_args(self,count=1):
        if self.helper:
            return self.helper.get_add_args(count=count)
        else:
            raise exc.NotImplementedError("get_add_args")
        pass

    def get_delete_commands(self,nodelist):
        if self.helper:
            return self.helper.get_delete_commands(nodelist)
        return None
    
    def handle_added_node(self,node,status):
        if self.helper:
            return self.helper.handle_added_node(node,status)
        pass
    
    def handle_deleted_node(self,node):
        if self.helper:
            return self.helper.handle_deleted_node(node)
        pass

    pass

class SimpleElasticSliceHelper(ElasticSliceHelper):
    """
    A default, simple elasticslice helper.  User can specify number of
    PCs, an image URN, number of lans (and whether or not they are
    multiplexed), tarballs, startup command, node type, and then some
    naming conventions.
    """
    
    def __init__(self,server=None,config=None,
                 num_pcs=1,image_urn=None,num_lans=0,multiplex_lans=False,
                 tarballs=[],startup_command=None,nodetype=None,
                 node_prefix="node",lan_prefix="lan"):
        super(SimpleElasticSliceHelper,self).__init__(server)
        
        self.config = config
        self.rspec = RSpec.Request()
        self.num_pcs = (config and 'num_pcs' in config.all
                        and config.all['num_pcs']) or num_pcs
        self.image_urn = (config and 'image_urn' in config.all
                          and config.all['image_urn']) or image_urn
        self.num_lans = (config and 'num_lans' in config.all
                         and config.all['num_lans']) or num_lans
        self.multiplex_lans = \
            (config and 'multiplex_lans' in config.all
             and config.all['multiplex_lans']) or multiplex_lans
        self.tarballs = (config and 'tarballs' in config.all
                         and config.all['tarballs']) or tarballs
        self.startup_command = \
            (config and 'startup_command' in config.all
             and config.all['startup_command']) or startup_command
        self.nodetype = (config and 'nodetype' in config.all
                         and config.all['nodetype']) or nodetype
        self.node_prefix = (config and 'node_prefix' in config.all
                            and config.all['node_prefix']) or node_prefix
        self.lan_prefix = (config and 'lan_prefix' in config.all
                           and config.all['lan_prefix']) or lan_prefix
        self.generated = False
        self.idx = 0
        
        pass
    
    def create_rspec(self):
        lans = []
    
        for i in range(0,self.num_lans):
            datalan = RSpec.LAN("%s-%d" % (self.lan_prefix,i,))
            # Might need this if you cram multiple links atop a PC with only one
            # NIC, for instance...
            datalan.link_multiplexing = self.multiplex_lans
            datalan.best_effort = True
            datalan.type = "vlan"
            lans.append(datalan)
            pass
    
        for i in range(0,self.num_pcs):
            node = RSpec.RawPC("%s-%d" % (self.node_prefix,i,))
            if self.image_urn:
                node.disk_image = self.image_urn
            if self.nodetype:
                node.hardware_type = self.nodetype
            j = 0
            for datalan in lans:
                iface = node.addInterface("if%d" % (j,))
                datalan.addInterface(iface)
                j += 1
                pass
            if self.tarballs:
                for tb in self.tarballs:
                    if len(tb) == 2:
                        (tarballURL,installPath) = tb
                        node.addService(RSpec.Install(url=tarballURL,
                                                      path=installPath))
                    else:
                        tarballURL = tb[0]
                        node.addService(RSpec.Install(url=tarballURL))
                        pass
                    pass
                pass
            if self.startup_command:
                node.addService(RSpec.Execute(shell="sh",
                                              command=self.startup_command))
                pass
            self.rspec.addResource(node)
            pass
    
        for datalan in lans:
            self.rspec.addResource(datalan)
            pass
        
        self.idx += self.num_pcs
        self.generated = True
        
        return self.rspec.toXMLString()
    
    def _set_state_from_manifest(self,wrapped_pgmanifest):
        # Find our max node ID.
        for node in wrapped_pgmanifest.nodes:
            cid = node.name
            if cid.startswith(self.node_prefix):
                cid_num = int(cid[(len(self.node_prefix)+1):])
                if cid_num > self.idx:
                    LOG.info("found %s; setting idx to %d" % (cid,cid_num))
                    self.idx = cid_num
                    pass
                pass
            pass
        
        pass
    
    def get_add_args(self,count=1):
        from xml.sax.saxutils import escape
        
        # If we didn't generate an rspec for a sliver creation,
        # initialize our state from the manifest.
        if not self.generated:
            wrapped_pgmanifest = self.server.get_wrapped_manifest()
            self._set_state_from_manifest(wrapped_pgmanifest)
            pass
        
        retval = {}
        for i in range(self.idx + 1,self.idx + count + 1):
            nn = "%s-%d" % (self.node_prefix,i)
            lans = []
            for j in range(0,self.num_lans):
                lans.append({ 'name' : "%s-%s" % (self.lan_prefix,j) })
                pass
            nd = {}
            if self.image_urn:
                nd["diskimage"] = self.image_urn
            if self.startup_command:
                nd["startup"] = escape(self.startup_command)
            if self.tarballs:
                nd["tarballs"] = tarballs
            if self.nodetype:
                nd['hardware_type'] = self.nodetype
            if len(lans) > 0:
                nd["lans"] = lans
                pass
            retval[nn] = nd
            pass
        self.idx += count
        return retval
    
    def get_delete_commands(self,nodelist):
        return None
    
    pass

class ElasticSliceClientEndpoint(ProtoGeniClientServerEndpoint):
    """
    Creates a stub elasticslice client-server API endpoint that simply
    returns SUCCESS (and prints out the RPC arguments on invocation if
    in debug mode).
    """
    def __init__(self):
        super(ElasticSliceClientEndpoint,self).__init__(self)
        pass
    
    def GetVersion(self,params=None):
        return ProtoGeniResponse(ProtoGeniClientDefs.RESPONSE_SUCCESS,
                                 output="1.0")
    
    def SetResourceValues(self,params):
        """
        @params["nodelist"] is a list of physical node resources.  Each list
        item must have at least the following key/value pairs::
        
            "node_id" : "<NODE_ID>"
            "value" : float(0,1.0)
        
        Each list item may also have the following values, if the server sends
        them::
        
            "client_id" : "<CLIENT_ID>" (the client_id from the rspec, if this
                                         client has this component_id allocated)
            "component_urn" : "<COMPONENT_URN>"
        """
        LOG.debug("SetResourceValues(%s)" % (str(params),))
        
        return ProtoGeniResponse(ProtoGeniClientDefs.RESPONSE_SUCCESS)
    
    def NotifyDeletePending(self,params):
        """
        The server calls this method to tell us it is going to revoke some of
        our resources.  @params["nodelist"] is a list of physical
        resources it is going to revoke.  Each list item must have at
        least the following key/value pairs::

            "node_id" : "<NODE_ID>" (the physical node_id)
            "client_id" : "<CLIENT_ID>" (the client_id from the rspec, if this
                                         client has this component_id allocated)
            "max_wait_time" : <seconds>

        The server may call this method repeatedly as a sort of "countdown"
        timer (by default it gets called every minute), and it will count down
        the max_wait_time dict fields on each call.  Thus, you have multiple
        opportunities to respond when your delete jobs have finished.
        
        The server may specify max_wait_time of 0 if it cannot wait gracefully
        for the client to clean off the node (in which case the node may
        have been revoked by the time the client receives this message);
        otherwise it should specify how long it is willing to wait for the
        client to release the node before it forcibly takes the node back.
        
        The client may respond with simply success, or it can reply with
        a list of dicts that specify nodes that may be immediately
        stolen.  This list should be idential to the input list, but you
        tell the server that it can stop waiting for a particular node
        by setting max_wait_time to 0.
        """
        LOG.debug("NotifyResourceRevocation(%s)" % (str(params),))

        return ProtoGeniResponse(ProtoGeniClientDefs.RESPONSE_SUCCESS)
    
    def NotifyDeleteComplete(self,params):
        """
        The server calls this method to tell us that it has finished invoking
        the CM v2.0 DeleteNodes() method on your behalf -- so from
        Cloudlab's perspective, those nodes have been removed from your
        experiment.  @params["nodelist"] is a list of physical resources
        it is going to revoke.  Each list item must have at least the
        following key/value pairs::

            "node_id" : "<NODE_ID>" (the physical node_id)
            "client_id" : "<CLIENT_ID>" (the client_id from the rspec, if this
                                         client has this component_id allocated)
            "max_wait_time" : <seconds>
        
        The only valid response from this method is SUCCESS; it is ignored
        anyway.
        """
        LOG.debug("NotifyResourceRevocation(%s)" % (str(params),))

        return ProtoGeniResponse(ProtoGeniClientDefs.RESPONSE_SUCCESS)
    
    pass

class SimpleElasticSliceClientEndpoint(ElasticSliceClientEndpoint):
    """
    SimpleElasticSliceClientEndpoint is a simple class that provides the
    elasticslice client endpoint API, but tracks and saves notification
    info rom the server, and tracks node delete operations and kicks off
    commands using a Manager to run when a node should be deleted.  An
    experiment creator creates a ProtoGeniClientServer with one of these
    endpoints, and it handles the elasticslice RPC invocations from the
    ProtoGeni server.  If delete_in_group is true, the
    start_delete_operation function will be called with *all* nodes in
    the list sent to NotifyDeletePending; this allows experiments which
    want to delete at once all nodes the server sent to us.  This avoids
    potential wasted work (i.e., if the experiment is an openstack
    cluster, and we need to migrate VMs from the hypervisor/compute
    nodes to other compute nodes, we don't want to migrate to another
    compute node that is about to be deleted!).  On the other hand, if
    you must delete nodes one at a time, you can do that too, by setting
    delete_in_group=False.
    """
    def __init__(self,helper=None,delete_in_group=True,rlock=None):
        super(SimpleElasticSliceClientEndpoint,self).__init__()
        
        self.helper = helper
        self.dsc_lock = rlock
        self.delete_in_group = delete_in_group
        self.values_timestamp = 0
        self.values_by_nodeid = {}
        self.values_by_clientid = {}
        self.pending_deletes = {}
        self.operations = {}
        #
        # A hash of childpid to op dicts, like:
        #   'nodelist' : [],
        #
        self.fork_operations = {}
        pass
    
    ##
    ## The CamelCase methods are the RPCs.
    ##
    def SetResourceValues(self,params):
        """
        @params["nodelist"] is a list of physical node resources.  Each list
        item must have at least the following key/value pairs:
        
          "node_id" : "<NODE_ID>"
          "value" : float(0,1.0)
        
        Each list item may also have the following values, if the server sends
        them:
        
          "client_id" : "<CLIENT_ID>" (the client_id from the rspec, if this
                                       client has this component_id allocated)
          "component_urn" : "<COMPONENT_URN>"
        """
        LOG.debug("SetResourceValues(%s)" % (str(params),))
        
        if self.dsc_lock: self.dsc_lock.acquire()
        self.values_timestamp = time.time()
        self.values = {}
        self.valuesByNodeId = {}
        self.valuesByClientId = {}
        for nv in params['nodelist']:
            self.valuesByNodeId[nv['node_id']] = nv['value']
            if nv.has_key('client_id'):
                self.valuesByClientId[nv['client_id']] = nv['value']
                pass
            pass
        #self.values = params['nodelist']
        if self.dsc_lock: self.dsc_lock.release()

        return ProtoGeniResponse(ProtoGeniClientDefs.RESPONSE_SUCCESS)
    
    def NotifyDeletePending(self,params):
        """
        The server calls this method to tell us it is going to revoke some of
        our resources.  @params["nodelist"] is a list of physical
        resources it is going to revoke.  Each list item must have at
        least the following key/value pairs:

          "node_id" : "<NODE_ID>" (the physical node_id)        
          "client_id" : "<CLIENT_ID>" (the client_id from the rspec, if this
                                       client has this component_id allocated)
          "max_wait_time" : <seconds>
          
        The server may call this method repeatedly as a sort of "countdown"
        timer (by default it gets called every minute), and it will count down
        the max_wait_time dict fields on each call.  Thus, you have multiple
        opportunities to respond when your delete jobs have finished.
        
        The server may specify max_wait_time of 0 if it cannot wait gracefully
        for the client to clean off the node (in which case the node may
        have been revoked by the time the client receives this message);
        otherwise it should specify how long it is willing to wait for the
        client to release the node before it forcibly takes the node back.
        
        The client may respond with simply success, or it can reply with
        a list of dicts that specify nodes that may be immediately
        stolen.  This list should be idential to the input list, but you
        tell the server that it can stop waiting for a particular node
        by setting max_wait_time to 0.
        """
        LOG.debug("NotifyDeletePending(%s)" % (str(params),))
        
        if self.dsc_lock: self.dsc_lock.acquire()
        
        nodelist = params['nodelist']
        
        # Before we handle the pending nodelist, see if we already have
        # started delete operations for any of them.  If we have, we
        # need to check those pids and see if they're done.
        for nv in nodelist:
            node_id = nv['node_id']
            if self.pending_deletes.has_key(node_id) \
                and self.pending_deletes[node_id].has_key('op'):
                childpid = self.pending_deletes[node_id]['op']
                if self.operations[childpid].has_key('exit_code'):
                    self.pending_deletes[node_id]['max_wait_time'] = 0
                    continue
                else:
                    (pid,exit_code) = os.waitpid(childpid,os.WNOHANG)
                    if pid == childpid:
                        LOG.debug("childpid %d finished for nodes %s"
                                  % (childpid,str(self.operations[childpid]['nodelist'])))
                        self.pending_deletes[node_id]['max_wait_time'] = 0

                        self.operations[childpid]['exit_code'] = exit_code
                        pass
                    pass
                pass
            pass
        
        # Ok, now process the nodelist and kick off any new delete
        # operations, and note any completed delete operations (if there
        # are no completions from us, we just respond with success; else
        # we tell the server the good news that it's free to steal the
        # node back).
        deletelist = []
        pushUpdates = False
        for nv in nodelist:
            node_id = nv['node_id']
            if not self.pending_deletes.has_key(node_id):
                # add it
                self.pending_deletes[node_id] = nv
                deletelist.append(nv)
                pass
            else:
                #
                # Ok.  If our delete operation finished (and thus our
                # copy of max_wait_time is set 0), tell the server.  If
                # our delete operation is still pending, decrement the
                # remaining max_wait_time.
                #
                if self.pending_deletes[node_id]['max_wait_time'] == 0:
                    nv['max_wait_time'] = 0
                    pushUpdates = True
                else:
                    self.pending_deletes[node_id]['max_wait_time'] = \
                        nv['max_wait_time']
                    pass
                pass
            pass
        if len(deletelist) > 0:
            if self.delete_in_group:
                self.start_delete(deletelist)
                pass
            else:
                for nv in deletelist:
                    self.start_delete([nv])
                    pass
                pass
            pass
        if self.dsc_lock: self.dsc_lock.release()

        if pushUpdates:
            return ProtoGeniResponse(ProtoGeniClientDefs.RESPONSE_SUCCESS,
                                     params['nodelist'])
        else:
            return ProtoGeniResponse(ProtoGeniClientDefs.RESPONSE_SUCCESS)
        pass

    def NotifyDeleteComplete(self,params):
        """
        The server calls this method to tell us that it has finished invoking
        the CM v2.0 DeleteNodes() method on your behalf -- so from
        Cloudlab's perspective, those nodes have been removed from your
        experiment.  @params["nodelist"] is a list of physical resources
        it is going to revoke.  Each list item must have at least the
        following key/value pairs:

          "node_id" : "<NODE_ID>" (the physical node_id)        
          "client_id" : "<CLIENT_ID>" (the client_id from the rspec, if this
                                       client has this component_id allocated)
          "max_wait_time" : <seconds>
        
        The only valid response from this method is SUCCESS; it is ignored
        anyway.
        """
        LOG.debug("NotifyDeleteComplete(%s)" % (str(params),))
        
        if self.dsc_lock: self.dsc_lock.acquire()
        deletelist = []
        for nv in params['nodelist']:
            node_id = nv['node_id']
            self.end_delete(nv)
            pass

        return ProtoGeniResponse(ProtoGeniClientDefs.RESPONSE_SUCCESS)
    
    def start_delete(self,nodelist):
        cmds = self.helper.get_delete_commands(nodelist)
        if not cmds or len(cmds) == 0:
            return None

        childpid = os.fork()
        
        if childpid != 0:
            self.operations[childpid] = {'nodelist':nodelist}
            LOG.debug("forked child %d to run commands '%s'"
                      % (childpid," ; ".join(cmds)))
            for nv in nodelist:
                node_id = nv['node_id']
                self.pending_deletes[node_id]['op'] = childpid
                pass
            pass
        else:
            #
            # We're in the child, just run them all.
            #
            try:
                for cmd in cmds:
                    os.system(cmd)
                    pass
                exit(0)
                pass
            except:
                exit(-1)
                pass
            pass
                
        return None
    
    def end_delete(self,nodelist):
        for nv in nodelist:
            node_id = nv['node_id']
            if not self.pending_deletes.has_key[node_id]:
                continue
            pd = self.pending_deletes[node_id]
            del self.pending_deletes[node_id]
            if not pd.has_key('op'):
                continue
            op = pd['op']
            if not self.operations.has_key(op):
                continue
            if not self.operations[op].has_key(exit_code):
                os.kill(op,15)
            del self.operations[op]
            pass
        return None
    
    ##
    ## These are the utility methods that users can call.
    ##
    
    def has_pending_deletes(self):
        if len(self.pending_deletes.keys()) > 0:
            return True
        return False
    
    pass

@ApplicableClass()
class ElasticSliceManager(ElasticSliceHelper):
    """
    A manager of a dynamic slice that creates a slice (and its sliver);
    adds nodes when possible (i.e., when usage is under some specified
    threshold); optionally deletes nodes when the free threshold is
    breached; and renews the slice and sliver.
    
    A manager is also a helper by default, although the default helper
    interface (ElasticSliceHelper) methods either raise
    NotImplementedError or return None.  Managers that do not provide a
    Helper implementation natively should subclass the
    PluginElasticSliceHelper (which takes an external helper object and
    calls it), and the library user should call manager.set_helper(helper_obj).
    """
    def __init__(self,server,config=None):
        ElasticSliceHelper.__init__(self,server)
        self.server = server
        self.config = config
        pass

    @ApplicableMethod(alias='managed_create_slice')
    def create_slice(self):
        """
        Create the slice from within the manager, using information
        from the ElasticSliceHelper associated with this ElasticSliceManager.
        """
        ret = self.server.create_slice()
        if ret:
            return ret

        LOG.error("Could not create slice!")
        raise exc.NonexistentSliceError()

    @ApplicableMethod(alias='managed_ensure_slice')
    def ensure_slice(self):
        """
        Check to see if the slice exists; if not, create it.
        """
        ret = self.server.resolve_slice()
        if ret:
            return ret
        LOG.info("Slice does not exist, creating...")
        return self.create_slice()

    @ApplicableMethod(alias='managed_create_sliver')
    def create_sliver(self,cm=None):
        """
        Create the sliver from within the manager, using information
        from the ElasticSliceHelper associated with this ElasticSliceManager.
        
        :param cm: the CM (component manager) at which to create the sliver.
        """
        if not cm: cm = self.default_cm
        
        # Grab an rspec from the helper (us, via plugin helper)
        # Save off old argv because geni-lib scripts process it :(
        oldargv = sys.argv
        sys.argv = [ sys.argv[0], ]
        if not self.baseRspec:
            self.baseRspec = self.create_rspec()
            pass
        sys.argv = oldargv
                    
        ret = self.server.create_sliver(self.baseRspec,gen_random_key=True)
        if ret:
            return ret
        
        LOG.error("Could not create sliver!")
        raise exc.NonexistentSliverError()

    @ApplicableMethod(alias='managed_ensure_sliver')
    def ensure_sliver(self,cm=None):
        """
        Check to see if the sliver exists; if not, create it using information
        from the ElasticSliceHelper associated with this ElasticSliceManager.
        
        :param cm: the CM (component manager) at which to create the sliver.
        """
        if not cm: cm = self.default_cm
        
        ret = self.server.resolve_sliver(cm=cm)
        if ret:
            return ret
        
        LOG.info("Sliver does not exist, creating...")
        return self.create_sliver(cm=cm)
    
    def _add_nodes(self,nodes={},cm=None):
        return self.server.add_nodes(nodes=nodes,cm=cm)
    
    @ApplicableMethod(alias='managed_should_add_nodes')
    def should_add_nodes(self,count=0,cm=None):
        """
        By default, add_nodes calls this method to find out if it should
        add any nodes.  If this function returns an integer, add_nodes
        interprets that value as the number of new nodes to add, and it
        calls get_add_args() to get the arguments to pass to _add_nodes.
        If the function returns a dict, it passes that directly to
        _add_nodes.  If the function returns None, False, nothing is
        added.  This gives subclasses enough flexibility, hopefully.
        If you pass a nonzero, positive integer via `count`, this method
        must check if it should add exactly `count` nodes, or not, and
        return accordingly.
        
        :param count: If nonzero, positive integer, this method checks if the manager should add exactly `count` nodes, or not, and return accordingly.
        :param cm: the CM (component manager) at which to add nodes.
        """
        return False
    
    @ApplicableMethod(
        alias='managed_add_nodes')
    def add_nodes(self,count=0,cm=None):
        """
        This function manages the addition of nodes.  This manager will
        call it to add nodes to this slice, as necessary.
        
        :param count: If nonzero, positive integer, this method checks (via :func:`should_add_nodes`) if the manager should add exactly `count` nodes, or not, and return accordingly.
        :param cm: the CM (component manager) at which to add nodes.
        """
        result = self.should_add_nodes(count=count,cm=cm)
        if not result:
            return
        
        if type(result) == int:
            LOG.info("trying to add %d nodes..." % (result,))
            ndict = self.get_add_args(count=result)
            LOG.info("trying to add %d nodes (%s)..." % (result,str(ndict)))
        elif type(result) == dict:
            ndict = result
            LOG.info("trying to add nodes %s..." % (str(ndict),))
        else:
            raise Exception("return value from should_add_nodes must be int or dict")
        
        manifest = self._add_nodes(nodes=ndict,cm=cm)
        if not manifest:
            LOG.error("could not add new nodes: %s (%s)"
                      % (manifest,ndict))
            pass
            
        return manifest
    
    def _delete_nodes(self,nodelist,cm=None):
        return self.server.delete_nodes(nodes=nodelist,cm=cm)

    @ApplicableMethod(alias='managed_should_delete_nodes')
    def should_delete_nodes(self,cm=None):
        """
        Should we delete a node?  If so, this function must return a
        list of nodes to delete.  This list is exactly the arguments
        that CM::DeleteNodes expects.  If there is nothing to do, it can
        return None or False.
        
        :param cm: the CM (component manager) at which to add nodes.
        """
        return False
    
    def delete_nodes(self,cm=None):
        """
        This function handles the deletion of the nodes in @nodelist.  This
        manager will call it to delete nodes from this slice, as necessary.
        """
        result = self.should_delete_nodes(cm=cm)
        if not result or len(result) <= 0:
            return None
        elif type(result) != list:
            raise Exception("return value from should_delete_nodes must be None or a list")
        manifest = self._delete_nodes(result,cm=cm)
        if not manifest:
            LOG.error("could not delete existing nodes: %s (%s)"
                      % (manifest,str(result)))
            pass
        return manifest
    
    def get_system_state(self):
        """
        Allows this client to pull in state from a system that is using or
        interacting with the dynamic resources this client has obtained for it.
        If you are creating a dynamic experiment for yourself, this may well
        be a noop.  However, if you are allowing the resources to be used by
        something else (like another cluster, or cluster management system), you
        might want to query it to get its state so that state can be taken
        into account in add_nodes or delete_nodes.
        
        For instance, if you are using this client to manage the addition or
        subtraction of nodes into Slurm, you will want to grab Slurm's job queue
        for each node, or something.
        """
        pass
    
    def update_sliver_status(self):
        """
        This function updates sliver status.
        """
        pass
    
    def update_all(self):
        """
        This function updates the details of all nodes at the CMs this manager
        has been told to use, even those nodes that are not currently available.
        """
        pass
    
    def update_available(self):
        """
        This function updates the details of currently available nodes at the
        CMs this manager has been told to use.  It is a cheap way to obtain the
        free node list for heavily-utilized clusters.
        """
        pass
    
    def set_system_state(self):
        """
        Allows this client to push its state (i.e., resource values Cloudlab
        has pushed to it) to the "system" it is integrating with (i.e. Slurm).
        """
        pass
    
    def manage_once(self,force=False):
        """
        Performs one management cycle.  A management cycle is all the logic
        needed to do to choose, for this cycle, whether or not to add or
        delete any nodes from the slice.  If @force is specified, it performs
        each step in its algorithm, regardless of if the specified time
        interval between core operations has been reached.  Otherwise, the core
        operations are only performed at the intervals specified for each.
        """
        raise exc.NotImplementedError("manage_once")

    def manage(self):
        """
        Runs manage_once infinitely, aborting only on a fatal error, or on
        user interrupt.
        """
        pass

    pass

#
# XXX: need to lock between the functions above, and the manager below...
# createSlice, createSliver, renewSlice, addNodes, deleteNodes obviously stomp
# on each other, potentially; they are writers of the slice/sliver.  Others are
# read-only (but might error due to held lock at the server...).
#
class SimpleElasticSliceManager(ElasticSliceManager,
                                PluginElasticSliceHelper,
                                SimpleElasticSliceClientEndpoint):
    
    DEF_ORDER = [ 'ensure_slice','ensure_sliver','update_sliver_status',
                  'renew','update_all','update_available','get_system_state',
                  'add_nodes','delete_nodes','set_system_state' ]
    CM_METHODS = [ 'ensure_sliver','update_sliver_status',
                   'update_available','update_all','add_nodes','delete_nodes' ]
    METHOD_RESET_TRIGGERS = \
        dict(add_nodes=[ 'update_sliver_status','update_available',
                         'get_system_state','set_system_state' ],
             delete_nodes=[ 'update_sliver_status','update_available',
                            'get_system_state','set_system_state' ])
    DEF_INTERVALS = dict(ensure_slice=MINUTE,ensure_sliver=MINUTE,
                         update_sliver_status=MINUTE,renew=HOUR,
                         update_available=10 * MINUTE,update_all=DAY,
                         add_nodes=5 * MINUTE,delete_nodes=5 * MINUTE)
    DEF_INTERVAL = MINUTE
    DEF_MIN_THRESHOLD = 1
    DEF_MAX_THRESHOLD = 10
    
    """
    A manager of a dynamic slice that creates a slice (and its sliver);
    adds nodes when possible (i.e., when usage is under some specified
    threshold); optionally deletes nodes when the free threshold is
    breached; and renews the slice and sliver.  The user really should
    provide a Helper to manage the semantics of the experiment.
    
    ElasticSliceManager locks using a per-instance threading.RLock.
    Thus, once one thread has the lock, they're good.
    
    If you subclass SimpleElasticSliceManager, and want to replace its
    helper functions, it is very important that you first subclass
    whatever helper class you're extending, then
    SimpleElasticSliceManager.  For instance,
    
        class FooManager(SimpleElasticSliceHelper,SimpleElasticSliceManager):
    
    Why?  Because that makes the method resolution order inside of FooManager
    be FooManager, SimpleElasticSliceHelper, SimpleElasticSliceManager ... .
    If you instead tried to do the reverse:
    
        class FooManager(SimpleElasticSliceManager,SimpleElasticSliceHelper):
    
    the method resolution order would be FooManager, SimpleElasticSliceManager,
    SimpleElasticSliceHelper.  Because SimpleElasticSliceManager provides an
    implementation of each method in the ElasticSliceHelper interface, those
    methods will be called, and those from SimpleElasticSliceHelper will never
    be called (which defeats the point, that you hoped to re-use the helper
    methods from SimpleElasticSliceHelper).  So consider method resolution
    order!  Helpers are not mixins, because an ElasticSliceManager is also
    an ElasticSliceHelper!  This is an OO multiple inheritance style, not a
    mixin style.
    """
    def __init__(self,server,config=None,
             minthreshold=DEF_MIN_THRESHOLD,maxthreshold=DEF_MAX_THRESHOLD,
             percent_available_minimum=0.50,
             manage_order=DEF_ORDER,manage_intervals=DEF_INTERVALS,
             method_reset_triggers=METHOD_RESET_TRIGGERS,
             retry_interval=MINUTE/2,
             nodetype=None,cmlist=None,enable_delete=False,email=True):
        
        ElasticSliceManager.__init__(self,server=server)
        PluginElasticSliceHelper.__init__(self,server=server)
        
        self.mlock = threading.RLock()
        #
        # Tricky: we are both a manager and a manager client endpoint;
        # call super().__init__ accordingly!  And we want the endpoint
        # to use our lock to avoid deadlock, since it calls our methods
        # :-/.  That means we have to be careful not to lock for a long
        # time in any manager operations; we don't want to block the
        # endpoint from getting called by the server.
        #
        SimpleElasticSliceClientEndpoint.__init__(self,rlock=self.mlock)

        self.methoddata = {}
        self.aggdata = {}
        self.baseRspec = None
        
        self.server = server
        self.config = config
        
        LOG.debug("server = %s, config = %s" % (server,config))
        
        self.minthreshold = (config and 'minthreshold' in config.all
                             and config.all['minthreshold']) or minthreshold
        self.minthreshold = int(self.minthreshold)
        self.maxthreshold = (config and 'maxthreshold' in config.all
                             and config.all['maxthreshold']) or maxthreshold
        self.maxthreshold = int(self.maxthreshold)
        self.percent_available_minimum = \
            (config and 'percent_available_minimum' in config.all
             and config.all['percent_available_minimum']) \
            or percent_available_minimum
        self.percent_available_minimum = float(self.percent_available_minimum)
        
        _order = (config and 'manage_order' in config.all
                  and config.all['manage_order']) or manage_order
        if type(_order) is str:
            self.manage_order = []
            sa = _order.split(',')
            for m in sa:
                try:
                    getattr(self,m)
                except:
                    raise Exception("no such method %s in manager!" % (m,))
                self.manage_order.append(m)
                pass
            pass
        else:
            self.manage_order = _order
            pass
        
        _intervals = (config and 'manage_intervals' in config.all
                  and config.all['manage_intervals']) or manage_intervals
        if type(_intervals) is str:
            self.manage_intervals = []
            sa = _intervals.split(',')
            for m in sa:
                try:
                    ma = m.split(':')
                except:
                    raise Exception("bad method interval specification %s" % (m,))
                try:
                    getattr(self,ma[0])
                except:
                    raise Exception("no such method %s in manager!" % (ma[0],))
                self.manage_intervals[ma[0]] = float(ma[1])
                pass
            pass
        else:
            self.manage_intervals = _intervals
            pass
        
        self.retry_interval = \
            (config and 'retry_interval' in config.all
             and config.all['retry_interval']) or retry_interval
        self.method_reset_triggers = method_reset_triggers
        
        self.nodetype = \
            (config and 'nodetype' in config.all
             and config.all['nodetype']) or nodetype
        self.enable_delete = \
            (config and 'enable_delete' in config.all
             and config.all['enable_delete']) or enable_delete
        self.email = \
            (config and 'email' in config.all
             and config.all['email']) or email
        self.default_cm = server.default_cm
        if cmlist is None:
            self.cmlist = [self.default_cm,]
        else:
            self.cmlist = cmlist
            pass
    
        # Set up our method metadata with some defaults
        cmdict = {}
        non_cmdict = {}
        for method in self.manage_order:
            if method in SimpleElasticSliceManager.CM_METHODS:
                cmdict[method] = dict(start=None,stop=None,result=None)
            else:
                non_cmdict[method] = dict(start=None,stop=None,result=None)
            pass
        for cm in self.cmlist:
            self.methoddata[cm] = dict(cmdict)
            pass
        # Special case for non-CM methods
        self.methoddata[None] = dict(non_cmdict)

        # Set up our aggregate (CM) databases.
        for cm in self.cmlist:
            self.aggdata[cm] = \
              dict(node_map={},allxml='',availxml='',all=None,avail=None,
                   status={},node_status={},adding={},deleting={})
            pass
        
        # prepare for stoppable run feature
        self._stop         = threading.Event()
        self._thread       = None
        
        pass
    
    def get_email(self):
        if self.email == None:
            return None
        elif type(self.email) == str and self.email.find('@') > -1:
            return self.email
        # autodetect one from the server if possible
        elif self.email == True:
            try:
                self.email = self.server.get_user_email()
                return self.email
            except:
                LOG.exception("could not autodetect user email!")
                return None
            pass
        else:
            return None
        pass
    
    def get_node_status(self,node,cm=None,key=None):
        """
        Get either the status dictionary for the given virtual node name
        @node, or if @key is specified, retrieve that key.
        """
        if not cm: cm = self.default_cm
        
        if not node in self.aggdata[cm]['node_status']:
            return None
        if key:
            if not key in self.aggdata[cm]['node_status'][node]:
                return None
            else:
                return self.aggdata[cm]['node_status'][node][key]
        else:
            return self.aggdata[cm]['node_status'][node]
        pass
    
    def update_sliver_status(self,cm=None,force=False):
        """
        Ok, now grab our sliver status to see the nodes we have at @cm,
        and what their status is.
        """
        if not cm: cm = self.default_cm
        
        wm = self.server.get_wrapped_manifest(cm=cm)
        last_node_map = self.aggdata[cm]['node_map']
        self.aggdata[cm]['node_map'] = {}
        last_node_status = self.aggdata[cm]['node_status']
        self.aggdata[cm]['node_status'] = {}
        ss = self.server.sliver_status(cm=cm,force=force)
        last_status = self.aggdata[cm]['status']
        self.aggdata[cm]['status'] = ss
        for sliver_id in ss['details']:
            if "client_id" in ss['details'][sliver_id]:
                cid = ss['details'][sliver_id]['client_id']
                # Only put nodes in here, not other kinds of components
                # like links
                if not cid in wm.nodenames:
                    continue
                pnid = ss['details'][sliver_id]['component_urn']
                self.aggdata[cm]['node_map'][cid] = pnid
                self.aggdata[cm]['node_status'][cid] = ss['details'][sliver_id]
                status = ss['details'][sliver_id]['status']
                if cid in self.aggdata[cm]['adding'] and status == 'ready':
                    delta = time.time() - self.aggdata[cm]['adding'][cid]
                    LOG.info("finished adding node %s is now ready after %d seconds"
                             % (cid,delta))
                    del self.aggdata[cm]['adding'][cid]
                    self.handle_added_node(ss['details'][sliver_id],status)
                    pass
                pass
            pass
        todelete = []
        for (k,v) in self.aggdata[cm]['deleting'].iteritems():
            if not k in self.aggdata[cm]['node_map']:
                delta = time.time() - v
                LOG.info("finished deleting node %s after %d seconds" % (k,delta))
                todelete.append(k)
                pass
            pass
        for k in todelete:
            del self.aggdata[cm]['deleting'][k]
            nd = None
            for (sliver_id,nsd) in last_status['details'].iteritems():
                if 'client_id' in nsd and nsd['client_id'] == k:
                    nd = nsd
                    break
                pass
            if not nd:
                nd = { 'client_id':k }
                pass
            self.handle_deleted_node(nd)
            pass
        if LOG.isEnabledFor(logging.DEBUG):
            LOG.debug("Node list:")
            for (cid,pnid) in self.aggdata[cm]['node_map'].iteritems():
                LOG.debug("  %s -> %s" % (cid,pnid))
                pass
            pass
        if last_node_map:
            delta = len(self.aggdata[cm]['node_map']) \
                - len(last_node_map)
            LOG.debug("Net node change: %d" % (delta,))
            pass
        pass
    
    def update_available(self,cm=None):
        if not cm: cm = self.default_cm
        
        cn = self.server.get_cache_notifier()
        xml = self.server.get_resources(cm=cm,available=True,cache_notifier=cn)
        # Only do this if we have no data yet, or if the data is stale
        if not self.aggdata[cm]['availxml'] or not self.aggdata[cm]['avail'] \
          or cn.updated or not cn.cached:
            self.aggdata[cm]['availxml'] = xml
            ad = RSpecAd.Advertisement(xml=xml)
            self.aggdata[cm]['avail'] = ad
        else:
            ad = self.aggdata[cm]['avail']
            pass
                    
        # a little processing... just to calculate CM fullpercent
        # but this time only count avail and use existing total
        # to calculate percent
        avail = 0
        nt_avail = 0
        for n in ad.nodes:
            if n.available:
                avail += 1
            if self.nodetype and n.hardware_types.has_key(self.nodetype):
                if n.available:
                    nt_avail +=1
                pass
            pass
        self.aggdata[cm]['navail'] = avail
        self.aggdata[cm]['npercentavail'] = \
            float(avail) / float(self.aggdata[cm]['ntotal'])
        self.aggdata[cm]['ntavail'] = nt_avail
        if self.aggdata[cm]['nttotal'] > 0:
            self.aggdata[cm]['ntpercentavail'] = \
                float(nt_avail) / float(self.aggdata[cm]['nttotal'])
        else:
            self.aggdata[cm]['ntpercentavail'] = 0.0
        
        LOG.debug("AVAILABLE: total=%d avail=%d (%f %%), %s total=%d"
                  " avail=%d (%f %%)"
                  % (self.aggdata[cm]['ntotal'],self.aggdata[cm]['navail'],
                     self.aggdata[cm]['npercentavail'] * 100.0,
                     str(self.nodetype),self.aggdata[cm]['nttotal'],
                     self.aggdata[cm]['ntavail'],
                     self.aggdata[cm]['ntpercentavail'] * 100.0))
        pass
    
    def update_all(self,cm=None,force=False,lastFetchBefore=None):
        if not cm: cm = self.default_cm
        
        if force or not lastFetchBefore \
          or (self.aggdata[cm]['update_all']['start'] < lastFetchBefore):
            try:
                cn = self.server.get_cache_notifier()
                # Only do this if we have no data yet, or if the data is stale
                if not self.aggdata[cm]['allxml'] or not self.aggdata[cm]['all'] \
                  or cn.updated or not cn.cached:
                    xml = self.server.get_resources(cm=cm,available=False)
                    self.aggdata[cm]['allxml'] = xml
                    ad = RSpecAd.Advertisement(xml=xml)
                    self.aggdata[cm]['all'] = ad
                else:
                    ad = self.aggdata[cm]['all']
                    pass
                
                # a little processing... just to calculate CM fullpercent
                total = 0
                avail = 0
                nt_total = 0
                nt_avail = 0
                for n in ad.nodes:
                    total += 1
                    if n.available:
                        avail += 1
                    if self.nodetype and n.hardware_types.has_key(self.nodetype):
                        nt_total += 1
                        if n.available:
                            nt_avail +=1
                        pass
                    pass
                self.aggdata[cm]['ntotal'] = total
                self.aggdata[cm]['navail'] = avail
                self.aggdata[cm]['npercentavail'] = float(avail) / float(total)
                self.aggdata[cm]['nttotal'] = nt_total
                self.aggdata[cm]['ntavail'] = nt_avail
                if self.aggdata[cm]['nttotal'] > 0:
                    self.aggdata[cm]['ntpercentavail'] = \
                        float(nt_avail) / float(nt_total)
                else:
                    self.aggdata[cm]['ntpercentavail'] = 0.0
                
                LOG.debug("AVAILABLE: total=%d avail=%d (%f %%), %s total=%d"
                          " avail=%d (%f %%)"
                          % (self.aggdata[cm]['ntotal'],
                             self.aggdata[cm]['navail'],
                             self.aggdata[cm]['npercentavail'] * 100.0,
                             str(self.nodetype),
                             self.aggdata[cm]['nttotal'],
                             self.aggdata[cm]['ntavail'],
                             self.aggdata[cm]['ntpercentavail'] * 100.0))
                pass
            except:
                LOG.exception("could not get resources at CM %s!" % (cm,))
            pass
        pass
    
    def _handle_method_reset_triggers(self,method,cm=None):
        if not cm: cm = self.default_cm
        
        if method in self.method_reset_triggers:
            start = time.time()
            for mrt in self.method_reset_triggers[method]:
                if not mrt in self.methoddata[cm]:
                    continue
                interval = self._get_method_interval(method)
                self.methoddata[cm][mrt]['stop'] = start - interval
                LOG.info("reset method %s to run immediately because %s"
                         " was run successfully"
                         % (mrt,method))
                pass
            pass
        pass
    
    def _get_method_interval(self,method):
        return (method in self.manage_intervals \
                and self.manage_intervals[method]) \
               or SimpleElasticSliceManager.DEF_INTERVAL
    
    def _compute_manage_sleep_time(self):
        """
        Compute a minimum amount of time the manage() method should wait
        before running another method.  Said another way, return the
        amount of time until the next nearest pending method should run.
        """
        mintime = 0xffffffff
        ctime = time.time()
        for cm in self.methoddata.keys():
            for method in self.methoddata[cm].keys():
                interval = self._get_method_interval(method)
                # If the method hasn't been run yet, don't sleep.
                if self.methoddata[cm][method]['start'] is None \
                  or self.methoddata[cm][method]['start'] is None:
                    mintime = 0.0
                    break
                # Otherwise, if the last one succeeded, wait until its interval
                # will have passed:
                elif self.methoddata[cm][method]['stop'] \
                  > self.methoddata[cm][method]['start']:
                    t = (self.methoddata[cm][method]['start'] + interval) - ctime
                    if t > 0 and t < mintime:
                        mintime = t
                    pass
                # Otherwise, if the last one failed, wait until a retry interval
                # will have passed:
                elif self.methoddata[cm][method]['start'] \
                  > self.methoddata[cm][method]['stop']:
                    t = (self.methoddata[cm][method]['start']
                         + self.retry_interval) - ctime
                    if t > 0 and t < mintime:
                        mintime = t
                    pass
                pass
            pass
        return mintime
    
    def _is_manage_method_runnable(self,method,cm=None):
        """
        Check if a given method should be run for a given CM.
        """
        _cm = cm
        if not _cm and method in SimpleElasticSliceManager.CM_METHODS:
            _cm = self.default_cm
            pass

        ctime = time.time()
        interval = self._get_method_interval(method)
        if self.methoddata[_cm][method]['start'] is None \
          or self.methoddata[_cm][method]['stop'] is None:
            return True
        elif (self.methoddata[_cm][method]['stop']
              - self.methoddata[_cm][method]['start']) > interval:
            return True
        elif (ctime - self.methoddata[_cm][method]['start']) \
          > self.retry_interval:
            return True
        else:
            return False
        pass
    
    def _run_manage_method(self,method,cm=None,**kwargs):
        """
        Run a given method on a given CM.
        """
        _cm = cm
        if not _cm and method in SimpleElasticSliceManager.CM_METHODS:
            _cm = self.default_cm
            pass
        
        start = time.time()
        self.methoddata[_cm][method]['start'] = start
        self.methoddata[_cm][method]['result'] = None
        ret = None
        try:
            LOG.debug('running %s @ %s' % (method,_cm))
            m = getattr(self,method)
            if method in SimpleElasticSliceManager.CM_METHODS:
                ret = m(cm=_cm,**kwargs)
            else:
                ret = m(**kwargs)
                pass
            self.methoddata[_cm][method]['result'] = ret
            self.methoddata[_cm][method]['stop'] = time.time()
            return ret
        except:
            e = sys.exc_value
            LOG.exception("%s @ %s" % (method,_cm))
            raise e
        pass

    def _manage_should_yield(self):
        if self._stop and self._stop.isSet():
            LOG.info("manage interrupted")
            return True
        else:
            return False
        pass

    def renew(self):
        #
        # Renew the slice (and sliver).
        #
        try:
            ctime = time.time()
            ets = self.server.get_slice_expiration()
            if (ets - ctime) < (6 * HOUR):
                self.server.renew_slice(additional_seconds=DAY,
                                        cmlist=self.cmlist)
                #self.server.renew_sliver(cmlist=self.cmlist)
                pass
            pass
        except:
            if self.get_email():
                SENDMAIL(self.get_email(),
                         "elasticslice: Error renewing slice"
                         "Error in elasticslice renewing slice:\n\n%s" \
                             % (traceback.format_exc(),))
                pass
            LOG.exception("could not renew slice '%s'; continuing; will try again later!")
            pass

        pass
    
    def manage_once(self):
        """
        This is a function that runs all the manage methods, in order,
        that we were configured to run.  XXX: Currently, we catch
        exceptions from any individual function and keep going; probably
        need something better.
        """
        for method in self.manage_order:
            if method in SimpleElasticSliceManager.CM_METHODS:
                for cm in self.cmlist:
                    if self._is_manage_method_runnable(method,cm):
                        try:
                            ret = self._run_manage_method(method,cm)
                        except:
                            LOG.exception("%s @ %s" % (method,cm))
                            pass
                        pass
                    if self._manage_should_yield():
                        return
                    pass
                pass
            elif self._is_manage_method_runnable(method):
                try:
                    ret = self._run_manage_method(method)
                except:
                    LOG.exception("%s" % (method,))
                    pass
                if self._manage_should_yield():
                    return
                pass
            pass
        pass
    
    def is_adding(self,node=None,cm=None):
        if not cm: cm = self.default_cm
        
        if not node:
            if len(self.aggdata[cm]['adding']):
                return True
            else:
                return False
        else:
            if node in self.aggdata[cm]['adding']:
                return True
            else:
                return False
            pass
        pass
    
    def is_deleting(self,node=None,cm=None):
        if not cm: cm = self.default_cm
        
        if not node:
            if len(self.aggdata[cm]['deleting']):
                return True
            else:
                return False
        else:
            if node in self.aggdata[cm]['deleting']:
                return True
            else:
                return False
            pass
        pass
    
    def _add_nodes(self,nodes={},cm=None):
        if not cm: cm = self.default_cm
        
        manifest = super(SimpleElasticSliceManager,self)._add_nodes(
            nodes=nodes,cm=cm)
        if manifest:
            atime = time.time()
            for (k,v) in nodes.iteritems():
                self.aggdata[cm]['adding'][k] = atime
            pass
        
        return manifest

    def should_add_nodes(self,count=0,cm=None):
        if not cm: cm = self.default_cm
        
        LOG.debug("should_add_nodes: cm=%s" % (str(cm)))
        
        if not 'ntotal' in self.aggdata[cm]:
            self.update_all()
            pass
        if not 'status' in self.aggdata[cm]['status']:
            self.update_sliver_status()
            pass
        
        #
        # See if we need to add more nodes to get to our threshold; if
        # so, see if any are available, and add a few if so.  Otherwise
        # wait N minutes and try again, depending on the error.
        #
        if not 'node_map' in self.aggdata[cm] \
          or len(self.aggdata[cm]['node_map']) >= self.maxthreshold:
            return False

        # XXX: fix
        #
        #start_time = time.time()
        #
        ## Make sure we updated this cycle
        #if self.aggdata[cm]['update_available']['start_time']
        #self.update_available(force=True)Available(lastFetchBefore=start_time)
        # status too
        #self.updateStatus(lastFetchBefore=start_time)
        
        if self.is_adding(cm=cm):
            LOG.debug("cm=%s already adding, skipping" % (cm,))
            return False
        
        #
        # Ok, we can add nodes if we have a type and our
        # type avail percent is under the threshold (WITH
        # one more node), OR if we don't have a type and our
        # avail percent is under the threshold (WITH one
        # more node).
        #
        if self.aggdata[cm]['status']['status'] == 'ready' \
          and ((self.nodetype \
                and ('ntpercentavail' in self.aggdata[cm] \
                     and (self.aggdata[cm]['ntpercentavail'] \
                          - (1.0 / self.aggdata[cm]['nttotal'])) \
                         > self.percent_available_minimum)) \
               or (not self.nodetype \
                   and ('ntpercentavail' in self.aggdata[cm] \
                        and (self.aggdata[cm]['npercentavail'] \
                             - (1.0 / self.aggdata[cm]['ntotal'])) \
                            > self.percent_available_minimum))):
            # Just add nodes 1 by 1.
            return 1
        else:
            return False
        pass
    
    def add_nodes(self,count=0,cm=None):
        retval = super(SimpleElasticSliceManager,self).add_nodes(count=count,cm=cm)
        if not retval:
            return None
        else:
            LOG.debug("resetting add_nodes triggers")
            self._handle_method_reset_triggers('add_nodes')
        return retval
    
    def _delete_nodes(self,nodelist,cm=None):
        if not cm:
            cm = self.default_cm
        
        manifest = super(SimpleElasticSliceManager,self)._delete_nodes(
            nodelist,cm=cm)
        if manifest:
            atime = time.time()
            for k in nodelist:
                if k in self.aggdata[cm]['adding']:
                    LOG.warn("deleting a node still in adding: %s!" % (k,))
                    del self.aggdata[cm]['adding'][k]
                    pass
                self.aggdata[cm]['deleting'][k] = atime
            pass
        
        return manifest

    def should_delete_nodes(self,cm=None):
        if not cm:
            cm = self.default_cm
        
        if self.is_deleting(cm=cm):
            LOG.debug("cm=%s already deleting, skipping" % (cm,))
            return False
        if self.is_adding(cm=cm):
            LOG.debug("cm=%s currently adding, not deleting" % (cm,))
            return False
        
        if not 'ntotal' in self.aggdata[cm]:
            self.update_all()
            pass
        if not 'status' in self.aggdata[cm]['status']:
            self.update_sliver_status()
            pass
        
        if self.enable_delete and len(self.aggdata[cm]['adding']) == 0 \
          and self.aggdata[cm]['status']['status'] == 'ready' \
          and len(self.aggdata[cm]['node_map']) > self.minthreshold \
          and ((self.nodetype \
                and self.aggdata[cm]['ntpercentavail'] \
               < self.percent_available_minimum) \
              or (not self.nodetype \
                  and (self.aggdata[cm]['npercentavail'] \
                       < self.percent_available_minimum))):
            #
            # Pick one (at random) to delete!
            #
            llen = len(self.aggdata[cm]['node_map'].keys())
            if llen > 0:
                idx = random.sample(range(0,llen),1)[0]
                cid = self.aggdata[cm]['node_map'].keys()[idx]
                LOG.debug("Should delete node %s" % (cid,))
                return [cid]
            pass
        # Otherwise, don't delete anything.
        return False
    
    def delete_nodes(self,cm=None):
        retval = super(SimpleElasticSliceManager,self).delete_nodes(cm=cm)
        if not retval:
            return None
        else:
            LOG.debug("resetting delete_nodes triggers")
            self._handle_method_reset_triggers('delete_nodes')
        return retval
    
    def manage(self):
        emailaddr = self.get_email()
        
        while True:
            start_time = time.time()

            try:
                self.manage_once()
            except NonexistentSliceError,e:
                if emailaddr:
                    SENDMAIL(emailaddr,
                             "elasticslice: Error resolving/creating slice",
                             "Error in elasticslice resolving/creating slice:\n\n%s"
                             % (traceback.format_exc(),))
                    pass
            except NonexistentSliverError,e:
                if emailaddr:
                    SENDMAIL(emailaddr,
                             "elasticslice: Error resolving/creating sliver",
                             "Error in elasticslice resolving/creating sliver:\n\n%s"
                             % (traceback.format_exc(),))
                    pass
            except:
                SENDMAIL(emailaddr,
                         "elasticslice: Error in manage_once()",
                         "Error in manage_once():\n\n%s"
                         % (traceback.format_exc(),))
                pass

            if self._manage_should_yield():
                return

            nsec = self._compute_manage_sleep_time()
            LOG.info("Manager pausing for %f seconds..." % (nsec,))
            rsec = nsec
            while rsec > 0:
                if rsec > 1:
                    ssec = 1
                else:
                    ssec = rsec
                    pass
                time.sleep(ssec)
                rsec -= ssec
                if self._manage_should_yield():
                        return
                pass
            LOG.info("Manager unpausing...")
            pass
                
        pass
    
    def stop(self):
        if self._thread:
            self._stop.set()
            self._thread.join()
            # reset
            self._thread = None
            self._stop = threading.Event()
            pass
        pass
    
    def start(self):
        if self._thread:
            raise Exception("already handling requests in separate thread")
        else:
            name = "manager[daemon]"
            self._thread = threading.Thread(target=self.manage,name=name)
            self._thread.start()
        pass

    pass
