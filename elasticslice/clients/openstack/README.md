The openstack elasticslice support is currently maintained in a separate
repository: https://gitlab.flux.utah.edu/johnsond/openstack-build-ubuntu .

More specifically, see the `osp-dynamic.py` file in the top-level dir in
that repository.
