import time
import re
import ast
import logging
from shutil import copyfile
from elasticslice.managers.core import SimpleElasticSliceHelper, \
    SimpleElasticSliceManager
from elasticslice.util.util import ShellCommand
from elasticslice.util.log import configure_file_logging
from elasticslice.util.util import get_file_contents, push_local_file_to_remote_node

SECOND = 1
MINUTE = SECOND * 60
HOUR   = MINUTE * 60
DAY    = HOUR * 24
MONTH  = DAY * 31
YEAR   = DAY * 365

LOG = logging.getLogger(__name__)

#
# NB: it is very important that, as documented, SimpleElasticSliceHelper
# is the first class here, so that its methods override those of
# SimpleElasticSliceManager.  Why?  Because SimpleElasticSliceManager is
# also a helper -- it is a PluginElasticSliceHelper, which means it
# accepts a plugin helper, and doesn't have any functionality of its own
# (other than to call the the plugin helper object's methods).
#
class SlurmDynamicManager(SimpleElasticSliceHelper,SimpleElasticSliceManager):
    
    # Important: 'update_all' should precede 'update_available'
    #DEF_ORDER = [ 'ensure_slice','ensure_sliver','update_sliver_status',
    #              'renew','update_all','update_available','get_system_state',
    #              'add_nodes','delete_nodes','set_system_state' ]

    #DEF_INTERVALS = dict(ensure_slice=MINUTE,ensure_sliver=MINUTE,
    #                     update_sliver_status=MINUTE,renew=HOUR,
    #                     update_available=10 * MINUTE,update_all=DAY,
    #                     add_nodes=5 * MINUTE,delete_nodes=5 * MINUTE)

    DEF_ORDER = [ 'ensure_slice','ensure_sliver','update_sliver_status',
                  'renew','update_all','update_available','get_system_state',
                  'add_nodes','delete_nodes','set_system_state' ]
    CM_METHODS = [ 'ensure_sliver','update_sliver_status',
                   'update_available','update_all','add_nodes','delete_nodes' ]
    METHOD_RESET_TRIGGERS = \
        dict(add_nodes=[ 'update_sliver_status','update_available',
                         'get_system_state','set_system_state' ],
             delete_nodes=[ 'update_sliver_status','update_available',
                            'get_system_state','set_system_state' ])
    DEF_INTERVALS = dict(ensure_slice=MINUTE,ensure_sliver=MINUTE,
                         update_sliver_status=MINUTE,renew=HOUR,
                         update_available=10 * MINUTE,update_all=DAY,
                         add_nodes=5 * MINUTE,delete_nodes=5 * MINUTE)
    DEF_INTERVAL = MINUTE
    DEF_MIN_THRESHOLD = 1
    DEF_MAX_THRESHOLD = 10

    def __init__(self,server,config=None):
        SimpleElasticSliceHelper.__init__(self,server,config=config)
        SimpleElasticSliceManager.__init__(self,server,config=config, 
					   manage_order=SlurmDynamicManager.DEF_ORDER,
					   manage_intervals=SlurmDynamicManager.DEF_INTERVALS)

        # Define the name of the existing experiment network
        self.exp_lan = 'link-1'

        # Add SLURM related capabilities 
        self.slurm = SlurmScheduler(config=self.config)

        # Add Chef/knife wrapper
        self.knife = KnifeWrapper(config=self.config)

        # File logger for special events  
        configure_file_logging('file_log', '/tmp/SlurmDynamicManager.log')
        self.file_log = logging.getLogger('file_log')
        self.file_log.debug("Created file logging for special events")

    def get_add_args(self,count=1):
        self.file_log.debug("Running get_add_args()")
        from xml.sax.saxutils import escape
        
        # If we didn't generate an rspec for a sliver creation,
        # initialize our state from the manifest.
        if not self.generated:
            wrapped_pgmanifest = self.server.get_wrapped_manifest()
            
            #self._set_state_from_manifest(wrapped_pgmanifest)
            #
            # Instead of always incrementing self.idx, fill the gaps if there are any:
            used_ids = []
            for node in wrapped_pgmanifest.nodes:
                cid = node.name
                if cid.startswith(self.node_prefix):
                    used_ids.append(int(cid[(len(self.node_prefix)+1):]))
            min_id = min(used_ids)
            max_id = max(used_ids)
            fullrange_ids = range(min_id, max_id + 1)
            missing_ids = list(set(fullrange_ids) - set(used_ids))
            missing_ids.sort()
            if len(missing_ids) >= count:
                ids = missing_ids[:count]
            else:
                need_to_add = count - len(missing_ids)
                ids = missing_ids + range(max_id + 1, max_id + 1 + need_to_add)
        
        retval = {}
        for i in ids:
            nn = "%s-%d" % (self.node_prefix,i)
            
            # lans = []
            # for j in range(0,self.num_lans):
            #     lans.append({ 'name' : "%s-%s" % (self.lan_prefix,j) })
            #     pass
            #
            # Different from superclass in that it adds a single experimental lan with a specified name:
            lans = [self.exp_lan]
            
            nd = {}
            if self.image_urn:
                nd["diskimage"] = self.image_urn
            
            if self.tarballs:
                ### nd["tarballs"] = tarballs
                # Tarballs list specified in the config file will appear as a string
                if isinstance(self.tarballs, str):
                    # Example of tarballs string in the config file:
                    #    [['https://s3-us-west-2.amazonaws.com/cloudlab-stable/hba.tar.gz','/root']]
                    # Need to parse the string and and pass it a list of 2-member lists
                    nd["tarballs"] = ast.literal_eval(self.tarballs.lstrip('\'').rstrip('\''))
            
            if self.startup_command:
                nd["startup"] = escape(self.startup_command.lstrip('\'').rstrip('\''))

            if self.nodetype:
                nd['hardware_type'] = self.nodetype
            if len(lans) > 0:
                nd["lans"] = lans
                pass
            retval[nn] = nd
            pass
        #self.idx += count
        self.file_log.debug("Adding nodes with the following attributes: %s" % retval)
        return retval

    def get_system_state(self):
        jc = self.slurm.get_job_count()
        LOG.info("Number of SLURM jobs (running and pending): %d" % jc)  
        self.file_log.debug("Running get_system_state()")
        pass

    def set_system_state(self):
        self.file_log.debug("Running set_system_state()")
        pass

    def handle_added_node(self,node,status):
        """
        This method should be called by a manager when it sees that an
        added node has become "ready" or "failed".  @node is a dict like
        
          { 'client_id':'node-1','node_id':'pc100',...}
        
        and @status is either 'ready', 'failed'.  The return value is
        not checked by the caller.
        """
        self.file_log.debug("Handling added node: %s; Status: %s" % (node, status))

        self.update_etc_hosts()
        all_nodes = self.get_nodenames()
        self.slurm.update_slurm_config(node_list=all_nodes)

        code,output = self.knife.bootstrap_node(node=node['client_id'], run_list='role[nfs],role[slurm]')
        self.file_log.debug("Bootstrapped a new node %s with Chef. Code: %s; Output: %s" % (node['client_id'], code, output))
        
        # Push locally updated files to all nodes (including the new one)
        for n in all_nodes:
            push_local_file_to_remote_node("/etc/hosts", n)
            push_local_file_to_remote_node("/etc/slurm-llnl/slurm.conf", n)

        code,output = self.knife.run_cmd_on_all_clients(cmd_str="service slurm-llnl restart",retries=1)
        self.file_log.debug("Restarting SLURM deamons. Code: %s; Output: %s" % (code, output))
        pass
    
    def handle_deleted_node(self,node):
        """
        This method should be called by a manager when it sees that an
        added node has become "ready" or "failed".  @node is a dict like
        
          { 'client_id':'node-1','node_id':'pc100',...}
        
        The return value is not checked by the caller.
        """
        self.file_log.debug("Handling deleted node: %s" % node)

        self.update_etc_hosts()
        self.slurm.update_slurm_config(node_list=self.get_nodenames())

        code,output = self.knife.purge_node(node=node['client_id'])
        self.file_log.debug("Purging node %s from Chef. Code: %s; Output: %s" % (node['client_id'], code, output))
        pass

    def get_nodenames(self, include_IPs=False, include_fullnames=False):
        """
        This method returns a list of short (virtual) nodenames for all nodes in the experiment.
        It can optionally also include external (physical) nodenames, 
        as well as internal (experiment) IP addresses. 
        """
        wrapped_pgmanifest = self.server.get_wrapped_manifest()
        result = []
        for node in wrapped_pgmanifest.nodes:
            name = node.name
            line = ""
            if include_IPs:
                line += wrapped_pgmanifest.nodenames[name].interfaces[0].address_info[0] + " "
            if include_fullnames:
                line += wrapped_pgmanifest.getHostnameAndPort(name)[0] + " "
            line += name
            result.append(line)
        return result

    def update_etc_hosts(self):
        """
        This method updates /etc/hosts file. It adds a block of entries for all experiment nodes 
        (obtained using the get_nodenames() funcion) at the end of the file, unless it is already there.
        A special marker is used to determine whether such block is present or not.
        This method preserves all manually entered entries before the auto-generated block.
        """    
        MARKER = "# AUTO-GENERATED BLOCK - DO NOT EDIT BY HAND"
        END_LINE = "# END OF AUTO-GENERATED BLOCK - DO NOT ADD LINES BELOW"
        TMP_FILE = "/tmp/.new_hosts"

        contents = get_file_contents("/etc/hosts", cleanup=True)
        new_block = [MARKER] + self.get_nodenames(include_IPs=True, include_fullnames=True) + [END_LINE]

        detected_change = False
        if MARKER in contents:   
            
            pos = contents.index(MARKER)
            old_block = contents[pos:]
            if set(old_block) != set(new_block):
                # Old block is really different than the new one
                detected_change = True
                new_contents = contents[:pos] + new_block
        else: 
            detected_change = True
            new_contents = contents + new_block

        if detected_change:
            with open(TMP_FILE, "w") as f: 
                f.write("\n".join(new_contents))

            # Make a backup copy of the old /etc/hosts
            time_suff = time.strftime("-%Y%m%d-%H%M%S")
            copyfile('/etc/hosts', '/etc/hosts' + time_suff)

            # Replace the old file with the new one
            copyfile(TMP_FILE, '/etc/hosts')

class SlurmScheduler(object):
    """Define operations for interacting with the SLURM resource manager and scheduler"""
    def __init__(self, config=None):
        self.config = config
        # List of dislayed fields; all flags are documented at: http://slurm.schedmd.com/squeue.html
        self.squeue_format = "%i,%P,%j,%u,%T,%M,%l,%D,%R,%p,%C,%D,%e"

    def _get_queue_info(self, job_type="RUNNING", show_header=True):
        cmd = ShellCommand("squeue -o %s -t %s" % (self.squeue_format, job_type))
        code,output = cmd.execute()
        job_list = filter(None, output.split('\n'))
        if show_header:
        	return job_list
        else:
            return job_list[1:]

    def _timelimit_to_sec(self, timelimit):
        """ Convert SLURM timelimits in the format "days-hours:minutes:seconds" to seconds;
        handle cases when fields on the left side aren't present 
        """
        t = timelimit
        factor = 1
        total = 0.0
        while t:
            right_value = re.match('.*?([0-9]+)$', t).group(1)
            # crop the entire string
            t = t[:len(t)-len(right_value)].rstrip(':-')
            # Increase total
            total += factor * int(right_value) 
            # Bump up the factor
            if factor == 3600:
                # Switching from hours to days
                factor = 86400
            elif factor < 3600:
                factor *= 60
            elif factor > 86400:
                LOG.error("Timelimit format is incorrect; It appears to go beyond days")
                return 0
        return total

    def queue_info(self):
        """ Return list of dictionaries where every element is a dict """
        pj = self._get_queue_info(job_type="RUNNING", show_header=True)
        rj = self._get_queue_info(job_type="PENDING", show_header=True)
        if pj[0] != rj[0]:
            LOG.error("Mismatch between headers for RUNNING and PENDING jobs in the queue")
            return None
        info = []
        header = pj[0].split(',')
        for j in pj[1:] + rj[1:]:
            job_attrs = j.split(',')
            info.append(dict(zip(header,job_attrs)))
        return info

    def get_future_compute_cycles(self, unit='s'):
        """ Sum up all future compute cycles and convert to the specified units """
        info = self.queue_info()
        total = 0
        for j in info:
            if j['STATE'] == 'PENDING':
                # Add requested walltime
                total += self._timelimit_to_sec(j['TIMELIMIT'])
            elif j['STATE'] == 'RUNNING':
                # Add remaining time
                total += self._timelimit_to_sec(j['TIMELIMIT']) - self._timelimit_to_sec(j['TIME'])
        if unit == 's':
            return float(total)
        elif unit == 'm':
            return total / 60.0
        elif unit == 'h':
            return total / 3600.0
        elif unit == 'd':
            return total / 86400.0    
        else:
            LOG.error("Incorrect unit is used for calculating future cycles")
            return 0.0
            
    def list_pending_jobs(self, show_header=True):
        return self._get_queue_info(job_type="PENDING", show_header=show_header)

    def list_running_jobs(self, show_header=True):
        return self._get_queue_info(job_type="RUNNING", show_header=show_header)

    def get_running_job_ids(self):
        return [j.split(',')[0] for j in self.list_running_jobs(show_header=False)]

    def get_job_count(self):
        pj = self.list_pending_jobs(show_header=False)
        LOG.debug("Pending jobs: %s" % pj)
        rj = self.list_running_jobs(show_header=False)
        LOG.debug("Running jobs: %s" % rj)
        return len(pj) + len(rj)

    def update_slurm_config(self, node_list=[]):
        """
        This method updates SLURM configuration in /etc/slurm-llnl/slurm.conf. 
        It adds an auto-generated block with lines such as "NodeName=" and "PartitionName=" 
        with all current experiment nodes at the end of the file, unless they such block is already there.
        A special marker is used to determine whether such lines are present or not.
        This method preserves all lines that exist in the file before this block.

        Arguments:
        node_list -- list of short (virtual) nodenames to be added in SLURM config; if empty, the method does nothing 
        """  

        if not node_list:
            return

        if "SLURM::partition_name" in self.config.all:
            partition = self.config.all["SLURM::partition_name"]
        else:
            LOG.debug("Config file is missing 'partition_name' key in the 'SLURM' section. Using: 'all'")  
            partition = "all"

        CONF_FILE = "/etc/slurm-llnl/slurm.conf"
        TMP_FILE = "/tmp/.slurm.conf"

        MARKER = "# COMPUTE NODES BLOCK - DO NOT EDIT BY HAND"
        END_LINE = "# END OF COMPUTE NODES BLOCK - DO NOT ADD LINES BELOW"

        node_list_comma_separated = ",".join(node_list)
        NODE_LINE="NodeName=%s State=UNKNOWN" % node_list_comma_separated
        PARTITION_LINE="PartitionName=%s Nodes=%s Default=YES MaxTime=INFINITE State=UP" % (partition, node_list_comma_separated)

        contents = get_file_contents(CONF_FILE, cleanup=True)
        new_block = [MARKER, NODE_LINE, PARTITION_LINE, END_LINE]

        detected_change = False
        if MARKER in contents:   
            pos = contents.index(MARKER)
            old_block = contents[pos:]
            if set(old_block) != set(new_block):
                # Old block is really different than the new one
                detected_change = True
                new_contents = contents[:pos] + new_block
        else: 
            detected_change = True
            new_contents = contents + new_block

        if detected_change:
            with open(TMP_FILE, "w") as f: 
                f.write("\n".join(new_contents))

            # Make a backup copy of the old config
            time_suff = time.strftime("-%Y%m%d-%H%M%S")
            copyfile(CONF_FILE, CONF_FILE + time_suff)

            # Replace the old file with the new one
            copyfile(TMP_FILE, CONF_FILE)

class KnifeWrapper(object):
    """Define operations for interacting with Chef via the knife command line utility installed locally"""
    def __init__(self, config=None):
        self.config = config

    def bootstrap_node(self, node=None, run_list=None, retries=5):
        if node:
            if run_list:
                cmd_str = "knife bootstrap %s -N %s -r '%s'" % (node, node, run_list)
            else:
                cmd_str = "knife bootstrap %s -N %s" % (node, node)
            cmd = ShellCommand(cmd_str, retries=retries)
            code,output = cmd.execute()
            return (code,output)
        else:
            return (None,None)

    def purge_node(self, node=None, retries=5):
        if node: 
            cmd = ShellCommand("knife node delete -y %s && knife client delete -y %s " % (node, node), retries=retries)
            code,output = cmd.execute()
            return (code,output)
        else:
            return (None,None)

    def run_cmd_on_all_clients(self, cmd_str=None, retries=5):
        if cmd_str:
            cmd = ShellCommand("knife ssh 'name:*' '%s'" % (cmd_str), retries=retries)
            code,output = cmd.execute()
            return (code,output)
        else:
            return (None,None)
               


