#!/usr/bin/env python

##
## This file defines a helper class for vanilla (i.e. bare-bones) elastic slices.
## No custom functionality is implemented in this helper; all necessary functions 
## come from the superclass SimpleElasticSliceManagerHelper.
##
## This file is meant to be imported by a manager.
## The manager looks for two key variables: ELASTICSLICE_CLASS and ELASTICSLICE_HELPER.
## The former is a class it will instantiate; the latter is a an object it will just invoke operations on.
##
## Example ivocation of a manager:
## python -i elasticslice.py -d -D johnsond -U <username> -P <password> \
## -S 'urn:publicid:IDN+emulab.net:<projectname>+slice+<slicename>' --default-cm apt -C ~/.ssl/apt-cacert.pem \
## -H ~/elasticslice/clients/vanilla/vanilla_elastic.py
##

import protogeniclientlib
import geni.portal as portal
import geni.rspec.pg as RSpec
import geni.rspec.igext as IG
from lxml import etree as ET
import crypt
import random
import os
import argparse
from argparse import Namespace
import logging
from xml.sax.saxutils import escape

helper = None

# ToDo: UPDATE
COMPUTEIMAGE = "urn:publicid:IDN+emulab.net+image+emulab-ops:UBUNTU14-64-STD"
TBURL = ""
TBCMD = ""
TBCMD_ADDNODE = ""
TBCMD_DELNODE = ""
TBPATH = ""

class VanillaElasticSliceManagerHelper(protogeniclientlib.SimpleElasticSliceManagerHelper):
    def __init__(self,manager=None,server=None,debug=True):
        super(VanillaElasticSliceManagerHelper,self).__init__()
	# Override superclass variables if necessary:
        # self.debug = debug
        # self.rspec = RSpec.Request()
        # self.numPCs = numPCs
        self.imageURN = COMPUTEIMAGE
        # self.numLans = numLans
        # self.multiplexLans = multiplexLans
        # self.tarballs = tarballs
        # self.startupCommand = startupCommand
        self.nodetype = 'r320'
        # self.node_prefix = node_prefix
        # self.lan_prefix = lan_prefix
        # self.generated = False
        # self.idx = 0

        self._configure_logging(debug=debug)
        self.log.debug("Constructor finished, logger is set")
        
    def _configure_logging(self,debug=False):
        formatter = logging.Formatter("%(asctime)s - %(name)-16s - %(levelname)-8s - %(message)s")
        console = logging.StreamHandler()
        console.setFormatter(formatter)
        logging.getLogger('').addHandler(console)
        self.log = logging.getLogger(__name__)  
        if debug:
            self.log.setLevel(logging.DEBUG)
        else:
            self.log.setLevel(logging.INFO)   
 
    def _setState(self):
        if not self.generated:
            wrappedPGManifest = self.server.get_wrapped_manifest()
            self.idx = self._getMaxNodeID(wrappedPGManifest)
            self.lans = wrappedPGManifest.getLinkClientIds()

    def _getMaxNodeID(self,wrappedPGManifest):
        retval = 0
        for node in wrappedPGManifest.nodes:
            cid = node.name
            if cid.startswith(self.node_prefix):
                # "+1" supports nodenames formatted as follows: <node_prefix>-<ID>
                cid_num = int(cid[(len(self.node_prefix)+1):])
                if cid_num > retval:
                    retval = cid_num
        return retval

    def generateAddNodesArgs(self,count=1):
	self.log.debug("Running generateAddNodesArgs")

        self._setState() 
        
        retval = {}
        for i in range(self.idx + 1,self.idx + count + 1):
            nn = "%s-%d" % (self.node_prefix,i)

            # lans = []
            # #map(lambda(x): "%s-%d" % (self.node_prefix,x,),range(0,self.numLans))
            # for j in range(0,self.numLans):
            #     lans.append({ 'name' : "%s-%s" % (self.lan_prefix,j) })
            #     pass
            # lans lists is now set in _setState()

            nd = {}
            if self.imageURN:
                nd["diskimage"] = self.imageURN

            #if self.startupCommand:
            #    nd["startup"] = escape(self.startupCommand)
            #if self.tarballs:
            #    nd["tarballs"] = tarballs

            if self.nodetype:
                nd['hardware_type'] = self.nodetype
            if len(self.lans) > 0:
                nd["lans"] = self.lans
                pass
            retval[nn] = nd
            pass
        self.idx += count

        self.log.debug("Created NodeArgs: %s" % retval)

        return retval

ELASTICSLICE_CLASS = VanillaElasticSliceManagerHelper
ELASTICSLICE_HELPER = None
