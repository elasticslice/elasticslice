

class NonexistentSliceError(Exception):
    def __init__(self,msg=None):
        s = "cannot resolve slice"
        if msg:
            s += ": " + msg
        super(NonexistentSliceError,self).__init__(s)
    pass

class NonexistentSliverError(Exception):
    def __init__(self,msg=None):
        s = "cannot resolve sliver"
        if msg:
            s += ": " + msg
        super(NonexistentSliverError,self).__init__(s)
    pass

class NonexistentHelperError(Exception):
    def __init__(self,msg=None):
        s = "No helper object"
        if msg is not None:
            s += ": " + msg
            pass
        super(NonexistentHelperError,self).__init__(s)
        pass
    pass

class NotImplementedError(Exception):
    def __init__(self,msg=None):
        s = "Method not implemented"
        if msg is not None:
            s += ": " + msg
            pass
        super(NotImplementedError,self).__init__(s)
        pass
    pass

class InvalidArgumentError(Exception):
    pass

class RPCError(Exception):
    """
    Raised as an exception on XMLRPC method errors.  Encapsulates error
    information.
    """
    def __init__(self,rpcname,rval,response,msg=None):
        s = "Server returned error code %s while executing %s (%s)" \
            % (str(rval),rpcname,str(response))
        if msg:
            s += ": " + msg
            pass
        super(RPCError,self).__init__(s)
        self.rval = rval
        self.response = response
        pass
    pass
