import logging
import os
import sys
import time
import subprocess
from ConfigParser import SafeConfigParser
import argparse
from elasticslice.util.applicable import Applicable

SECOND = 1
MINUTE = SECOND * 60
HOUR   = MINUTE * 60
DAY    = HOUR * 24
MONTH  = DAY * 31
YEAR   = DAY * 365

LOG = logging.getLogger(__name__)

def parse_options():
  parser = DefaultSubcommandArgumentParser()

  # "-h" and "--help" are handled by optparse by default

  # Currently does not print anything useful 
  parser.add_argument("-V", "--version", action="version")

  # TODO: Remove or add to defaults (these were not exposed as arguments)
  #   endpoints = None
  #   defwaittime = 10 * MINUTE

  # None is the default value, no need to set explicitly
  # parser.set_defaults(
  #   config_file = "../etc/main.conf",
  #   endpoint = None,
  #   server = None,
  #   port = None, 
  #   cert = "~/.ssl/encrypted.pem",
  #   passphrasefile = "~/.ssl/password",
  #   cacert = None,
  #   ext_addr = None,
  #   ext_port = None,
  #   username = None,
  #   password = None,
  #   percent_available_minimum = None,
  #   minthreshold = None,
  #   maxthreshold = None,
  #   nodetype = None,
  #   devuser = None,
  #   slicename = None,
  #   default_cm = None,
  #   cmuri = None,
  #   sauri = None,
  #   fscache = True,
  #   fscachedir = "~/.protogeni/cache",
  #   manager_class = "manager.SimpleElasticSliceManager",
  #   helper_class = "manager.SimpleElasticSliceHelper",
  #   clientserver_endpoint_class = None,
  #   node_delete_wait_time = 300,
  # )
  # Defaults for boolean options are set below, see: action='store_[true|false]' 
                    
  parser.add_argument("-d", "--debug", dest="debug", action='store_true',
    help="Enable debugging log level")
  parser.add_argument("--config", dest="config_file",
    default="~/.protogeni/elasticslice.conf",
    help="Set location of the file with configuration parameters")
  parser.add_argument("-e", "--endpoint", dest="endpoint",
    help="Set endpoint")
  parser.add_argument("-s", "--server", dest="server",
    help="Set server")
  parser.add_argument("-p", "--port", dest="port", type=int,
    help="Set port")
  parser.add_argument("-c", "--cert", dest="cert",
    help="Set the certificate to use")
  parser.add_argument("--passphrasefile", dest="passphrasefile",
    help="A file containing the passphrase for your certificate")
  parser.add_argument("-C", "--cacert", dest="cacert",
    help="Set the server certificate to use for verification \
    [default: ~/.ssl/<default-cm>-cacert.pem, OR ~/.ssl/emulab-cacert.pem if --default-cm was not specified]")
  parser.add_argument("--ext_addr", dest="ext_addr",
    help="Set an external public hostname/IP (and possibly a port with --ext_port) that \
    the ProtoGENI server should call out to our client server as \
    (i.e., a public hostname that is port-forwarded to an internal private IP)")
  parser.add_argument("--ext_port", dest="ext_port", type=int,
    help="Set external port")
  parser.add_argument("-U", "--username", dest="username", 
    help="Set the HTTP Basic Auth username for the client server \
    (thus the Emulab CM/AM notification client of that server must be told this username/password)")
  parser.add_argument("-P", "--password", dest="password",
    help="Set the HTTP Basic Auth password for the client server")
  parser.add_argument("-A", "--autostart", dest="autostart", action='store_true',
    help="Autostart a client server")
  parser.add_argument("-M", "--automanage", dest="automanage", action='store_true',
    help="Autostart a manager")
  parser.add_argument("--am-percent-available-minimum", dest="percent_available_minimum", type=float,
    help="The minimum percent of available (free) nodes \
    (either globally or of --am-nodetype's type) to ensure are available -- \
    if the available percent falls below this threshold, we start freeing our nodes to the minimum threshold; \
    if the available percent is above this threshold, we add nodes.")
  parser.add_argument("--am-min-threshold", dest="minthreshold", type=int,
    help="Automanage threshold (minimum nodes to keep)")
  parser.add_argument("--am-max-threshold", dest="maxthreshold", type=int,
    help="Automanage threshold (maximum nodes to keep)")
  parser.add_argument("--am-nodetype", dest="nodetype",
    help="Automanage node type (i.e., pc600)")
  parser.add_argument("--am-enable-delete", dest="enable_delete", action='store_true',
    help="By default, we don't delete any nodes")
  parser.add_argument("-D", "--devuser", dest="devuser",
    help="Set the ProtoGENI devtree (i.e., johnsond)")
  parser.add_argument("-S", "--slicename", dest="slicename",
    help="Set the default slicename")
  parser.add_argument("--default-cm", dest="default_cm",
    help="Set the default CM to one of: apt, clutah, clemson, wisc, emulab")
  parser.add_argument("-H", "--nocerthostchecking", dest="nocerthostchecking", action="store_true",
    help="Disable certificate host checking (unecessary if you're communicating \
    with a real server, but during testing just with your local cert, \
    you will need it, because your Emulab/PG cert is a user cert, not a hostname cert) \
   ")
  # TODO: need better help messages
  parser.add_argument("--cmuri", dest="cmuri", 
    help="Set cmuri")
  parser.add_argument("--sauri", dest="sauri", 
    help="Set sauri")
  parser.add_argument("--fscache", dest="fscache", action="store_true",
    help="Should we cache method results to the filesystem; True or False")
  parser.add_argument("--fscachedir", dest="fscachedir", 
    default="~/.protogeni/cache",
    help="The location of the fscache")
  parser.add_argument("--image_urn", dest="image_urn",
    help="Set the default image URN, e.g. urn:publicid:IDN+emulab.net+image+emulab-ops:UBUNTU14-64-STD")
  parser.add_argument("--manager_class",dest="manager_class",
    help="The class to instantiate and use as the Manager [default: elasticslice.managers.core.SimpleElasticSliceManager]")
#    default="manager.SimpleElasticSliceManager")
  parser.add_argument("--helper_class",dest="helper_class",
    help="The class to instantiate and use as the Helper [default: elasticslice.managers.core.SimpleElasticSliceHelper, if you also use the default manager_class]")
#    default="manager.SimpleElasticSliceHelper")
  parser.add_argument("--clientserver_endpoint_class",
    dest="clientserver_endpoint_class",
    help="The class to instantiate and use as the Client Server endpoint")
  parser.add_argument("--node_delete_wait_time",type=int,
    dest="node_delete_wait_time",default=300,
    help="The time we request the server to wait, after notifying us of a pending delete for a node, before dynamically removing the node from our slice")

  # Add in subparsers.
  subparsers = parser.add_subparsers(help="Subcommands",dest='subcommand')
  subparsers.add_parser('interactive',help="Run in daemon/interactive mode")
  Applicable.add_subparsers(subparsers)
  parser.set_default_subparser('interactive')
  
  (options, args) = parser.parse_known_args(sys.argv[1:])
  return (options, args)

def is_int(s):
    try: 
        int(s)
        return True
    except ValueError:
        return False

def is_float(s):
    try: 
        float(s)
        return True
    except ValueError:
        return False

def auto_convert(s):
  if not s:
    return s
  elif is_int(s):
    return int(s)
  elif is_float(s):
    return float(s)
  elif (isinstance(s, str)):
    if (s.lower() == 'true'):
      return True
    elif (s.lower() == 'false'):
      return False  
    else: 
      return s
  else:
    return s

class Config(object):
  """ Config class includes code for parsing and processing config file and command-line arguments"""

  def __init__(self, options):
    self.config_file = options.config_file
    if self.config_file.startswith('~/'):
      self.config_file = os.environ['HOME'] + "/" + self.config_file[2:]
    self.config = self.read_config(self.config_file)

    self.main_section_name = 'MAIN'
    self.all = {}
    if not self.main_section_name in self.config.sections():
      LOG.error('Specified config file does not contain a section named %s' % self.main_section_name)
    else:
      # Populate self.all with options from self.main_section_name
      for opt, val in self.config.items(self.main_section_name):
        # Recognize int and float values and record them as such
        self.all[opt] = auto_convert(val)

    # Process optional sections (i.e. sections other than self.main_section_name)
    # Adding options from those sections to self.all in the form: "<section_name>::<config_option>"
    for s in self.config.sections():
      if s != self.main_section_name:
        for (opt,val) in self.config.items(s):
          self.all["%s::%s" % (s,opt) ] = auto_convert(val)

    LOG.debug("Configuration options from file: %s" % self.all)
    
    # Override values from the config file with command-line arguments
    # But, skip command-line opts prefixed with ___; those are subcommand
    # options.
    _opt_dict = vars(options)
    opt_dict = {}
    for (k,v) in _opt_dict.iteritems():
        if not k.startswith('___'):
            opt_dict[k] = v
        pass
    for k,v in opt_dict.iteritems():
      if (k in self.all) and v:
        # Condition above doesn't allow overring file options with default/None values from command-line parser
        LOG.debug("Overriding value for %s from config file with value %s from command line" % (k,v))
        self.all[k] = auto_convert(v)
      elif not (k in self.all):
        LOG.debug("Adding %s:%s to the list of config options" % (k,v))
        self.all[k] = auto_convert(v)

    # TODO: Complete processing
    # The following is left from the old way of processing args and still need to be included
    # elif opt in ("-e", "--endpoint"):
    #     (name,url) = val.split(":")
    #     if not endpoints:
    #         endpoints = {}
    #     endpoints[name] = {}
    #     eus = urlsplit(url)
    #     if eus.hostname:
    #         endpoints[name]['hostname'] = eus.hostname
    #     if eus.port:
    #         endpoints[name]['port'] = eus.port
    #     if eus.path:
    #         endpoints[name]['path'] = eus.path
    #     if eus.username:
    #         endpoints[name]['username'] = eus.username
    #     if eus.password:
    #         endpoints[name]['password'] = eus.password
    #     pass
    # elif opt in ("-s", "--server"):
    #     server = val
    #     #
    #     # Allow port spec here too.
    #     #
    #     if val.find(":") > 0:
    #         server,port = string.split(val, ":", 1)
    #         port = int(port)
    #         cskwargs['port'] = port
    #         pass
    #     cskwargs['server'] = server
    #     pass

    # For convenience, expose config options as member variables 
    self.username = self.all['username']
    self.ext_port = self.all['ext_port']
    self.config_file = self.all['config_file']
    self.percent_available_minimum = self.all['percent_available_minimum']
    self.nodetype = self.all['nodetype']
    self.endpoint = self.all['endpoint']
    self.nocerthostchecking = self.all['nocerthostchecking']
    self.cacert = self.all['cacert']
    self.server = self.all['server']
    self.slicename = self.all['slicename']
    self.ext_port = self.all['ext_port']
    self.automanage = self.all['automanage']
    self.password = self.all['password']
    self.ext_addr = self.all['ext_addr']
    self.devuser = self.all['devuser']
    self.default_cm = self.all['default_cm']
    self.minthreshold = self.all['minthreshold']
    self.maxthreshold = self.all['maxthreshold']
    self.port = self.all['port']
    self.cert = self.all['cert']
    self.passphrasefile = self.all['passphrasefile']
    self.autostart = self.all['autostart']
    self.debug = self.all['debug']
    self.nodetype = self.all['nodetype']
    self.enable_delete = self.all['enable_delete']
    self.cmuri = self.all['cmuri']
    self.sauri = self.all['sauri']
    self.fscache = self.all['fscache']
    self.fscachedir = self.all['fscachedir']

    LOG.debug("All configuration options: %s" % self.all)
    LOG.debug("Exposed options: %s" % dir(self))

  def read_config(self, file):
    config = SafeConfigParser()
    config.read(file)
    return config

class ShellCommand(object):
    """Wrapper for running shell commands"""
    def __init__(self, args=[], executable='/bin/bash', retries=1):
        self.stdout = None
        self.stderr = None
        self.args = args
        self.executable = executable
        self.retries = retries

    def execute(self,pause=1):
        attempts = 0
        retval = 1
        while (attempts < self.retries) and retval:
          process = subprocess.Popen(self.args, shell=True,
                                     executable=self.executable,
                                     stdout=subprocess.PIPE,
                                     stderr=subprocess.PIPE)
          (self.stdout, self.stderr) = process.communicate()
          attempts += 1
          retval = process.returncode
          time.sleep(pause)

        if retval:
            LOG.error("Exceeded number of retries=%d. Command \"%s\" failed with message: %s" % (self.retries,self.args, self.stdout))
            return retval, None
        else:
            return retval, self.stdout

class DictNamespace(dict):
    """
    A simple class that emulates argparse.Namespace, but is also an
    iterable dictionary.
    """
    def __setattr__(self,attr,value):
      self.__setitem__(attr,value)
      pass
    
    def __getattr__(self,attr):
      if attr in self.keys():
        return self.__getitem__(attr)
      else:
        return dict.__getattr__(self,attr)
      pass
    
    pass

class DefaultSubcommandArgumentParser(argparse.ArgumentParser):
    __default_subparser = None

    def set_default_subparser(self, name):
        self.__default_subparser = name

    def _parse_known_args(self, arg_strings, *args, **kwargs):
        in_args = set(arg_strings)
        dsp = self.__default_subparser
        if dsp is not None and not {'-h', '--help'}.intersection(in_args):
            for x in self._subparsers._actions:
                subparser_found = (
                    isinstance(x, argparse._SubParsersAction) and
                    in_args.intersection(x._name_parser_map.keys())
                )
                if subparser_found:
                    break
            else:
                # insert default in first position, this implies no
                # global options without a sub_parsers specified
                arg_strings = [dsp] + arg_strings
        return super(DefaultSubcommandArgumentParser,self)._parse_known_args(
            arg_strings, *args, **kwargs)

    pass

def get_file_contents(file_path, cleanup=True):
    """
    This method returns the contents of the file at the specified path.
    Optionally, it cleans up the contents by removing EOL chars and replacing tabs with spaces,
    as well removes leading, trailing, and duplicate spaces. It will also remove empty lines.
    """    
    with open(file_path, "r") as f: 
        contents = f.readlines()

    if cleanup:
        return filter(None, [' '.join(l.rstrip('\n').replace('\t',' ').strip().split()) for l in contents])
    else:
        return contents

def push_local_file_to_remote_node(file_source, node, file_dest=None, retries=5):
    """
    This method wraps the ShellCommand class to run scp command
    and copy the specified local file to a remote node.
    If file_dest is not specified, the same path, file_source, will be used as destination. 
    If specified, file_dest should be absolute path.
    Number or retries will be passed to ShellCommand.
    """     
    if not os.path.isfile(file_source):
      LOG.error("Argument file_source is not pointing to a local file.")
      return
    if not node:
      LOG.warning("Argument node should not be empty for the method to actually work.")
      return

    abs_file_source = os.path.abspath(file_source)
    if file_dest:
      cmd_str = "scp %s %s:%s" % (abs_file_source, node, file_dest)
    else:
      cmd_str = "scp %s %s:%s" % (abs_file_source, node, abs_file_source)
    cmd = ShellCommand(cmd_str, retries=retries)
    code,output = cmd.execute()
    return (code,output)
