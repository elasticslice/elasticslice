#!/usr/bin/env python

"""
:mod:`elasticslice.rpc.protogeni` contains an object-based
implementation of the basic ProtoGeni test client-side python code, plus
several more useful classes for building client-side (i.e., slice- and
experiment-managing) ProtoGENI programs.  It requires geni-lib (i.e.,
the geni.* modules imported below).  Cloudlab maintains its own copy of
this repository which has the latest Cloudlab additions that may not
have made it upstream yet.  This library doesn't currently require those
additions; just FYI.  The Cloudlab repository can be found at
https://bitbucket.org/emulab/geni-lib/ ; the main repository is
https://bitbucket.org/barnstorm/geni-lib .
"""

import sys
import getopt
import os, os.path
import dircache
import pwd
import traceback
import syslog
import string
import BaseHTTPServer
from SimpleXMLRPCServer import SimpleXMLRPCDispatcher
import M2Crypto
from M2Crypto import SSL
from M2Crypto.SSL import SSLError
import base64
import threading
from urlparse import urlsplit, urlunsplit
from urllib import splitport
import httplib
import xmlrpclib
import zlib
import re
import threading
import select
import smtplib
from email.mime.text import MIMEText
import time
import socket
import hashlib
import pickle
import logging
import geni.rspec.pgmanifest
from geni.rspec.pgmanifest import Manifest as PGManifest
import geni.rspec.pgad as RSpecAd
from elasticslice.util.applicable \
  import ApplicableClass,ApplicableMethod,ApplicableFormatter, \
    get_default_formatter
import elasticslice.util.exceptions as exc
import json as JSON

LOG = logging.getLogger(__name__)

HOME = os.getenv('HOME')
if HOME:
    CERTIFICATE     = HOME + "/.ssl/encrypted.pem"
    PASSPHRASEFILE  = HOME + "/.ssl/password"
    CACERTIFICATE     = HOME + "/.ssl/emulab-cacert.pem"
else:
    CERTIFICATE     = None
    PASSPHRASEFILE  = None
    CACERTIFICATE     = None
    
    import smtplib

SECOND = 1
MINUTE = SECOND * 60
HOUR   = MINUTE * 60
DAY    = HOUR * 24
MONTH  = DAY * 31
YEAR   = DAY * 365

# Ever tried time zones in python?  It sucks cause there's no zoneinfo
# by default.  And dateutil/pytz/whatever isn't everywhere.  So hack it.
____now = time.time()
__UTC_OFFSET = time.mktime(time.gmtime(____now)) \
                   - time.mktime(time.localtime(____now))

def GeniTimeStringToLocalTimeStamp(timestring):
    """
    :param timestring: a GENI time string (UTC)
    :return: a local UNIX time stamp
    """
    if timestring[-1] == 'Z':
        timestring = timestring[:-1]
        pass
    ts = time.strptime(timestring,"%Y-%m-%dT%H:%M:%S")
    utcstamp = time.mktime(ts)
    return utcstamp - __UTC_OFFSET

def LocalTimeStampToGeniTimeString(ts):
    """
    :param timestring: a local UNIX time stamp
    :return: a GENI time string (UTC)
    """
    utctt = time.mktime(ts + __UTC_OFFSET)
    return time.strftime("%Y-%m-%dT%H:%M:%S",utctt) + "Z"

def GeniTimeStampToLocalTimeStamp(ts):
    """
    :param timestring: a GENI (UTC) UNIX time stamp
    :return: a local UNIX time stamp
    """
    return ts - __UTC_OFFSET

##
## A utility function to send email to someone.
##
def SENDMAIL(To,Subject,Msg,From=None,headers=None):
    smsg = MIMEText(Msg)
    smsg['Subject'] = Subject
    if not From:
        smsg['From'] = os.getlogin() + "@" + socket.gethostname()
    else:
        smsg['From'] = From
    smsg['To'] = To
    if headers and len(headers.keys()) > 0:
        for (k,v) in headers.iteritems():
            smsg[k] = v
            pass
        pass
    
    sent = False
    try:
        # Send the message via our own SMTP server, but don't include the
        # envelope header.
        s = smtplib.SMTP('localhost')
        s.sendmail(smsg['From'],[smsg['To'],],smsg.as_string())
        s.quit()
        sent = True
    except:
        LOG.exception("direct mail send failed, trying sendmail")
        pass
    
    if not sent:
        try:
            stdin = os.popen("sendmail '%s'" % (To,),'w')
            stdin.write(smsg.as_string())
            stdin.close()
            sent = True
        except:
            LOG.exception("sendmail send failed")
            pass
        pass

    return True

##
## A helper function to read the passphrase for a private key.
##
def PassPhraseCB(v, prompt1='Enter passphrase:', prompt2='Verify passphrase:',
                 passphrasefile=PASSPHRASEFILE):
    """Acquire the encrypted certificate passphrase by reading a file
    or prompting the user.

    This is an M2Crypto callback. If the passphrase file exists and is
    readable, use it. If the passphrase file does not exist or is not
    readable, delegate to the standard M2Crypto passphrase
    callback. Return the passphrase.
    """
    if os.path.exists(passphrasefile):
        try:
            passphrase = open(passphrasefile).readline()
            passphrase = passphrase.strip()
            return passphrase
        except IOError, e:
            LOG.exception('Error reading passphrase file %s' % (passphrasefile,))
    else:
        LOG.warning('passphrase file %s does not exist' % (passphrasefile,))
    # Prompt user if passphrasefile does not exist or could not be read.
    from M2Crypto.util import passphrase_callback
    return passphrase_callback(v, prompt1, prompt2)

##
## Some useful codes that ProtoGeni API Servers can return.
##
class ProtoGeniClientDefs:
    """
    XMLRPC/HTTP return codes from a ProtoGeni server.
    """
    RESPONSE_SUCCESS        = 0
    RESPONSE_BADARGS        = 1
    RESPONSE_ERROR          = 2
    RESPONSE_FORBIDDEN      = 3
    RESPONSE_BADVERSION     = 4
    RESPONSE_SERVERERROR    = 5
    RESPONSE_TOOBIG         = 6
    RESPONSE_REFUSED        = 7  # Emulab is down, try again later.
    RESPONSE_TIMEDOUT       = 8
    RESPONSE_DBERROR        = 9
    RESPONSE_RPCERROR       = 10
    RESPONSE_UNAVAILABLE    = 11
    RESPONSE_SEARCHFAILED   = 12
    RESPONSE_UNSUPPORTED    = 13
    RESPONSE_BUSY           = 14
    RESPONSE_EXPIRED        = 15
    RESPONSE_INPROGRESS     = 16
    RESPONSE_ALREADYEXISTS  = 17
    RESPONSE_VLAN_UNAVAILABLE = 24
    RESPONSE_INSUFFICIENT_BANDWIDTH  = 25
    RESPONSE_INSUFFICIENT_NODES      = 26
    RESPONSE_INSUFFICIENT_MEMORY     = 27
    RESPONSE_NO_MAPPING              = 28
    RESPONSE_NOT_IMPLEMENTED         = 100
    RESPONSE_SERVER_UNAVAILABLE = 503
    pass

##
## Hacks for better control over cert validation in M2Crypto.  We have
## this unusual situation, where the protogeni client has a server (to
## which protogeni daemons might call RPCs on, i.e. to notify), but is
## not a real server.  They've only got their user cert, and the ca cert
## of the SA (Emulab).  This user cert uses the CN for user info, not
## hostname info.  So we have to elide that check.  M2Crypto does not
## make this easy to do properly, so we hack.
##
class NoHostChecker(M2Crypto.SSL.Checker.Checker):
    def __call__(self, peerCert, host=None):
        LOG.debug("Using NoHostChecker")
        val = M2Crypto.SSL.Checker.Checker.__call__(self,peerCert,None)
        LOG.debug("Used NoHostChecker: %s" % str(val))
        return val
    pass

class NoHostCheckHTTPSConnection(M2Crypto.httpslib.HTTPSConnection):
    #
    # Have to change out the cert checker after the Connection is created but
    # before .connect() happens!  Ugh, this is sick.  So, steal the code, but
    # replace the checker in the Connection object.
    #
    def connect(self):
        self.sock = M2Crypto.SSL.Connection(self.ssl_ctx)
        self.sock.clientPostConnectionCheck = NoHostChecker()
        if self.session:
            self.sock.set_session(self.session)
        self.sock.connect((self.host, self.port))
        pass
    pass

##
## These are the default Component Managers we know about.  You can
## extend or replace this list when you construct a ProtoGeniClient
## object.
##
DEFAULT_CMS = {
    "apt" : "https://boss.apt.emulab.net:12369/protogeni/xmlrpc",
    "clutah" : "https://www.utah.cloudlab.us:12369/protogeni/xmlrpc",
    "clemson" : "https://www.clemson.cloudlab.us:12369/protogeni/xmlrpc",
    "wisc" : "https://www.wisc.cloudlab.us:12369/protogeni/xmlrpc",
    "emulab" : "https://www.emulab.net:12369/protogeni/xmlrpc",
}

RPC_CERT_CN_REMAPS = {
    "www.apt.emulab.net" : "boss.apt.emulab.net",
}

def parse_cm_list(cmliststr):
    if str is None:
        return
    retval = []
    cmlist = cmliststr.split(',')
    for cm in cmlist:
        if not cm in DEFAULT_CMS:
            raise Exception("cm %s not in DEFAULT_CMS (%s)"
                            % (cm,DEFAULT_CMS))
        retval.append(cm)
        pass
    return retval

def parse_bool(str):
    if str is None:
        return
    str = str.lstrip(' ').rstrip(' ')
    if str in ['True','TRUE','true','1','yes','YES','Yes']:
        return Truef
    return False

def parse_bool_or_int(str):
    if str is None:
        return
    str = str.lstrip(' ').rstrip(' ')
    if str in ['True','TRUE','true','T','t','yes','YES','Yes','Y','y']:
        return True
    elif str in ['False','FALSE','false','F','f','no','NO','No','N','n']:
        return False
    else:
        return int(str)
    pass

def parse_str_list(str):
    if str is None:
        return
    str = str.lstrip(' ').rstrip(' ')
    if str == '':
        return []
    return str.split(',')

def parse_rspec_arg(_rspec):
    try:
        from lxml import etree as ET
        ET.fromstring(_rspec)
        return _rspec
    except:
        pass
    try:
        f = open(_rspec,'r')
        return f.read()
    except:
        LOG.warn("rspec argument does not appear to be XML nor a file;"
                 " but assuming it is indeed XML!")
        return _rspec
    pass
    
def parse_safe_eval(value):
    retval = None
    try:
        retval = eval(value)
        return retval
    except:
        LOG.exception("Argument must be Python code")
        raise Exception("Argument must be Python code")
    pass
    
def parse_safe_list_or_eval(value):
    retval = None
    try:
        retval = eval(value)
        return retval
    except:
        pass
    try:
        _list = value.split(',')
        return _list
    except:
        pass
    raise Exception("Argument must be Python code, or a list of strings")

#
# By default, adjust our credential caching time by this much to give a
# little slop to make sure we can renew the credential.
#
DEFAULT_CREDENTIAL_EXPIRATION_BUFFER = 6 * HOUR
#
# If we're caching a method result, but we don't have a more specific
# cache time or cache expiration function, use this time.
#
DEFAULT_METHOD_CACHE_INTERVAL = 5 * MINUTE
DEFAULT_METHOD_CACHE_TIMES = {
    'sa' : { 'Resolve' : HOUR,
             'GetKeys' : YEAR,
             'GetCredential' : 5 * MINUTE, # credential_cache_func,
             'DiscoverResources' : HOUR,
        },
    'cm' : { 'Resolve' : HOUR,
             'DiscoverResources' : 10 * MINUTE,
             'SliverStatus' : 1 * MINUTE,
        }
    }

class Credential(object):
    def __init__(self,credential):
        self.credential = credential
        pass
    pass

class SelfCredential(Credential):
    def __init__(self,credential):
        super(SelfCredential,self).__init__(credential)
        pass
    pass

class SliceCredential(Credential):
    def __init__(self,credential):
        super(SliceCredential,self).__init__(credential)
        pass
    pass

class SelfCredential(Credential):
    def __init__(self,credential):
        super(SelfCredential,self).__init__(credential)
        pass
    pass


class Slice(object):
    def __init__(self,urn,uuid,creator_uuid,creator_urn,gid,
                 component_managers):
        self.urn = urn
        self.uuid = uuid
        self.creator_uuid = creator_uuid
        self.creator_urn = creator_urn
        self.gid = gid
        self.component_managers = component_managers
        pass
    pass

class Sliver(object):
    def __init__(self,):

        pass
    pass

class User(object):
    def __init__(self,uid,hrn,uuid,email,gid,name):
        self.uid = uid
        self.hrn = hrn
        self.uuid = uuid
        self.email = email
        self.gid = gid
        self.name = name
        pass
    pass

@ApplicableClass()
class ProtoGeniServer(object):
    """A ProtoGeniServer is a class that provides a client user with the
    ability to call RPCs at ProtoGENI SA (slice authority) and CM
    (component manager) (aka AM -- aggregate manager) servers.  In the
    Cloudlab/PG world, Emulab central is the SA, and the various
    clusters are CM/AMs.  This client helps you create a slice at an SA
    (defaults to Emulab, or whomever issued your certificate), then
    create slivers at one or more CM/AMs by submitting an RSpec to each
    cluster.  An RSpec describes the resources you want allocated and
    how they're configured/connected.
  
    It is a generic class that lets you directly invoke PG/GENI methods
    on a CH, SA, AM, CM; but it also provides wrappers for key functions
    on ProtoGENI's SA and CM interfaces.  It creates an XMLRPC
    connection for each RPC you invoke, although it attempts to cache
    credentials and state from your previous RPCs, and only renew those
    things as necessary.
    
    Some of the methods cache results for you.  Some RPCs are very
    heavyweight (i.e. DiscoverResources), and you just don't need to
    refresh the resource list all that often.  However, you may want to
    review the information it contains more regularly, and only refresh
    periodically or in exceptional cases.  The caching infrastructure
    helps with this pattern.  Generally speaking, the rule is that any
    RPCs that modify resources or state on the server are not cached;
    whereas requests for information *are* cached.  Any method that
    caches an RPC's result has several keyword args (i.e., force,
    cache_notifier) that help you interact with the cache usefully.
    Finally, the cache persists itself by default to
    HOME/.protogeni/cache .
    
    Finally, this server stub can be multithreaded; it locks itself to
    prevent parallel invocation of certain methods.  Right now it uses
    one big lock; this should be broken out into a cache lock, and locks
    for each RPC endpoint URI.  This may seem stupid, but we really want
    to reduce weird things happening (i.e., two parallel CM.AddNodes
    calls on the same slice, or two parallel CM.SliverStatus calls that
    duplicate work).
    """
    
    def __init__(self,config=None,
                 cert=CERTIFICATE,passphrasefile=PASSPHRASEFILE,
                 cacert=None,noCertHostChecking=False,
                 endpoints={},devuser=None,slicename=None,sliceurn=None,
                 cms=DEFAULT_CMS,default_cm='emulab',cmuri=None,sauri=None,
                 speaksforcredential=None,
                 fscache=True,fscachedir=None,
                 cache_times=DEFAULT_METHOD_CACHE_TIMES,nolock=False):
        """
        :param cert: Your GENI user certificate.  It should be encrypted, and you'll need to supply a passphrase.  Defaults to ~/.ssl/encrypted.pem .
        :param passphrasefile: If you place your certificate passphrase into a file and tell the constructor, you won't be prompted to enter your password all the time.
        :param cacert: The certificate authority who issued your cert.  Defaults to ~/.ssl/@<default_cm>-cacert.pem .
        :param default_cm: The CM to use by default (if none is specified to any of the CM API wrapper functions via cm= keyword arg)
        :param noCertHostChecking: Usually, when certs are verified during SSL session establishment, the CN field is checked for a matching hostname.  This should only be disabled if you're trying to talk to a server that doesn't have a real certificate -- or a server that is using a GENI identity cert as its certificate.
        :param endpoints: A dictionary of endpoint types to endpoint URL information. This is useful if you would like to use a non-default SA, CH, etc.
        :param devuser: If you want to invoke RPCs through an Emulab developer's development tree, provide the dev's loginuid here, and it will get added to RPC URLs in the right way.
        :param slicename: The HRN of the "default" slice that will be used in any RPCs for which a slicename is required and not supplied in that call.
        :param sliceurn: The URN of the "default" slice that will be used in any RPCs for which a slicename is required and not supplied in that call.
        :param cms: A dict of known CMs; defaults to protogeniclientlib.DEFAULT_CMS.
        :param default_cm: The default cm to use for CM RPCs when one isn't specified as a method argument.  Must be a key in @cms.
        :param cmuri: Provide a custom CM URI here instead of filling out cms.  If this is set, it will become your @default_cm (with a name of "custom").
        :param sauri: Provide a custom SA URI here.
        :param speaksforcredential: A credential of a GENI user on whose behalf you are invoking RPCs.
        :param fscache: Should we cache method results to the filesystem; True or False
        :param fscachedir: The location of the fscache; defaults to HOME/.protogeni/cache
        :param cache_times: A dict of SA/CM RPCs to cache times in seconds; defaults to DEFAULT_METHOD_CACHE_TIMES.
        :param nolock: Set this to true if you don't want any locking.
        """

        super(ProtoGeniServer,self).__init__()

        self.config = config
        self.cert = (config and config.cert) or cert
        if self.cert[0] == '~':
            self.cert = HOME + "/" + self.cert[1:]
        self.passphrasefile = (config and config.passphrasefile) or passphrasefile
        if self.passphrasefile[0] == '~':
            self.passphrasefile = HOME + "/" + self.passphrasefile[1:]
        self.cacert = (config and config.cacert) or cacert
        if self.cacert is not None and self.cacert[0] == '~':
            self.cacert = HOME + "/" + self.cacert[1:]
        self.noCertHostChecking = (config and config.nocerthostchecking) or noCertHostChecking
        self.endpoints = endpoints
        self.devuser = (config and config.devuser) or devuser
        self.slicename = (config and config.slicename) or slicename
        if not self.slicename:
            raise exc.InvalidArgumentError("no default slicename specified!")
        self.sliceurn = sliceurn
        
        self.user_urn = None
        self.user_email = None
        self.cert_x509 = M2Crypto.X509.load_cert(self.cert)
        subject_alt_name = self.cert_x509.get_ext('subjectAltName').get_value()
        ss = subject_alt_name.find("urn:")
        if ss >= 0:
            sc = subject_alt_name[ss:].find(',')
            if sc >= 0:
                self.user_urn = subject_alt_name[ss:(ss+sc)]
            else:
                self.user_urn = subject_alt_name[ss:]
            pass
        ss = subject_alt_name.find("email:")
        if ss >= 0:
            ss += 6
            sc = subject_alt_name[ss:].find(',')
            if sc >= 0:
                self.user_email = subject_alt_name[ss:(ss+sc)]
            else:
                self.user_email = subject_alt_name[ss:]
            pass

        self.cms = cms
        self.default_cm = (config and config.default_cm) or default_cm
        self.cmuri = (config and config.cmuri) or cmuri
        if cmuri is not None:
            self.default_cm = 'custom'
            if cms is None:
                self.cms = {}
                pass
            self.cms['custom'] = self.cmuri
            pass
        self.sauri = (config and config.sauri) or sauri
        
        if not self.cacert:
            if not self.default_cm:
                self.cacert = CACERTIFICATE
            elif HOME:
                self.cacert = HOME + "/.ssl/%s-cacert.pem" % (self.default_cm,)
            else:
                self.cacert = None
            pass
        
        if self.default_cm and not cms.has_key(self.default_cm):
            raise exc.InvalidArgumentError("cannot specify a default cm not in cms!")
        if self.cmuri is not None and self.default_cm is not None:
            raise exc.InvalidArgumentError("cannot specify both a custom CM URI and a default cm!")
        
        self.selfcredential = None
        self.selfcredential_expiration = None
        self.selfcredential_expiration_str = None
        
        self.cache = {}
        self.cache_times = cache_times
        self.fscache = (config and config.fscache) or fscache
        self.fscachedir = (config and config.fscachedir) or fscachedir
        if not self.fscachedir:
            self.fscachedir = os.getenv('HOME') + '/.protogeni/cache'
            pass
        elif self.fscachedir[0] == '~':
            self.fscachedir = HOME + "/" + self.fscachedir[1:]
        if not os.path.exists(self.fscachedir):
            try:
                os.makedirs(self.fscachedir)
            except:
                LOG.exception("could not create cache dir %s" % (self.fscachedir))
                pass
            pass
        
        self.speaksforcredential = speaksforcredential

        self.locking = not nolock
        if self.locking:
            self.lock = threading.RLock()
        else:
            self.lock = None
            pass

        LOG.debug("default_cm = %s" % (self.default_cm,))

        #
        # Configure our endpoints.  By default, we assume this is a client of
        # a ProtoGENI/GENI server, although even the default config tries to
        # learn from the user's cert.
        #
        if not endpoints or len(endpoints.keys()) == 0:
            self.endpoints = {}
            # XMLRPC server: use www.emulab.net for the clearinghouse.
            self.endpoints['ch'] = { "host":"www.emulab.net",
                                     "port":12369,"path":"/protogeni/xmlrpc" }
            self.endpoints["sr"] = { "host" : "www.emulab.net","port":12370,
                                     "path":"/protogeni/pubxmlrpc" }
            pass
        else:
            self.endpoints = {}
            pass

        if not self.endpoints.has_key("default"):
            try:
                cert = M2Crypto.X509.load_cert(self.cert)
                extension = cert.get_ext("authorityInfoAccess")
                val = extension.get_value()
                if val.find('URI:') > 0:
                    url = val[val.find('URI:')+4:]
                url = url.rstrip()
                # strip trailing sa
                if url.endswith('/sa') > 0:
                    url = url[:-3]
                    pass
                scheme, netloc, path, query, fragment = urlsplit(url)
                host,port = splitport(netloc)
                if host in RPC_CERT_CN_REMAPS:
                    host = RPC_CERT_CN_REMAPS[host]
                self.endpoints["default"] = { "host":host,"port":port,
                                              "path":path }
            except LookupError, err:
                LOG.exception("Error while loading certificate %s to get"
                              " default endpoint" % (self.cert,))
                pass

            if "default" not in self.endpoints and cert is not None:
                dhost = cert.get_issuer().CN
                if dhost in RPC_CERT_CN_REMAPS:
                    dhost = RPC_CERT_CN_REMAPS[dhost]
                self.endpoints["default"] = { "host":dhost,
                                              "port":443,
                                              "path":"/protogeni/xmlrpc", }
                pass
            
            LOG.debug("Default endpoint: %s" % (str(self.endpoints["default"]),))
            LOG.debug("All endpoints: %s" % (str(self.endpoints),))
            pass
        
        #
        # Try to build our slice URN
        #
        if not self.sliceurn and self.slicename:
            if "sa" in self.endpoints and "host" in self.endpoints["sa"]:
                hn = self.endpoints["sa"]["host"]
            elif "default" in self.endpoints and "host" in self.endpoints["default"]:
                hn = self.endpoints["default"]["host"]
            else:
                hn = None
                pass
            if hn:
                domain = hn[hn.find('.')+1:]
                self.sliceurn = "urn:publicid:IDN+" + domain + "+slice+" + self.slicename
                pass
            pass
        
        pass
    
    def __get_lock(self):
        if self.locking:
            self.lock.acquire()
        pass
    
    def __put_lock(self):
        if self.locking:
            self.lock.release()
        pass
    
    def __cache_get(self,key,accept_stale=False,max_age=None):
        self.__get_lock()
        
        value = None
        cur_time = None
        if self.cache.has_key(key):
            cur_time = time.time()
            # If there is a max_age, and the value is under it, set it up
            # for return (but NB: we check accept_stale below separately).
            if not max_age == None:
                if type(max_age) == int and \
                  (cur_time - self.cache[key]['ctime']) <= max_age:
                    value = self.cache[key]
                else:
                    # We had a max_age and the value was over it, so don't
                    # return anything.
                    value = None
                pass
            else:
                # We aren't checking a max_age or are under it; now we just
                # need to set value so that the staleness/expiration check
                # below is handled correctly.
                value = self.cache[key]
                pass
            
            # NB: check accept_stale separately from max_age: if we
            # don't accept stale values, and it's expired, don't return
            # even if there was an acceptable max_age!
            if not accept_stale and (self.cache[key]['xtime'] <= cur_time):
                value = None
                pass
            pass
        
        self.__put_lock()

        if value:
            LOG.debug("CACHE: found %s (%s %s) (expires=%f,added=%f,age=%f)"
                      % (key,str(accept_stale),str(max_age),
                         value['xtime'],value['ctime'],
                         (cur_time - value['ctime'])))
        else:
            LOG.debug("CACHE: did not find %s (%s %s)"
                      % (key,str(accept_stale),str(max_age)))
            pass
        
        return value
    
    def _cache_get(self,uri,module,method,version,params,accept_stale=False,
                   max_age=None,cache_notifier=None):
        # Set up the key for lookup
        key = "|uri=%s||module=%s||method=%s|version=%s|" \
            % (str(uri),str(module),str(method),str(version),)
        for (k,v) in params.iteritems():
            key += "|%s=%s|" % (str(k),str(v),)
            pass
        
        value = self.__cache_get(key,accept_stale=accept_stale,max_age=max_age)
        if cache_notifier:
            cache_notifier.reset()
            if not value:
                cache_notifier.cached = False
            else:
                cache_notifier.cached = True
                cache_notifier.expires = value['xtime']
                cache_notifier.added = value['ctime']
                cache_notifier.key = key
                cache_notifier.updated = False
                LOG.debug("CACHE: %s" % (str(cache_notifier),))
                pass
            pass
        
        if value:
            value = value['value']
        
        return value
    
    def __cache_add(self,key,value,ctime=None,xtime=None,
                    cache_notifier=None):
        self.__get_lock()
        
        updated = False
        if key in self.cache:
            updated = True
            pass
        
        # Make sure we have a synchronous timestamp so we can use the
        # same ctime for both ctime and for computing xtime.
        __ctime = None
        if not ctime or not xtime:
            __ctime = time.time()
            pass
        
        if not ctime:
            ctime = __ctime
        if not xtime:
            xtime = __ctime + DEFAULT_METHOD_CACHE_INTERVAL
            pass
        
        self.cache[key] = { 'value':value,'ctime':ctime,'xtime':xtime }
        
        if self.fscache:
            h = hashlib.md5(key).hexdigest()
            kf = file("%s/%s.key" % (self.fscachedir,h),'w')
            vf = file("%s/%s.value" % (self.fscachedir,h),'w')
            cf = file("%s/%s.ctime" % (self.fscachedir,h),'w')
            xf = file("%s/%s.xtime" % (self.fscachedir,h),'w')
            cf.write(str(ctime))
            cf.close()
            xf.write(str(xtime))
            xf.close()
            kf.write(key)
            kf.close()
            pickle.dump(value,vf)
            vf.close()
            pass
        
        if cache_notifier:
            cache_notifier.reset()
            cache_notifier.cached = False
            cache_notifier.expires = xtime
            cache_notifier.added = ctime
            cache_notifier.key = key
            cache_notifier.updated = updated
            pass
        
        self.__put_lock()
        
        LOG.debug("CACHE: added key %s at %s (expires %s, duration %.4f hrs,"
                  " remaining %.4f hrs)"
                  % (key,str(ctime),str(xtime),(xtime - ctime)/HOUR,
                     (xtime - time.time())/HOUR))
        pass
    
    def _cache_add(self,uri,module,method,version,params,value,
                   ctime=time.time(),xtime=None,cache_notifier=None):
        key = "|uri=%s||module=%s||method=%s|version=%s|" \
            % (str(uri),str(module),str(method),str(version),)
        for (k,v) in params.iteritems():
            key += "|%s=%s|" % (str(k),str(v),)
            pass
        
        if not xtime:
            xtime = ctime + DEFAULT_METHOD_CACHE_INTERVAL
            if self.cache_times.has_key(module) \
                and self.cache_times[module].has_key(method):
                xtime = ctime + self.cache_times[module][method]
                pass
            pass
        
        self.__cache_add(key,value,ctime,xtime,cache_notifier=cache_notifier)
        pass
    
    def _cache_delete(self,uri,module,method,version,params):
        # Assemble a list of cache key tags to filter over; all have to
        # be matched for a particular key to be deleted.
        dkeys = []
        if uri:
            dkeys.append("|uri=%s|" % (str(uri),))
        if module:
            dkeys.append("|module=%s|" % (str(module),))
        if method:
            dkeys.append("|method=%s|" % (str(method),))
        if version:
            dkeys.append("|version%s|" % (str(version),))
        if params:
            for (k,v) in params.iteritems():
                dkeys.append("|%s=%s|" % (str(k),str(v),))
                pass
            pass
        
        LOG.debug("CACHE: deleting items matching keys %s" % (str(dkeys),))
        
        self.__get_lock()

        # Assemble a list of matching cache keys.
        deletes = []
        for k in self.cache.keys():
            allfound = True
            for dk in dkeys:
                if k.find(dk) < 0:
                    allfound = False
                    break
                pass
            if not allfound:
                deletes.append(k)
                pass
            pass
        
        # Delete the matching cache keys.
        for dk in deletes:
            LOG.debug("CACHE: deleting matching key %s" % (dk,))
            del self.cache[dk]
            pass
        
        self.__put_lock()
        
        pass
    
    def _cache_load(self):
        if not self.fscache:
            return None
        
        cn = self.get_cache_notifier()
        
        LOG.debug("CACHE: loading cache...")
        cur_time = time.time()
        if not self.fscache:
            return None
        dls = dircache.listdir(self.fscachedir)
        if not dls:
            return
        LOG.debug("CACHE: files %s" % (" ".join(dls),))
        for filename in dls:
            try:
                if not filename.endswith(".ctime"):
                    continue
                LOG.debug("CACHE: considering file %s" % filename,)
                idx = filename.rfind('.')
                h = filename[:idx]
                kf = open("%s/%s.key" % (self.fscachedir,h))
                vf = open("%s/%s.value" % (self.fscachedir,h))
                cf = open("%s/%s.ctime" % (self.fscachedir,h))
                xf = open("%s/%s.xtime" % (self.fscachedir,h))
                key = kf.read()
                kf.close()
                value = pickle.load(vf)
                vf.close()
                ctime = float(cf.read())
                cf.close()
                xtime = float(xf.read())
                xf.close()
                
                if cur_time > xtime:
                    LOG.debug("CACHE: removing stale file %s" % (filename,))
                    # Delete stale entries
                    unlink(vf.name)
                    unlink(cf.name)
                    unlink(xf.name)
                    unlink(kf.name)
                    pass
                else:
                    LOG.debug("CACHE: adding file %s" % (filename,))
                    self.__cache_add(key,value,ctime,xtime)
                    pass
            except:
                pass
            pass
        pass
    
    def loadCache(self):
        """
        Loads the persistent disk cache of RPC results; removes expired values.
        """
        try:
            return self._cache_load()
        except:
            LOG.exception("could not load cache dir %s:" % (self.fscachedir,))
            return None
        pass
    
    def geni_am_response_handler(method, method_args):
        """
        Handles the GENI AM responses, which are different from the
        ProtoGENI responses. ProtoGENI always returns a dict with three
        keys (code, value, and output. GENI AM operations return the
        value, or an XML RPC Fault if there was a problem.
        """
        return apply(method, method_args)

    def geni_sr_response_handler(method, method_args):
        """
        Handles the GENI SR responses, which are different from the
        ProtoGENI responses. ProtoGENI always returns a dict with three
        keys (code, value, and output). GENI SR operations return the
        value, or an XML RPC Fault if there was a problem.
        """
        return apply(method, method_args)
    
    def _passphrase_callback(self,v,prompt1=None,prompt2=None):
        args = { 'passphrasefile':self.passphrasefile }
        if prompt1:
            args["prompt1"] = prompt1
        if prompt2:
            args["prompt2"] = prompt2
        return PassPhraseCB(v,**args)
    
    class CacheNotifier(object):
        """
        A simple cache notifier object, to tell the caller (if it cares)
        whether the return value is cached, is new, and when it expires.
        """
        def __init__(self,server):
            """
            :param server: The :class:`ProtoGeniServer` instance object this CacheNotifier belongs to.
            """
            self.server = server
            """The :class:`ProtoGeniServer` instance object this CacheNotifier belongs to."""
            self.key = None
            """The key for this cached value."""
            self.cached = False
            """True if this value was cached; False if not."""
            self.updated = False
            """True if this value was updated as a result of the operation associated with this CacheNotifier; False if not."""
            self.expires = None
            """The expiration timestamp of this cached value."""
            self.added = None
            """The add/update timestamp (ctime) of this cached value."""
            pass
        
        def reset(self):
            """
            Resets all fields (except `server`) to constructor defaults.

            :return: `self` (a convenience so you can reuse the object trivially by passing ``cache_notifier=notifier.reset()`` to a caching :class:`ProtoGeniServer` method)
            """
            self.key = None
            self.cached = False
            self.updated = False
            self.expires = None
            self.added = None
            return self
        
        def __repr__(self):
            s = super(ProtoGeniServer.CacheNotifier,self).__repr__()
            s += "<%s::::cached=%r,updated=%r,expires=%f,added=%f>" \
              % (self.key,self.cached,self.updated,self.expires,self.added)
            return s
        
        pass

            
    def get_cache_notifier(self):
        """
        :return: a :class:`ProtoGeniServer.CacheNotifier` object that
        can be passed to any :class:`ProtoGeniServer` method that takes
        a `cache_notifier` parameter, to be notified if the return value
        was cached (if not cached, it's new), and when it expires from
        the cache.
        """
        return ProtoGeniServer.CacheNotifier(self)
    
    def do_method(self,module,method,params,URI=None,project=None,quiet=False,version=None,
                  response_handler=None,cm=None,
                  force=False,cache=False,cache_accept_stale=False,
                  cache_add_params=[],cache_destroy_params=[],
                  cache_destroy_extra=[],cache_replace_info=None,
                  cache_notifier=None,cache_expire_func=None):
        """
        Invoke an RPC.
        
        :param module: The RPC server module to invoke `method` within; sa, cm, dynslice, etc.
        :param method: The RPC method to invoke.
        :param params: A dict of params to pass to the method invocation.
        :param URI: A custom URI to use; if not supplied, the full URI is constructed based on the specified @module and @cm.
        :param project: If this is a Project URN (i.e. for ProtoGENI/Cloudlab), this is the project we need to stick into URLs for some SA calls (i.e. Register, RenewSlice).
        :param version: Specify the version of the module/method being invoked (i.e., a str like "2.0"
        :param cm: Call this method on a specific CM; defaults to self.default_cm.
        :param force: If True, don't simply return a cached value even if a valid, unexpired value exists in the cache; call the method on the server.  Otherwise, if a non-zero, positive integer, if the value is cached and the result is older than `force` seconds, also call the method on the server.
        :param cache: True if the caller of do_method wants the result cached.  This is typically used by our method wrappers below to cache values.
        :param cache_accept_stale: True if the caller will accept a stale value.
        :param cache_notifier: A CacheNotifier object; caller can set this to find out if the response was already cached; if it was updated; and when it expires; these metadata are useful in the caller's decision of when to next call a particular method.
        :param cache_add_params,cache_destroy_params,cache_destroy_extra,cache_replace_info: These all control how the value is cached, uncached, or if it triggers destruction or replacement of other cached values.  For instance, calling DeleteSliver at a CM should remove the result of any prior SliverStatus call.  This is complicated to use and should only be used by this class's wrapper functions.
        :param cache_expire_func: A function that is applied to the response from the RPC server to determine its expiration time.  For instance, we use this to extract the expiration time of credentials so that we can age them out in accordance with their expiration time.  Like the arguments above, this should only be called by wrapper functions in this class.
        
        :returns: An (rval,response) tuple.  The rval is a ProtoGeniClientDefs response code; the response is a dict containing a 'value' key/value, whose value is the output from the RPC.
        """
                  
        #
        # Add speaksforcredential for credentials list.
        #
        if "credentials" in params and self.speaksforcredential:
            if 1 or type(params["credentials"]) is tuple:
                params["credentials"] = list(params["credentials"])
                pass
            params["credentials"].append(self.speaksforcredential);
            pass
        
        if URI == None and (module == "cm" or module == "cmv2"
                            or module == 'dynslice'):
            if cm:
                URI = self.cms[cm]
            else:
                URI = self.cms[self.default_cm]
                pass
        elif URI == None and self.sauri and module == "sa":
            URI = self.sauri

        # Save the cache URI -- it's the URI without the module/version
        # info.
        cache_uri = URI
        
        userstr = None
        if URI == None:
            if module in self.endpoints and "host" in self.endpoints[module]:
                addr = self.endpoints[module]["host"]
            else:
                addr = self.endpoints["default"]["host"]
            if module in self.endpoints and "port" in self.endpoints[module]:
                port = self.endpoints[module]["port"]
            else:
                port = self.endpoints["default"]["port"]
            if module in self.endpoints and "path" in self.endpoints[module]:
                path = self.endpoints[module]["path"]
            else:
                path = self.endpoints["default"]["path"]
                
            if module in self.endpoints \
                and "user" in self.endpoints[module] \
                and "pass" in self.endpoints[module]:
                userstr = "%s:%s" % (self.endpoints[module]["user"],
                                      self.endpoints[module]["pass"],)
            elif "user" in self.endpoints["default"] \
                and "pass" in self.endpoints["default"]:
                userstr = "%s:%s" % (self.endpoints["default"]["user"],
                                     self.endpoints["default"]["pass"],)
                pass

            URI = "https://" + addr + ":" + str(port) + path
            cache_uri = URI
            URI += "/" + module
        elif module:
            URI = URI + "/" + module
            pass

        if version:
            URI = URI + "/" + version
            pass

        url = urlsplit(URI, "https")
    
        if self.devuser and url.path.startswith("/protogeni/"):
            upath = url.path.replace("/protogeni/",
                                     "/protogeni/" + self.devuser + "/")
            url = url._replace(path=upath)

        if project is not None and url.path.endswith("/sa"):
            upath = url.path.replace("/sa","/project/%s/sa" % (str(project),))
            url = url._replace(path=upath)

        LOG.debug("URL: %s || %s" % (str(url),method))
        
        #
        # Maybe get the value from the cache
        #
        if cache and (not force or type(force) == int and force > 0):
            if type(force) == int and force > 0:
                max_age = force
            else:
                max_age = None
                pass
            if len(cache_add_params) == 0:
                cparams = params
            else:
                # Filter according to the specific cache params
                cparams = {}           
                for p in params.keys():
                    if p in cache_add_params:
                        cparams[p] = params[p]
                        pass
                    pass
                pass
            value = self._cache_get(cache_uri,module,method,version,cparams,
                                    accept_stale=cache_accept_stale,
                                    max_age=max_age,
                                    cache_notifier=cache_notifier)
            if value is not None:
                return value
            pass

        if url.scheme == "https":
            if not os.path.exists(self.cert):
                LOG.error("missing emulab certificate: %s" % (self.cert,))
                return (-1, None)

            port = url.port if url.port else 443

            ctx = M2Crypto.SSL.Context("sslv23")
            ctx.load_cert_chain(self.cert,self.cert,self._passphrase_callback)
            #ctx.load_client_CA(self.cacert)
            #ctx.load_verify_info(self.cacert)
            #ctx.load_verify_info(cert) #,callback=PassPhraseCB)
            #ctx.set_verify(SSL.verify_fail_if_no_peer_cert | SSL.verify_peer, 16)
            #ctx.set_verify(M2Crypto.SSL.verify_none, 16)
            ctx.set_allow_unknown_ca(0)
    
            if self.noCertHostChecking:
                server = NoHostCheckHTTPSConnection(url.hostname,port,
                                                    ssl_context=ctx)
            else:
                server = M2Crypto.httpslib.HTTPSConnection(url.hostname,port,
                                                           ssl_context=ctx)
                pass
        elif url.scheme == "http":
            port = url.port if url.port else 80
            server = httplib.HTTPConnection(url.hostname,port)
            
        headers = {}
        if userstr:
            headers["Authorization"] = "Basic %s" % (base64.b64encode(userstr).decode("ascii"),)
            pass
        
        if response_handler:
            # If a response handler was passed, use it and return the result.
            # This is the case when running the GENI AM.
            def am_helper(server, path, body):
                server.request("POST", path, body)
                return xmlrpclib.loads(server.getresponse().read())[ 0 ][ 0 ]
            
            return response_handler((lambda *x: am_helper(server, url.path, xmlrpclib.dumps(x, method))), params)

        #
        # Make the call. 
        #
        if LOG.isEnabledFor(logging.DEBUG):
            lparams = dict(params)
            if 'credential' in lparams and len(lparams['credential']) > 64:
                lparams['credential'] = lparams['credential'][:32] + "<<<...>>>" + lparams['credential'][-32:]
            if 'credentials' in lparams and len(str(lparams['credentials'])) > 64:
                lparams['credentials'] = str(lparams['credentials'])[:32] + "<<<...>>>" + str(lparams['credentials'])[-32:]
            LOG.debug("REQUEST: %s %s(%s)" % (str(url),str(method),str(lparams)))
            pass
        while True:
            try:
                server.request("POST",url.path,xmlrpclib.dumps((params,),method),
                               headers=headers)
                response = server.getresponse()
                if response.status == 503:
                    LOG.warning("Status 503; will try again in a few seconds...")
                    time.sleep(5.0)
                    continue
                elif response.status != 200:
                    LOG.warning("Status %d: %s"
                                % (response.status,response.reason))
                    return (-1,None)
                response = xmlrpclib.loads(response.read())[ 0 ][ 0 ]
                break
            except httplib.HTTPException, e:
                LOG.exception("http error")
                return (-1, None)
            except xmlrpclib.Fault, e:
                if e.faultCode == 503:
                    LOG.error("HTTP %s, retrying" % (e.faultString,))
                    time.sleep(5.0)
                    continue
                LOG.error("HTTP %s, aborting" % (e.faultString,))
                return (-1, None)
            except M2Crypto.SSL.Checker.WrongHost, e:
                LOG.exception("certificate host name mismatch (consult http://www.protogeni.net/trac/protogeni/wiki/HostNameMismatch)")
                return (-1, None)

        #
        # Parse the Response, which is a Dictionary. See EmulabResponse in the
        # emulabclient.py module. The XML standard converts classes to a plain
        # Dictionary, hence the code below. 
        # 
        if response["code"] and len(response["output"]):
            LOG.log(logging.DEBUG - 1,
                    "RESPONSE: %s, %s"
                      % (str(response['code']),response['output']))
            pass

        rval = response["code"]

        #
        # If the code indicates failure, look for a "value". Use that as the
        # return value instead of the code. 
        # 
        if rval:
            if response["value"]:
                rval = response["value"]
                pass
            pass
        
        #
        # If the return code indicates success and we have cache destroy
        # stuff, destroy matching cache items!  Note, for
        # cache_destroy_params, we look for a few specific ones
        # (uri,module,method); if the user sets those, we also set
        # delete to filter on those values.  IF THE USER DOESN'T SET
        # THOSE, they are wildcards for the delete.
        #
        if not rval and cache_destroy_params and len(cache_destroy_params) > 0:
            d_uri = None
            d_module = None
            d_method = None
            d_version = None
            if 'uri' in cache_destroy_params:
                d_uri = cache_uri
            if 'module' in cache_destroy_params:
                d_module = module
            if 'method' in cache_destroy_params:
                d_method = method
                d_version = version
                pass
            
            # Filter according to the specific cache params
            cparams = {}           
            for p in params.keys():
                if p in cache_destroy_params:
                    cparams[p] = params[p]
                    pass
                pass
            self._cache_delete(d_uri,d_module,d_method,d_version,cparams)
            pass
        
        #
        # If the return code indicates success and we have extra cache
        # destroy stuff, destroy matching cache items!  Note, for
        # cache_destroy_extra, we look for a few specific ones
        # (uri,module,method); if the user sets those, we also set
        # delete to filter on those values.  IF THE USER DOESN'T SET
        # THOSE, they are wildcards for the delete.
        #
        if not rval \
            and cache_destroy_extra and len(cache_destroy_extra) > 0:
            for cde in cache_destroy_extra:
                cparams = dict(cde)
                d_uri = None
                d_module = None
                d_method = None
                d_version = None
                if cde.has_key('uri'):
                    if cde['uri'] is None:
                        d_uri = cache_uri
                    else:
                        d_uri = cde['uri']
                        pass
                    del cparams['uri']
                    pass
                if cde.has_key('module'):
                    if cde['module'] is None:
                        d_module = module
                    else:
                        d_module = cde['module']
                        pass
                    del cparams['module']
                    pass
                if cde.has_key('method'):
                    if cde['method'] is None:
                        d_method = method
                        d_version = version
                    else:
                        d_method = cde['method']
                        pass
                    del cparams['method']
                    pass
                if cde.has_key('version'):
                    if cde['version'] is None:
                        d_version = version
                    else:
                        d_version = cde['version']
                        pass
                    del cparams['version']
                    pass
            
                self._cache_delete(d_uri,d_module,d_method,d_version,cparams)
                pass
            pass
        
        xtime = None
        if cache_expire_func is not None:
            xtime = cache_expire_func(response)
            pass
        
        #
        # If the output from this is supposed to replace some other cached
        # entry, do that.
        #
        if not rval and cache_replace_info is not None:
            _r_module = module
            if cache_replace_info.has_key('module'):
                _r_module = cache_replace_info['module']
            _r_method = method
            if cache_replace_info.has_key('method'):
                _r_method = cache_replace_info['method']
            _r_version = version
            if cache_replace_info.has_key('version'):
                _r_version = cache_replace_info['version']
            _r_params = {}
            if cache_replace_info.has_key('params'):
                _r_params = cache_replace_info['params']
                
            self._cache_add(cache_uri,_r_module,_r_method,_r_version,_r_params,
                            (rval,response),xtime=xtime,
                            cache_notifier=cache_notifier)
            pass
        
        #
        # If the return code indicates success and we're caching, add it
        # to the cache!
        #
        if not rval and cache:
            if cache_expire_func is not None:
                xtime = cache_expire_func(response)
                pass
            if len(cache_add_params) == 0:
                cparams = params
            else:
                # Filter according to the specific cache params
                cparams = {}           
                for p in params.keys():
                    if p in cache_add_params:
                        cparams[p] = params[p]
                        pass
                    pass
                pass
            self._cache_add(cache_uri,module,method,version,cparams,
                            (rval,response),xtime=xtime,
                            cache_notifier=cache_notifier)
            pass
        
        return (rval, response)

    def do_method_retry(self,module,method,params,URI=None,project=None,retries=200,
                        quiet=False,version=None,response_handler=None,cm=None,
                        force=False,cache=False,cache_accept_stale=False,
                        cache_add_params=[],cache_destroy_params=[],
                        cache_destroy_extra=[],cache_replace_info=None,
                        cache_notifier=None,cache_expire_func=None):
        """
        Retries the RPC @retries times (default 200).
        """
        count = retries
        rval, response = \
            self.do_method(module,method,params,URI=URI,project=project,quiet=quiet,cm=cm,
                           version=version,response_handler=response_handler,
                           force=force,cache=cache,
                           cache_accept_stale=cache_accept_stale,
                           cache_add_params=cache_add_params,
                           cache_destroy_params=cache_destroy_params,
                           cache_replace_info=cache_replace_info,
                           cache_destroy_extra=cache_destroy_extra,
                           cache_notifier=cache_notifier,
                           cache_expire_func=cache_expire_func)
        while count > 0 and response and response["code"] == 14:
            count = count - 1
            LOG.debug("Will try again in a few seconds (%s)" % (str(response),))
            time.sleep(5.0)
            rval, response = \
                self.do_method(module,method,params,URI=URI,project=project,quiet=quiet,cm=cm,
                               version=version,response_handler=response_handler,
                               force=force,cache=cache,
                               cache_accept_stale=cache_accept_stale,
                               cache_add_params=cache_add_params,
                               cache_destroy_params=cache_destroy_params,
                               cache_replace_info=cache_replace_info,
                               cache_destroy_extra=cache_destroy_extra,
                               cache_notifier=cache_notifier,
                               cache_expire_func=cache_expire_func)
        return (rval, response)

    def _expires_within(self,time_tuple,seconds=0):
        tt_exp = time.mktime(time_tuple)
        cgmtime = time.mktime(time.gmtime())
        if (cgmtime + seconds) > tt_exp:
            return True
        else:
            return False
        pass
    
    @staticmethod
    def __get_cred_expire_str(credential):
        try:
            from lxml import etree as ET
            cx = ET.fromstring(credential)
            #r = ET.parse(path)
            return cx.find("credential/expires").text
        except:
            LOG.exception("error getting cred timestamp")
            return None
        pass
    
    def __get_cred_expire_local_timestamp(self,response):
        credential = response['value']
        # grab expiration
        LOG.debug("CACHE: cache-checking credential expiration time")
        expstr = ProtoGeniServer.__get_cred_expire_str(credential)
        if expstr is None:
            return None
        
        lts = GeniTimeStringToLocalTimeStamp(expstr)
        LOG.debug("CACHE: expstr %s -> %f" % (expstr,lts))
        
        if DEFAULT_CREDENTIAL_EXPIRATION_BUFFER > 0:
            lts -= DEFAULT_CREDENTIAL_EXPIRATION_BUFFER
            LOG.debug("CACHE: giving credential a bit less time to expire"
                      " (%.4f hrs less)"
                      % (DEFAULT_CREDENTIAL_EXPIRATION_BUFFER/float(HOUR),))
            pass
        return lts
    
    @ApplicableFormatter(
        kwargs=[dict(name='text',action='store_true'),
                dict(name='json',action='store_true'),
                dict(name='expirestr',action='store_true'),
                dict(name='expirestamp',parser_largs=['-E','--expirestamp'],
                     action='store_true'),
                dict(name='remaining',action='store_true')])
    def _cred_formatter(result,text=None,json=None,
                        expirestr=None,expirestamp=None,remaining=None):
        """
        Format a credential or its expiration time as a string, modulo arguments.
        
        :param text: display credential as plaintext
        :param json: display credential as a JSON object
        :param expirestr: display expiration string
        :param expirestamp: display expiration as local UNIX timestamp
        :param remaining: display seconds remaining until credential expires
        """
        if remaining is True:
            es = ProtoGeniServer.__get_cred_expire_str(result)
            return GeniTimeStringToLocalTimeStamp(es) - time.time()
        elif expirestr is True:
            return ProtoGeniServer.__get_cred_expire_str(result)
        elif expirestamp is True:
            es = ProtoGeniServer.__get_cred_expire_str(result)
            return GeniTimeStringToLocalTimeStamp(es)
        else:
            return get_default_formatter()(result,text=text,json=json)
        pass
    
    @ApplicableMethod(
        kwargs=[dict(name='force',type=parse_bool_or_int)],
        excluded=['cache_notifier'],
        formatter=_cred_formatter)
    def get_self_credential(self,force=False,cache_notifier=None):
        """
        Gets a self credential (SA::GetCredential).
        
        :param force: If True, or if nonzero, positive integer and the cached value has been cached greater than `force` seconds --- bypass the cache.  Otherwise, might return a cached value.
        """
        self.__get_lock()
        
        LOG.debug("Refreshing self credential from SA (force=%s)" % (str(force),))
        params = {}
        rval,response = \
            self.do_method_retry("sa","GetCredential",params,force=force,
                                 cache=True,cache_notifier=cache_notifier,
                                 cache_expire_func=self.__get_cred_expire_local_timestamp)
        if rval:
            self.__put_lock()
            raise exc.RPCError("sa::GetCredential",rval,response,
                           "could not get self credential")
        selfcredential = response["value"]
        
        self.__put_lock()
        
        return selfcredential

    
    @ApplicableMethod(
        excluded=['slice','selfcredential','cache_notifier'],
        kwargs=[dict(name='force',type=parse_bool_or_int)],
        formatter=_cred_formatter)
    def get_slice_credential(self,slice=None,slicename=None,selfcredential=None,
                             force=False,cache_notifier=None):
        """
        Get a slice credential (SA::GetCredential).
        
        :param slicename: Specify a slice URN
        :param force: If True, or if nonzero, positive integer and the cached value has been cached greater than `force` seconds --- bypass the cache.  Otherwise, might return a cached value.
        """
        self.__get_lock()
        
        if not slice:
            if not slicename:
                slicename = self.slicename
                pass
            slice = self.resolve_slice(slicename=slicename,selfcredential=selfcredential,force=force)
            if not slice:
                raise exc.NonexistentSliceError(slicename)
            pass
        
        if not selfcredential:
            selfcredential = self.get_self_credential()
        params = {}
        params["credential"] = selfcredential
        params["type"]       = "Slice"
        if "urn" in slice:
            params["urn"] = slice["urn"]
            slice_id = slice['urn']
        else:
            params["uuid"] = slice["uuid"]
            slice_id = slice['uuid']
            pass
        LOG.debug("Refreshing slice credential from SA (%s)" % (slice_id,))
        rval,response = \
            self.do_method_retry("sa","GetCredential",params,
                                 force=force,cache=True,
                                 cache_add_params=['type','urn','uuid'],
                                 cache_notifier=cache_notifier,
                                 cache_expire_func=self.__get_cred_expire_local_timestamp)
        if rval:
            self.__put_lock()
            raise exc.RPCError("sa::GetCredential",rval,response,
                           "could not get slice credential")
        
        self.__put_lock()
            
        return response['value']

    
    @ApplicableMethod(
        excluded=['selfcredential','cache_notifier'],
        kwargs=[dict(name='force',type=parse_bool_or_int)],
        formatter=_cred_formatter)
    def get_sliver_credential(self,slicename=None,selfcredential=None,
                             force=False,cache_notifier=None):
        """
        Get a sliver credential (CM::GetSliver()).
        
        :param slicename: Specify a slice URN
        :param force: If True, or if nonzero, positive integer and the cached value has been cached greater than `force` seconds --- bypass the cache.  Otherwise, might return a cached value.
        """
        self.__get_lock()
        
        myslice = self.resolve_slice(slicename=slicename)
        slicecred = self.get_slice_credential(
            slicename=slicename,selfcredential=selfcredential,force=force)
        
        params = {}
        params["slice_urn"] = myslice['urn']
        params["credentials"] = (slicecred,)
        LOG.debug("Refreshing sliver credential from CM")
        rval,response = \
            self.do_method_retry("cm","GetSliver",params,version="2.0",
                                 force=force,cache=True,
                                 cache_add_params=['type','urn','uuid'],
                                 cache_notifier=cache_notifier,
                                 cache_expire_func=self.__get_cred_expire_local_timestamp)
        if rval:
            self.__put_lock()
            raise exc.RPCError("cm::GetSliver",rval,response,
                           "could not get sliver credential")
        
        self.__put_lock()
            
        return response['value']


    @ApplicableMethod(
        excluded=['selfcredential','cache_notifier'],
        kwargs=[dict(name='force',type=parse_bool_or_int)])
    def resolve_slice(self,slicename=None,selfcredential=None,force=False,
                      cache_notifier=None):
        """
        Resolve a slice (SA::Resolve(type=slice)).
        
        :param slicename: Specify a slice URN
        :param force: If True, or if nonzero, positive integer and the cached value has been cached greater than `force` seconds --- bypass the cache.  Otherwise, might return a cached value.
        """
        self.__get_lock()

        if not selfcredential:
            selfcredential = self.get_self_credential()
        if not slicename:
            slicename = self.slicename

        params = {}
        params["credential"] = selfcredential
        params["type"]       = "Slice"
        if slicename.startswith("urn:"):
            pname = "urn"
            params["urn"]       = slicename
        else:
            pname = "hrn"
            params["hrn"]       = slicename
            pass
        
        while True:
            rval,response = \
                self.do_method_retry("sa","Resolve",params,version="2.0",
                                     force=force,
                                     cache=True,cache_add_params=["type",pname],
                                     cache_notifier=cache_notifier)
            if rval:
                LOG.debug("Resolve: %s" % (str(rval)))
                self.__put_lock()
                return None
            else:
                break
            pass

        self.__put_lock()
        
        return response["value"]

    @ApplicableMethod(
        excluded=['selfcredential'],
        help="Get expiration time for slice",
        kwargs=[dict(name='force',type=parse_bool_or_int)])
    def get_slice_expiration(self,slicename=None,selfcredential=None,force=False):
        """
        Get the slice expiration as a local UNIX timestamp (SA::Resolve).

        :param slicename: Specify a slice URN
        :param force: If True, or if nonzero, positive integer and the cached value has been cached greater than `force` seconds --- bypass the cache.  Otherwise, might return a cached value.
        """
        self.__get_lock()
        
        if not selfcredential:
            selfcredential = self.get_self_credential()
        if not slicename:
            slicename = self.slicename
        
        slice = self.resolve_slice(slicename=slicename,selfcredential=selfcredential,
                                   force=force)
        
        slicecredential = self.get_slice_credential(slice,force=force)
        
        try:
            from lxml import etree as ET
            cx = ET.fromstring(slicecredential)
            expstr = cx.find("credential/expires").text
            self.__put_lock()
            return GeniTimeStringToLocalTimeStamp(expstr)
        except:
            self.__put_lock()
            return None
        pass

    @ApplicableMethod(
        excluded=['selfcredential','cache_notifier'])
    def resolve_user(self,urn=None,selfcredential=None,cache_notifier=None):
        """
        Resolve a GENI user (SA::Resolve(type=user).
        
        :param urn: Specify a user URN
        """
        self.__get_lock()
        if not selfcredential:
            selfcredential = self.get_self_credential()
        
        if not urn:
            if self.user_urn:
                urn = self.user_urn
            else:
                myslice = self.resolve_slice(selfcredential=selfcredential)
                urn = myslice['creator_urn']
                pass
            pass

        params = {}
        params["credential"] = selfcredential
        params["urn"] = urn
        params["type"] = 'User'
        rval,response = self.do_method("sa","Resolve",params,version="2.0",
                                       cache=True,cache_add_params=["urn","type"],
                                       cache_notifier=cache_notifier)
        if rval:
            self.__put_lock()
            LOG.error("could not resolve user at SA!")
            return None
        myuser = response["value"]
        LOG.debug("Got user: %s (%s)" % (str(rval),str(response),))
        self.__put_lock()
        return myuser

    @ApplicableMethod()
    def get_user_urn(self):
        """
        Get your user URN.
        """
        if self.user_urn:
            return self.user_urn
        else:
            myslice = self.resolve_slice()
            return myslice['creator_urn']
        pass
    
    @ApplicableMethod()
    def get_user_email(self):
        """
        Get your user email.
        """
        if self.user_email:
            return self.user_email
        else:
            myuser = self.resolve_user()
            return myuser['email']
        pass
    
    @ApplicableMethod()
    def get_user_uid(self):
        """
        Get your user uid.
        """
        myuser = self.resolve_user()
        return myuser['uid']

    @ApplicableMethod(
        excluded=['selfcredential','cache_notifier'])
    def get_keys(self,selfcredential=None,cache_notifier=None):
        """
        Get user's keys from the SA (SA:GetKeys).
        """
        self.__get_lock()
        if not selfcredential:
            selfcredential = self.get_self_credential()

        params = {}
        params["credential"] = selfcredential
        rval,response = self.do_method("sa","GetKeys",params,
                                       cache=True,cache_notifier=cache_notifier)
        if rval:
            self.__put_lock()
            LOG.error("could not get keys from SA!")
            return None
        keys = response["value"]
        LOG.debug("Got keys: %s (%s)" % (str(rval),str(response),))
        self.__put_lock()
        return keys

    @ApplicableMethod(
        excluded=['selfcredential'])
    def create_slice(self,slicename=None,valid_until=None,selfcredential=None):
        """
        Create a slice.
        
        :param slicename: Specify a slice URN
        :param valid_until: Specify a slice expiration time (as a GMT time string)
        """
        self.__get_lock()
        if not selfcredential:
            selfcredential = self.get_self_credential()
        if not slicename:
            slicename = self.slicename
        if not valid_until:
            valid_until = time.strftime("%Y%m%dT%H:%M:%S",
                                        time.gmtime(time.time() + 60*60*24))

        params = {}
        params["credential"] = selfcredential
        params["type"]       = "Slice"
        project = None
        if slicename.startswith("urn:"):
            params["urn"]       = slicename
            uchunks = slicename.split('+')
            if len(uchunks) == 4:
                hnchunks = uchunks[1].split(":")
                if len(hnchunks) >= 2:
                    project = hnchunks[1]
        else:
            params["hrn"]       = slicename
            pass
        params["expiration"] = valid_until
        
        rval,response = self.do_method("sa", "Register", params, project=project)
        if rval:
            self.__put_lock()
            raise exc.RPCError("sa::Register",rval,response,
                           "could not register slice %s" % (slicename,))
        myslice = response["value"]
        LOG.debug("Created slice %s: %s (%s)" % (slicename,str(rval),str(response),))
        self.__put_lock()
        return myslice

    @ApplicableMethod(
        excluded=['selfcredential'],
        kwargs=[dict(name='cmlist',type=parse_cm_list)])
    def delete_slice(self,slicename=None,cmlist=None,selfcredential=None):
        """
        Delete a slice.
        
        :param slicename: Specify a slice URN
        :param cmlist: A comma-separated list of CMs to delete slivers at; defaults to the default CM if unspecified.
        """
        self.__get_lock()
        
        if not selfcredential:
            selfcredential = self.get_self_credential()
        if not slicename:
            slicename = self.slicename
            pass
        if not cmlist or len(cmlist) < 1:
            cmlist = [self.default_cm,]
            pass
        
        myslice = self.resolve_slice(slicename=slicename)
        slicecredential = self.get_slice_credential(myslice,
                                                    selfcredential=selfcredential)
        
        if cmlist and len(cmlist) > 0:
            #
            # Delete at CMs too.
            #
            for cm in cmlist:
                params = {}
                params["slice_urn"]   = myslice['urn']
                params["credentials"] = (slicecredential,)
                rval,response = self.do_method_retry("cm","DeleteSlice",params,cm=cm,
                                               version="2.0")
                if rval:
                    LOG.error("DeleteSlice(%s,%s): %s, %s"
                      % (myslice['urn'],cm,str(rval),str(response),))
                else:
                    LOG.debug("Deleted slice %s at CM %s" % (myslice['urn'],cm))
                pass
            pass
        
        params = {}
        params['type']  = 'Slice'
        params["urn"]   = myslice['urn']
        params["credentials"] = (selfcredential,)
        rval,response = self.do_method_retry("sa","Remove",params,version="2.0")
        if rval:
            self.__put_lock()
            raise exc.RPCError("sa::Remove",rval,response,
                           "could not delete slice")
        LOG.debug("Deleted slice %s" % (myslice['urn'],))
        self.__put_lock()
        return response["value"]

    @ApplicableMethod(
        excluded=['selfcredential'],
        kwargs=[dict(name='additional_seconds',type=int),
                dict(name='cmlist',type=parse_cm_list)])
    def renew_slice(self,slicename=None,additional_seconds=None,expiration_time=None,
                    cmlist=None,selfcredential=None):
        """
        Renew a slice (and possibly its slivers).
        
        :param slicename: Specify a slice URN
        :param expiration_time: Specify an expiration time string, in GMT
        :param additional_seconds: If expiration_time is not set, and this is, <additional_seconds> will be added to your slice\'s current expiration time.
        :param cmlist: A comma-separated list of CMs to renew slivers at; defaults to the default CM if unspecified.
        """
        self.__get_lock()
        if not slicename:
            slicename = self.slicename
            pass
        if expiration_time:
            expiration = time.strftime("%Y%m%dT%H:%M:%S",
                                       time.gmtime(expiration_time))
        else:
            if not additional_seconds:
                additional_seconds = DAY
            expiration_time = time.gmtime(time.time() + additional_seconds)
            expiration = time.strftime("%Y%m%dT%H:%M:%S",expiration_time)
            pass
        
        myslice = self.resolve_slice(slicename=slicename)
        slicecredential = self.get_slice_credential(myslice,
                                                    selfcredential=selfcredential)

        slice_id = None
        project = None
        if 'urn' in myslice:
            slice_id = myslice['urn']
            uchunks = slice_id.split('+')
            if len(uchunks) == 4:
                hnchunks = uchunks[2].split(":")
                if len(hnchunks) >= 2:
                    project = hnchunks[1]
        else:
            slice_id = myslice['uuid']
        
        params = {}
        params["slice_urn"]   = myslice['urn']
        params["credentials"] = (slicecredential,)
        params["expiration"] = expiration
        crd = { 'method':'GetCredential','params':{ 'urn':myslice['urn'],
                                                    'type':'Slice' } }
        #
        # NB: the output of this function replaces our current slice credential
        # from GetCredential, so replace it in the cache too.
        #
        rval,response = \
            self.do_method_retry("sa","RenewSlice",params,project=project,
                                 cache=False,cache_replace_info=crd,
                                 cache_expire_func=self.__get_cred_expire_local_timestamp)
        if rval:
            self.__put_lock()
            raise exc.RPCError("sa::RenewSlice",rval,response,
                           "could not renew slice")
        
        slicecredential = response['value']

        # grab expiration
        try:
            from lxml import etree as ET
            cx = ET.fromstring(response['value'])
            #r = ET.parse(path)
            expstr = cx.find("credential/expires").text
        except:
            expstr = expiration
            pass
        
        if cmlist and len(cmlist) > 0:
            #
            # Renew at CMs too.
            #
            for cm in cmlist:
                params = {}
                params["slice_urn"]   = myslice['urn']
                params["credentials"] = (slicecredential,)
                params["expiration"] = expstr
                rval,response = \
                    self.do_method_retry("cm","RenewSlice",params,cm=cm,
                                         version="2.0")
                if rval:
                    self.__put_lock()
                    raise exc.RPCError("cm::RenewSlice",rval,response,
                                   "could not renew sliver")
                pass
            pass
        
        self.__put_lock()

        return slicecredential

    @ApplicableMethod(
        excluded=['selfcredential'],
        largs=[dict(name='rspec',type=parse_rspec_arg,
                     help='An RSpec XML string or a file containing an RSpec')],
        kwargs=[dict(name='dokeys',type=parse_bool),
                dict(name='gen_random_key',action='store_true')])
    def create_sliver(self,rspec,cm=None,slicename=None,dokeys=True,
                      selfcredential=None,gen_random_key=False):
        """
        Create a sliver at a CM.
        
        :param rspec: An RSpec as an XML string
        :param slicename: Specify a slice URN
        :param cm: Specify a CM; otherwise the default CM will be used
        :param dokeys: Install your pubkeys in the sliver if True
        :param gen_random_key: Generate an unencrypted RSA key that will be installed on each node if True
        """
        self.__get_lock()
        
        if not selfcredential:
            selfcredential = self.get_self_credential()
        if not slicename:
            slicename = self.slicename

        #
        # Lookup my ssh keys.
        #
        if dokeys:
            myuserurn = self.get_user_urn()
            myuseruid = self.get_user_uid()
            mykeys = self.get_keys()
        else:
            myuserurn = None
            myuseruid = None
            mykeys = None
            pass

        #
        # Lookup slice and get credential.
        #
        myslice = self.resolve_slice(slicename=slicename)
        slicecredential = self.get_slice_credential(myslice,
                                                    selfcredential=selfcredential)
        LOG.debug("Got the slice credential")

        #
        # Create the sliver.
        #
        LOG.debug("Creating the sliver ...")
        params = {}
        params["credentials"] = (slicecredential,)
        params["slice_urn"]   = myslice["urn"]
        params["rspec"]       = rspec
        if dokeys:
            params["keys"] = [ { 'urn':myuserurn,'login':myuseruid,'keys':mykeys } ]
            pass
        # Give each node a key that is unencrypted.
        if gen_random_key:
            fd = os.popen('openssl genpkey -algorithm RSA -pass pass:')
            params["key"] = fd.read()
            fd.close()
            pass
        rval,response = self.do_method("cm","CreateSliver",params,version="2.0",
                                       cm=cm)
        if rval:
            LOG.error("CreateSliver: %s, %s"
                      % (str(rval),str(response),))
            self.__put_lock()
            raise exc.RPCError("cm::CreateSliver",rval,response,
                           "could not create sliver")
        sliver,manifest = response["value"]
        LOG.debug("Created the sliver (%s, %s)" % (str(rval),str(response)))
        self.__put_lock()
        return (sliver,manifest)

    @ApplicableMethod(
        excluded=['selfcredential','cache_notifier'],
        kwargs=[dict(name='force',type=parse_bool_or_int)])
    def resolve_sliver(self,cm=None,slicename=None,selfcredential=None,force=False,
                       cache_notifier=None):
        """
        Resolve a sliver at a CM.
        
        :param slicename: Specify a slice URN
        :param cm: Specify a CM; otherwise the defaul CM will be used
        :param force: If True, or if nonzero, positive integer and the cached value has been cached greater than `force` seconds --- bypass the cache.  Otherwise, might return a cached value.
        """
        self.__get_lock()
        
        if not slicename:
            slicename = self.slicename
            pass

        #
        # Lookup slice and get credential.
        #
        myslice = self.resolve_slice(slicename=slicename)
        slicecredential = self.get_slice_credential(myslice,
                                                    selfcredential=selfcredential)

        #
        # Resolve the sliver.
        #
        LOG.debug("Resolving the sliver ...")
        params = {}
        params["credentials"] = (slicecredential,)
        params["urn"]   = myslice["urn"]
        rval,response = \
            self.do_method_retry("cm","Resolve",params,cm=cm,version="2.0",
                                 force=force,cache=True,cache_add_params=['urn'],
                                 cache_notifier=cache_notifier)
        if rval:
            LOG.error("Could not resolve sliver at CM (%s, %s)"
                      % (str(rval),str(response)))
            self.__put_lock()
            return None
        mysliver = response["value"]
        LOG.debug("Resolved the sliver: %s" % (str(mysliver),))
        self.__put_lock()
        return mysliver

    @ApplicableMethod(
        help="Get a sliver manifest from a CM",
        excluded=['selfcredential','cache_notifier'],
        kwargs=[dict(name='force',type=parse_bool_or_int)])
    def get_manifest(self,cm=None,slicename=None,force=False):
        """
        Get a sliver manifest from a CM.
        
        :param slicename: Specify a slice URN
        :param cm: Specify a CM; otherwise the defaul CM will be used
        :param force: If True, or if nonzero, positive integer and the cached value has been cached greater than `force` seconds --- bypass the cache.  Otherwise, might return a cached value.
        """
        mysliver = self.resolve_sliver(cm=cm,slicename=slicename,force=force)
        return mysliver['manifest']
    
    def get_wrapped_manifest(self,cm=None,slicename=None,force=False):
        mysliver = self.resolve_sliver(cm=cm,slicename=slicename,force=force)
        if not mysliver.has_key('wrapped_manifest'):
            mysliver['wrapped_manifest'] = \
                ProtoGeniManifestWrapper(xml=mysliver['manifest'])
            pass
        return mysliver['wrapped_manifest']

    @ApplicableMethod(
        kwargs=[dict(name='force',type=parse_bool_or_int)])
    def get_hostname_and_port(self,client_id,cm=None,slicename=None,force=False):
        """
        Get a hostname and SSH port for the given `client_id`.
        
        :param client_id: A GENI client_id (the logical name for a resource)
        :param slicename: Specify a slice URN
        :param cm: Specify a CM; otherwise the defaul CM will be used
        :param force: If True, or if nonzero, positive integer and the cached value has been cached greater than `force` seconds --- bypass the cache.  Otherwise, might return a cached value.
        """
        wm = self.get_wrapped_manifest(cm=cm,slicename=slicename,force=force)
        return wm.getHostnameAndPort(client_id)
    
    @ApplicableMethod(
        kwargs=[dict(name='force',type=parse_bool_or_int),])
    def get_sliver_urn(self,cm=None,slicename=None,force=False):
        """
        Get a sliver URN at a CM.
        
        :param slicename: Specify a slice URN
        :param cm: Specify a CM; otherwise the defaul CM will be used
        :param force: If True, or if nonzero, positive integer and the cached value has been cached greater than `force` seconds --- bypass the cache.  Otherwise, might return a cached value.
        """
        mysliver = self.resolve_sliver(cm=cm,slicename=slicename,force=force)
        return mysliver['sliver_urn']

    @ApplicableMethod(
        excluded=['selfcredential'])
    def delete_sliver(self,cm=None,slicename=None,selfcredential=None):
        """
        Delete a sliver at a CM.
        
        :param slicename: Specify a slice URN
        :param cm: Specify a CM; otherwise the defaul CM will be used
        """
        self.__get_lock()
        
        if not slicename:
            slicename = self.slicename

        #
        # Lookup slice and get credential.
        #
        myslice = self.resolve_slice(slicename=slicename)
        slicecredential = self.get_slice_credential(myslice,
                                                    selfcredential=selfcredential)
        LOG.debug("Got the slice credential")

        #
        # Delete the sliver.
        #
        params = {}
        params["credentials"] = (slicecredential,)
        params["slice_urn"]   = myslice["urn"]
        # For cache purposes, we have to delete everything from this CM
        # whose urn matches our slice_urn, because the Resolve() data in
        # resolve_sliver was called with a 'urn' param, not a
        # 'slice_urn' param, so our delete filter won't catch that.  So
        # we supply an extra filter...
        rval,response = \
            self.do_method_retry("cm","DeleteSlice",params,cm=cm,version="2.0",
                                 cache_destroy_params=['uri','module','slice_urn'],
                                 cache_destroy_extra=[{ 'uri':None,
                                                        'module':None,
                                                        'urn':myslice['urn'] }])
        if rval:
            LOG.error("DeleteSlice: %s, %s"
                      % (str(rval),str(response),))
            self.__put_lock()
            raise exc.RPCError("cm::DeleteSlice",rval,response,
                           "could not delete sliver")
        LOG.debug("Sliver has been deleted. Ticket for remaining time: %s"
                  % (str(response['value']),))
        self.__put_lock()
        return response['value']

    @ApplicableMethod(
        excluded=['selfcredential','cache_notifier'],
        kwargs=[dict(name='force',type=parse_bool_or_int)])
    def sliver_status(self,cm=None,slicename=None,selfcredential=None,force=False,
                      cache_notifier=None):
        """
        Get sliver status from a CM.
        
        :param slicename: Specify a slice URN
        :param cm: Specify a CM; otherwise the defaul CM will be used
        :param force: If True, or if nonzero, positive integer and the cached value has been cached greater than `force` seconds --- bypass the cache.  Otherwise, might return a cached value.
        """
        self.__get_lock()
        
        if not slicename:
            slicename = self.slicename

        #
        # Lookup slice and get credential.
        #
        myslice = self.resolve_slice(slicename=slicename)
        slicecredential = self.get_slice_credential(myslice,
                                                    selfcredential=selfcredential)

        #
        # Renew the sliver.
        #
        LOG.debug("Getting the sliver status...")
        params = {}
        params["credentials"] = (slicecredential,)
        params["slice_urn"]   = myslice["urn"]
        rval,response = \
            self.do_method_retry("cm","SliverStatus",params,cm=cm,version="2.0",
                                 force=force,cache=True,
                                 cache_add_params=['slice_urn'],
                                 cache_notifier=cache_notifier)
        if rval:
            LOG.error("SliverStatus: %s, %s"
                      % (str(rval),str(response),))
            self.__put_lock()
            raise exc.RPCError("cm::SliverStatus",rval,response,
                           "could not get sliver status")
        status = response["value"]
        LOG.debug("Got the sliver status: %s, %s" % (str(rval),str(response),))
        self.__put_lock()
        return status
    
    @ApplicableFormatter(
        kwargs=[dict(name='text',action='store_true'),
                dict(name='json',action='store_true'),
                dict(name='primarykey',
                     choices=['sliver_id','component_urn','client_id']),
                dict(name='keys',type=parse_str_list),
                dict(name='listmode',action='store_true'),])
    def _status_detail_formatter(result,text=None,json=None,
                                 primarykey='component_urn',keys=None,
                                 listmode=False):
        """
        Format sliver resource status details as a string, modulo arguments.
        
        :param text: display status details as plaintext
        :param json: display status details as a JSON object
        :param primarykey: the first-level key in the result dictionary
        :param keys: a list of per-resource keys you want to see ('status','utc','rawstate','nodestatus','execute_state','state','component_urn','sliver_id','client_id','error','execute_status').  If the argument is unspecified, None, or an empty list, it will show all keys.
        :param listmode: instead of returning a dict with your chosen primarykey, return a list of dicts with the keys you selected
        """
        if keys is None or len(keys) < 1:
            _keys = [ 'status','utc','rawstate','nodestatus','execute_state',
                      'state','component_urn','sliver_id','client_id',
                      'error','execute_status' ]
        else:
            _keys = keys
            pass
        if primarykey not in _keys:
            _keys.insert(0,primarykey)
            pass
        
        if listmode:
            ret = list()
        else:
            ret = dict()
            pass
        
        for (sliver_id,d) in result.iteritems():
            nd = dict()
            for k in _keys:
                if d.has_key(k):
                    nd[k] = d[k]
                    pass
                pass
            
            if listmode:
                ret.append(nd)
            else:
                pk = d[primarykey]
                ret[pk] = d
                pass
            pass
        
        return get_default_formatter()(ret,text=text,json=json)

    @ApplicableMethod(
        excluded=['selfcredential','cache_notifier'],
        kwargs=[dict(name='force',type=parse_bool_or_int),
                dict(name='nodesonly',action='store_true')],
        formatter=_status_detail_formatter)
    def sliver_status_details(self,cm=None,slicename=None,selfcredential=None,
                              force=False,cache_notifier=None,
                              nodesonly=False,):
        """
        Get status for all resources in a sliver from a CM.
        
        :param slicename: Specify a slice URN
        :param cm: Specify a CM; otherwise the defaul CM will be used
        :param force: If True, or if nonzero, positive integer and the cached value has been cached greater than `force` seconds --- bypass the cache.  Otherwise, might return a cached value.
        :param nodesonly: Only show node status (i.e., not lans/links/etc)
        """
        ss = self.sliver_status(
            cm=cm,slicename=slicename,selfcredential=selfcredential,
            force=force,cache_notifier=cache_notifier)
        
        ret = dict()
        for (sliver_id,d) in ss['details'].iteritems():
            if nodesonly and not d.has_key('nodestatus'):
                continue
            d['sliver_id'] = sliver_id
            ret[sliver_id] = d
            pass
        return ret
    
    @ApplicableMethod(
        excluded=['selfcredential'])
    def renew_sliver(self,cm=None,slicename=None,selfcredential=None):
        """
        Renew sliver at a CM using the current slice expiration time.
        
        :param slicename: Specify a slice URN
        :param cm: Specify a CM; otherwise the defaul CM will be used
        """
        self.__get_lock()
        
        if not slicename:
            slicename = self.slicename

        #
        # Lookup slice and get credential.
        #
        myslice = self.resolve_slice(slicename=slicename)
        urn = myslice['urn']
        slicecredential = self.get_slice_credential(myslice,
                                                    selfcredential=selfcredential)

        
        # grab expiration
        try:
            from lxml import etree as ET
            cx = ET.fromstring(slicecredential)
            #r = ET.parse(path)
            expstr = cx.find("credential/expires").text
            LOG.debug("Slice credential expires %s" % (expstr,))
            pass
        except:
            pass
        
        #
        # Renew the sliver.
        #
        LOG.debug("Renewing the sliver ...")
        params = {}
        params["credentials"] = (slicecredential,)
        params["slice_urn"]   = urn
        params["expiration"]  = expstr
        rval,response = self.do_method_retry("cm","RenewSlice",params,
                                             version="2.0")
        if rval:
            LOG.error("RenewSlice: %s, %s"
                      % (str(rval),str(response),))
            self.__put_lock()
            raise exc.RPCError("cm::RenewSlice",rval,response,
                           "could not renew sliver")
        LOG.debug("Renewed the sliver: %s" % (str(rval),))
        self.__put_lock()
        return rval
    
    @ApplicableFormatter(
        kwargs=[dict(name='text',action='store_true'),
                dict(name='json',action='store_true'),
                dict(name='exclusive',action='store_true'),
                dict(name='shared',action='store_true'),
                dict(name='primarykey',
                     choices=['component_id','name','exclusive','available',
                              'hardware_types','shared','interfaces',
                              'location','ram','cpu']),
                dict(name='listmode',action='store_true'),
                dict(name='listprimarykeysonly',action='store_true',
                     parser_largs=('-L','--listprimarykeysonly')),
                dict(name='keys',type=parse_str_list)])
    def _resources_formatter(result,text=None,json=None,
                             primarykey='name',keys=None,
                             listmode=False,listprimarykeysonly=False,
                             nodetype=None,exclusive=False,shared=False):
        """
        Format an RSpec resource advertisement.
        
        :param text: display result as plaintext
        :param json: display result as a JSON object
        :param primarykey: the first-level key in the result dictionary
        :param keys: a list of per-resource keys you want to see ('component_id','name','exclusive','available','hardware_types','shared','interfaces','location','ram','cpu').  If the argument is unspecified or None, it will show all keys.
        :param listmode: display each key/val in the dict as a separate line (this has the effect of moving the primary key into the dict, so each line is dict).
        :param listprimarykeysonly: display only a list of matching primary keys
        :param nodetype: display only nodes of this type
        :param exclusive: display only exclusively-allocable nodes
        :param shared: display only shared-allocable nodes
        """
        if keys is None:
            keys = [ 'component_id','name','exclusive','available','shared',
                     'hardware_types','interfaces','location','ram','cpu' ]
            pass
            
        ad = RSpecAd.Advertisement(xml=result)
        if not listprimarykeysonly and not listmode:
            ret = dict()
        else:
            ret = list()
            pass
        
        for n in ad.nodes:
            if exclusive and not n.exclusive:
                continue
            if shared and not n.shared:
                continue
            if nodetype is not None and not n.hardware_types.has_key(nodetype):
                continue
            
            pk = getattr(n,primarykey)
            
            # This just wants a simple list
            if listprimarykeysonly:
                ret.append(pk)
                continue

            kdict = dict()
            # This wants a list of dicts
            if listmode:
                kdict[primarykey] = pk
                ret.append(kdict)
            # This wants a dict of dicts
            else:
                ret[pk] = kdict
                pass

            for k in keys:
                kv = None
                if k == 'interfaces':
                    ilist = getattr(n,k)
                    if not ilist:
                        kv = None
                    else:
                        kv = list()
                        for i in ilist:
                            kv.append(dict(component_id=i.component_id,
                                           name=i.name,role=i.role))
                            pass
                        pass
                    pass
                elif k == 'location':
                    kv = getattr(n,k)
                    kv = (kv.latitude,kv.longitude)
                elif hasattr(n,k):
                    kv = getattr(n,k)
                    pass
                
                if kv is not None:
                    kdict[k] = kv
                pass
            pass
        
        LOG.debug("ret = %s" % (ret))
        
        return get_default_formatter()(ret,text=text,json=json)

    @ApplicableMethod(
        excluded=['URI','selfcredential','cache_notifier'],
        kwargs=[dict(name='available',action='store_true'),
                dict(name='force',type=parse_bool_or_int)],
        formatter=_resources_formatter)
    def get_resources(self,cm=None,available=False,URI=None,selfcredential=None,
                      force=False,cache_notifier=None):
        """
        Get a resource advertisement from a CM.
        
        :param available: Only list currently-available resources if True
        :param cm: Specify a CM; otherwise the defaul CM will be used
        :param force: If True, or if nonzero, positive integer and the cached value has been cached greater than `force` seconds --- bypass the cache.  Otherwise, might return a cached value.
        """
        if not selfcredential:
            selfcredential = self.get_self_credential()
            pass
        
        params = {}
        params["credentials"] = [selfcredential,]
        params["rspec_version"] = 3
        params["available"] = available
        params["compress"] = True
        rval,response = \
            self.do_method_retry("cm","DiscoverResources",
                                 params,version="2.0",cm=cm,URI=URI,force=force,
                                 cache=True,cache_add_params=['rspec_version','available','compress'],
                                 cache_notifier=cache_notifier)
        if rval:
            LOG.error("DiscoverResources: %s, %s"
                      % (str(rval),str(response),))
        else:
            if isinstance(response["value"],xmlrpclib.Binary):
                response["value"] = zlib.decompress(str(response["value" ]))
        LOG.log(logging.DEBUG - 1,"GetResources: %s" % (str(response["value"]),))
        return response["value"]
    
    def set_client_endpoint(self,clientserver,defwaittime=None,policy=None,
                            cm=None,slicename=None,selfcredential=None):
        self.__get_lock()
        
        if not slicename:
            slicename = self.slicename

        #
        # Lookup slice and get credential.
        #
        myslice = self.resolve_slice(slicename=slicename)
        slicecredential = self.get_slice_credential(myslice,
                                                    selfcredential=selfcredential)

        #
        # Renew the sliver.
        #
        LOG.debug("Setting client endpoint ...")
        params = {}
        params["slice_urn"]  = myslice["urn"]
        params["credentials"] = (slicecredential,)
        params['url']        = clientserver.getURL()
        if defwaittime:
            params['defwaittime'] = defwaittime
        if policy:
            params['policy']      = policy
        rval,response = self.do_method_retry("dynslice","SetSliceClientEndpoint",
                                             params)
        if rval:
            LOG.error("SetSliceClientEndpoint: %s, %s"
                      % (str(rval),str(response),))
            self.__put_lock()
            raise exc.RPCError("dynslice::SetSliceClientEndpoint",rval,response)
        LOG.debug("Set client endpoint: %s" % (str(rval),))
        self.__put_lock()
        return rval
    
    def set_client_endpoint_options(self,defwaittime=None,policy=None,
                                    cm=None,slicename=None,selfcredential=None):
        self.__get_lock()
        
        if not slicename:
            slicename = self.slicename

        #
        # Lookup slice and get credential.
        #
        myslice = self.resolve_slice(slicename=slicename)
        slicecredential = self.get_slice_credential(myslice,
                                                    selfcredential=selfcredential)

        #
        # Renew the sliver.
        #
        LOG.debug("Setting client endpoint ...")
        params = {}
        params["slice_urn"]  = myslice["urn"]
        params["credentials"] = (slicecredential,)
        if defwaittime:
            params['defwaittime'] = defwaittime
        if policy:
            params['policy']      = policy
        rval,response = self.do_method_retry("dynslice","SetSliceClientOptions",
                                             params)
        if rval:
            LOG.error("SetSliceClientOptions: %s, %s"
                      % (str(rval),str(response),))
            self.__put_lock()
            raise exc.RPCError("dynslice::SetSliceClientOptions",rval,response)
        LOG.debug("Set client endpoint: %s" % (str(rval),))
        self.__put_lock()
        return rval

    @ApplicableMethod(
        excluded=['selfcredential','URI'],
        kwargs=[dict(name='nodes',type=parse_safe_eval,required=True)])
    def add_nodes(self,nodes={},slicename=None,cm=None,URI=None,selfcredential=None):
        """
        Add nodes to a sliver at a CM.
        
        :param nodes: A Python dictionary where each key/val pair comforms to the specifications for the 'nodes' parameter to the CMv2::AddNodes method.
        :param slicename: Specify a slice URN
        :param cm: Specify a CM; otherwise the default CM will be used
        """
        self.__get_lock()
        
        if not nodes or len(nodes.keys()) < 1:
            raise exc.InvalidArgumentError("nodes list must have at least one element")
        if not (type(nodes) == list or type(nodes) == dict):
            self.__put_lock()
            raise exc.InvalidArgumentError("nodes argument must be a list or dict")
    
        #
        # Lookup slice.
        #
        myslice = self.resolve_slice(slicename=slicename,selfcredential=selfcredential)

        #
        # Get the slice credential.
        #
        slicecred = self.get_slice_credential(myslice,
                                              selfcredential=selfcredential)

        #
        # Add a node.
        #
        params = {}
        params["slice_urn"]   = myslice["urn"]
        params["credentials"] = (slicecred,)
        params["nodes"]       = nodes
        rval,manifest = self.do_method_retry(
            "cm","AddNodes",params,version="2.0",cm=cm,URI=URI,
            cache_destroy_extra=[dict(uri=None,module='cm',method='Resolve',
                                      version="2.0",urn=myslice['urn']),
                                 dict(uri=None,module='cm',method='SliverStatus',
                                      version="2.0",slice_urn=myslice['urn'])])
        if rval:
            LOG.error("AddNodes: %s, %s"
                      % (str(rval),str(manifest),))
            self.__put_lock()
            raise exc.RPCError("cm::AddNodes",rval,manifest)
        else:
            LOG.debug("Added nodes: new manifest: %s" % (str(manifest),))
            pass
        self.__put_lock()
        return manifest
    
    @ApplicableMethod(
        excluded=['selfcredential','URI'],
        kwargs=[dict(name='nodes',type=parse_safe_list_or_eval,required=True)])
    def delete_nodes(self,nodes=[],slicename=None,cm=None,URI=None,selfcredential=None):
        """
        Delete nodes from a sliver at a CM.
        
        :param nodes: A list of logical or physical node IDs that are in your slice.
        :param slicename: Specify a slice URN
        :param cm: Specify a CM; otherwise the default CM will be used
        """
        if not nodes or len(nodes) < 1:
            raise exc.InvalidArgumentError("nodes list must have at least one element")

        self.__get_lock()
        
        #
        # Lookup slice.
        #
        myslice = self.resolve_slice(slicename=slicename,selfcredential=selfcredential)

        #
        # Get the slice credential.
        #
        slicecred = self.get_slice_credential(myslice,
                                              selfcredential=selfcredential)

        #
        # Add a node.
        #
        params = {}
        params["slice_urn"]   = myslice["urn"]
        params["credentials"] = (slicecred,)
        params["nodes"]       = nodes
        rval,response = self.do_method_retry(
            "cm","DeleteNodes",params,version="2.0",cm=cm,URI=URI,
            cache_destroy_extra=[dict(uri=None,module='cm',method='Resolve',
                                      version="2.0",urn=myslice['urn']),
                                 dict(uri=None,module='cm',method='SliverStatus',
                                      version="2.0",slice_urn=myslice['urn'])])
        if rval:
            self.__put_lock()
            raise exc.RPCError("cm::DeleteNodes",rval,response)
        self.__put_lock()
        return response
    pass

class ProtoGeniResponse:
    """
    An object encapsulating a ProtoGeni RPC Server method response.
    """
    def __init__(self, code, value=0, output=""):
        self.code     = code            # A RESPONSE code
        self.value    = value           # A return value; any valid XML type.
        self.output   = re.sub(        # Pithy output to print
            r'[^' + re.escape(string.printable) + ']', "", output)
        return
    pass

class ProtoGeniManifestWrapper(PGManifest):
    """
    A simple wrapper for the geni-lib ProtoGeni Manifest.  The intent is
    to provide users some syntactic sugar to obtain common interesting
    pieces of information without walking the XML themselves.
    """
    
    def __init__(self,path=None,xml=None):
        # Do the core stuff
        super(ProtoGeniManifestWrapper,self).__init__(path=path,xml=xml)
        
        #
        # Construct a link interface member table for quicker
        # lookups.  Maps link client_id to ManifestNode.Interface
        # objects.
        #
        self.link_interfaces = {}
        # Maps interface sliver_id to link client id
        self.interface_links = {}
        for link in self.links:
            self.link_interfaces[link.client_id] = []
            for sid in link.interface_refs:
                self.interface_links[sid] = link.client_id
                pass
            pass
        self.nodenames = {}
        for node in self.nodes:
            self.nodenames[node.name] = node
            for i in node.interfaces:
                if self.interface_links.has_key(i.sliver_id):
                    lid = self.interface_links[i.sliver_id]
                    self.link_interfaces[lid].append(i)
                    pass
                pass
            pass
        
        #
        # Find any parameters.
        #
        pass
    
    def iptonum(self,ip):
        packedaddr = socket.inet_aton(ip)
        retval = (ord(packedaddr[0]) << 24) | (ord(packedaddr[1]) << 16) \
                | (ord(packedaddr[2]) << 8) | ord(packedaddr[3])
        return retval

    def getHostnameAndPort(self,node):
        for n in self.nodes:
            if node.startswith("urn:"):
                if node == n.component_id:
                    return (n.logins[0].hostname,n.logins[0].port)
            elif node == n.name:
                return (n.logins[0].hostname,n.logins[0].port)
            elif node == n.sliver_id:
                return (n.logins[0].hostname,n.logins[0].port)
            pass
        return None
    
    def getLinkClientIds(self):
        """
        Returns a list of link client_ids.
        """
        return self.link_interfaces.keys()
    
    def getLinkAddressInfo(self,lan_client_id):
        """
        Returns a dict of address info for the specified lan's client_id,
        which contains::
        
          'netmasks' : [netmask,...] (there really should only be one
                                        entry here, of course)
          'min_ip' : ip
          'max_ip' : ip
        
        The intent is that this could help an IP address algorithm generate
        addresses for new nodes, but so that they don't overlap with existing
        addresses.
        """
        if not self.link_interfaces.has_key(lan_client_id):
            return None
        
        retval = { 'netmasks' : [], 'min_ip' : None, 'max_ip' : None }
        for i in self.link_interfaces[lan_client_id]:
            (ip,netmask) = i.address_info
            ipnum = self.iptonum(ip)
            if netmask and not netmask in retval['netmasks']:
                retval['netmasks'].append(netmask)
                pass
            if ip:
                #print "%s -> %d" % (ip,ipnum)
                if not retval['min_ip'] or ipnum < self.iptonum(retval['min_ip']):
                    retval['min_ip'] = ip
                if not retval['max_ip'] or ipnum > self.iptonum(retval['max_ip']):
                    retval['max_ip'] = ip
                pass
            pass
        return retval
    
    pass

class ProtoGeniClientServerEndpoint(object):
    """
    A stub class from which all ClientServer Endpoints should descend.
    """
    def __init__(self,clientServer=None):
        self.clientServer = clientServer
        pass
    
    def setClientServer(self,s):
        self.clientServer = s
        pass
    
    pass

class ProtoGeniClientXMLRPCRequestHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    """
    Simple XML-RPC request handler class.

    Handles all HTTP POST requests and attempts to decode them as
    XML-RPC requests.  Taken from the SimpleXMLRPCServer module and
    heavily modified with path-based # dispatch.
    """

    ##
    # Change the default protocol so that persistent connections are *not*
    # the norm.  The ProtoGENI server, our client, should never have to make
    # persistent connections.
    #
    protocol_version = "HTTP/1.0"

    def do_AUTHHEAD(self):
        self.send_response(401)
        self.send_header('WWW-Authenticate',
                         'Basic realm=\"%s\"' % (self.server.name,))
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    ##
    # Change the default protocol so that persistent connections are not the norm.
    #
    protocol_version = "HTTP/1.0"

    def __xmlrpc_module_dispatcher(self,method,params):
        try:
            # figure out which module from path
            rpath = self.path
            if rpath is None or rpath == '/':
                raise Exception("must supply valid module interface")
            rpath = rpath.lstrip('/')
            rpa = rpath.split('/')
            modname = rpa[0]
            dobj = self.server.getModule(modname)
            if dobj is None:
                raise Exception("invalid module '%s'" % (modname,))
            
            LOG.debug("Looking up method %s in module %s (%s)"
                      % (method,modname,str(dobj)))
            meth = getattr(dobj,method);
            LOG.debug("Running method %s" % str(meth))
        except AttributeError:
            LOG.exception("attribute error during xmlrpc dispatch")
            raise Exception('method "%s" is not supported' % (method,))
        else:
            try:
                return apply(meth,params)
            except:
                LOG.exception("error running method")
                pass
            pass
        pass

    ##
    # Handle a POST request from the user.  This method was changed from the
    # standard version to not close the 
    #
    def do_POST(self):
        """Handles the HTTP POST request.

        Attempts to interpret all HTTP POST requests as XML-RPC calls,
        and handle them here so we have path info.  Can't forward to ._dispatch
        in the server because we lose the path info.
        """
        LOG.debug("REQUEST HEADERS: %s" % str(self.headers))
        try:
            # authenticate if the server wanted it!
            if self.server.wantsAuthorization():
                ahdr = self.headers.getheader('Authorization')
                if not ahdr:
                    return self.do_AUTHHEAD()
                if not self.server.checkAuthorizationHeader(ahdr):
                    LOG.warning("unauthorized!")
                    raise Exception("unauthorized!")
                else:
                    LOG.debug("authorized")
                pass

            # get arguments
            data = self.rfile.read(int(self.headers["content-length"]))
            
            LOG.debug("REQUEST BODY: %s" % str(data))

            # make the call through our module
            #_dispatcher = lambda(m,p): self.__xmlrpc_module_dispatcher(m,p,dobj)
            response = self.server._marshaled_dispatch(data,self.__xmlrpc_module_dispatcher)
            #_dispatcher)
        except Exception as e:
            # This should only happen if the module is buggy
            # internal error, report as HTTP server error
            LOG.exception("error handling incoming POST")
            self.send_response(500)
            self.end_headers()
            self.wfile.flush()
        else:
            # got a valid XML RPC response
            LOG.debug("RESPONSE: %s" % response)
            self.send_response(200)
            self.send_header("Content-type", "text/xml")
            self.send_header("Content-length", str(len(response)))
            self.end_headers()
            self.wfile.write(response)
            self.wfile.flush()
            pass
        return

    def log_request(self, code='-', size='-'):
        """Selectively log an accepted request."""
        if self.server.logRequests:
            BaseHTTPServer.BaseHTTPRequestHandler.log_request(self, code, size)
            
    #def finish(self):
    #    # Threading servers need this.
    #    self.request.set_shutdown(SSL.SSL_SENT_SHUTDOWN|SSL.SSL_RECEIVED_SHUTDOWN)
    #    self.request.close()

    pass

#
# A simple server based on the threading version SSLServer.
# 
class ProtoGeniClientServer(SSL.ThreadingSSLServer,SimpleXMLRPCDispatcher):
    """
    This is a simple client-side, threaded XMLRPC server that
    server-side ProtoGeni components can invoke to send notifications,
    and possibly request information.  For now, the only module they
    might try to invoke is the 'dynslice' module.
    
    This simple server uses your GENI certificate as its CA certificate,
    and validates the incoming RPC invocations from ProtoGeni servers.  In
    addition to certificate validation, it can also use HTTPS username/passwd
    authentication (and the ProtoGeni server programs will use that if
    specified).
    """
    
    def __init__(self,addr="0.0.0.0",port=15243,username=None,password=None,
                 passwdDict=None,modules=None,
                 cert=None,cacert=CACERTIFICATE,noCertHostChecking=False,
                 ext_addr=None,ext_port=None):
        self.addr          = addr
        self.port          = port
        self.username      = username
        self.password      = password
        self.logRequests   = 1
        self.modules       = modules
        self.passwdDict    = {}
        self.noCertHostChecking = noCertHostChecking
        self.ext_addr      = ext_addr
        self.ext_port      = ext_port
        
        if not modules or len(modules) == 0:
            raise Exception("no modules specified; aborting!")
        
        for (name,mod) in self.modules.iteritems():
            if isinstance(mod,ProtoGeniClientServerEndpoint):
                if hasattr(mod,'setClientServer'):
                    LOG.debug("setClientServer on %s" % (str(mod),))
                    mod.setClientServer(self)
                    pass
                pass
            pass
        
        if cert is None:
            hd = os.getenv('HOME')
            cert = hd + "/.ssl/encrypted.pem"
        if cacert is None:
            hd = os.getenv('HOME')
            cacert = hd + "/.ssl/emulab-cacert.pem"
            pass
        
        if passwdDict:
            for (user,passw) in passwdDict.iteritems():
                self.passwdDict[user] = base64.b64encode("%s:%s" % (user,passw))
                pass
            pass
        if username and password:
            self.passwdDict[username] = \
                base64.b64encode("%s:%s" % (username,password))
            pass

        M2Crypto.threading.init()
        ctx = SSL.Context('sslv23')
        ctx.load_cert(cert,callback=PassPhraseCB)
        ctx.load_client_CA(cacert)
        ctx.load_verify_info(cacert)
        #ctx.load_verify_info(cert) #,callback=PassPhraseCB)
        ctx.set_verify(SSL.verify_fail_if_no_peer_cert | SSL.verify_peer, 16)
        ctx.set_allow_unknown_ca(0)
        #ctx.set_info_callback()
        
        dargs = (self,)
        if sys.version_info[0] >= 2 and sys.version_info[1] >= 5:
            dargs = (self,False,None)
            pass
        SimpleXMLRPCDispatcher.__init__(*dargs)
        SSL.ThreadingSSLServer.__init__(self,(addr,port),
                                        ProtoGeniClientXMLRPCRequestHandler,ctx)
        if self.noCertHostChecking:
            # HACK -- change out the checker to not check CN/hostnames.
            self.socket.clientPostConnectionCheck = NoHostChecker()
            pass
        
        # prepare for stoppable run feature
        self._stop         = threading.Event()
        self._thread       = None
        
        # Build our URL:
        self.url = "https://"
        if self.username:
            self.url += self.username
            if self.password:
                self.url += ":" + self.password
            self.url += "@"
            pass
        (sa,sp) = (None,None)
        if self.ext_addr is not None:
            self.url += str(self.ext_addr)
        else:
            (sa,sp) = self.socket.getsockname()
            if sa == '0.0.0.0':
                self.url += socket.gethostbyname(socket.gethostname())
            else:
                self.url += str(sa)
                pass
            pass
        if self.ext_port is not None:
            self.url += ":" + str(self.ext_port)
        else:
            self.url += ":" + str(sp)
            pass
        self.url += "/dynslice"
        
        pass
    
    def getURL(self):
        return self.url
    
    def getModule(self,modname):
        if self.modules.has_key(modname):
            mod = self.modules[modname]
            if isinstance(mod,ProtoGeniClientServerEndpoint):
                return mod
            else:
                return mod(self)
        else:
            return None

    def wantsAuthorization(self):
        if len(self.passwdDict.keys()) == 0:
            return False
        else:
            return True
    
    def checkAuthorizationHeader(self,header):
        LOG.debug("AUTH HEADER: %s" % str(header))
        if header.find('Basic ') > -1:
            ha = header.split('Basic ')
            if len(ha) == 2:
                for (user,hash) in self.passwdDict.iteritems():
                    if ha[1] == hash:
                        LOG.debug('user %s successfully authenticated' % (user,))
                        return True
                    pass
                LOG.debug('bad authorization hash %s (%s)'
                          % (ha[1],base64.b64decode(ha[1])))
                return False
            else:
                LOG.debug('unsupported Authorization header "%s"' % (header,))
                return False
        else:
            LOG.debug('unsupported Authorization header "%s"' % (header,))
            return False

    def verify_request(self, request, client_address):
        return True
    
    def stop_handling(self):
        if self._thread:
            self._stop.set()
            self._thread.join()
            # reset
            self._thread = None
            self._stop = threading.Event()
        else:
            raise Exception("not handling in separate thread")
        pass
    
    def _handle_requests_threaded(self,timeout=0.10):
        while not self._stop.isSet():
            (rset,wset,eset) = select.select([self,],[],[],timeout)
            if self in rset:
                LOG.debug("Handling request...")
                self.handle_request()
                LOG.debug("done handling")
                pass
            pass
        pass
    
    def handle_requests_threaded(self,timeout=0.10):
        if self._thread:
            raise Exception("already handling requests in separate thread")
        else:
            name = "%s:%s[handler]" % (self.addr,str(self.port),)
            self._thread = threading.Thread(target=self._handle_requests_threaded,
                                            name=name,kwargs={'timeout':timeout})
            self._thread.start()
        pass
    
    pass
